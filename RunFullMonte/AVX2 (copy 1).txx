#!/bin/bash

USER=AbedYassine
TAG=master
REGISTRY=registry.gitlab.com
MIDDLE=fullmonte/fullmontesw/fullmonte-run-avx
IMAGE=$REGISTRY/$MIDDLE:$TAG


# The local path to be mounted to /meshes in the container
# MESH_PATH is a keyword. Please do not change it! 
MESH_PATH=/home/yassineab/PhD_Research/PDT-fullmonte-gui/mount

# Pull the Docker image
# docker login -u $USER $REGISTRY
docker pull $IMAGE

# Allow local access from docker group to X windows server
# (necessary on some hosts, not others - reason unknown)
xhost +local:docker


# Run Docker image
# --rm: Delete container on finish
# -t:   Provide terminal
# -i:   Interactive
# -e:   Set environment variable DISPLAY
# -v:   Mount host path into container <host-path>:<container path>
# --privileged: Allow container access to system sockets (for X)

docker run --rm \
    -v $MESH_PATH:/sims \
    -v /tmp/.X11-unix/X0:/tmp/.X11-unix/X0 \
    --privileged \
    -e DISPLAY=:0 \
    $IMAGE \
