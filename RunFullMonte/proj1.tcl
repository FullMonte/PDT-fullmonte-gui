Composite C
Point P1
   P1 position "0 0 0"
C add P1
#Line L2
   #L2 endpoint 0 "1 0 0"
   #L2 endpoint 1 "2 0 0"
#C add L2
Material tongue
   tongue scatteringCoeff   2
   tongue absorptionCoeff   1
   tongue refractiveIndex   4
   tongue anisotropy   3
Material larynx
   larynx scatteringCoeff   2
   larynx absorptionCoeff   1
   larynx refractiveIndex   4
   larynx anisotropy   3
Material tumour
   tumour scatteringCoeff   2
   tumour absorptionCoeff   1
   tumour refractiveIndex   4
   tumour anisotropy   3
Material teeth
   teeth scatteringCoeff   2
   teeth absorptionCoeff   1
   teeth refractiveIndex   4
   teeth anisotropy   3
Material bone
   bone scatteringCoeff   2
   bone absorptionCoeff   1
   bone refractiveIndex   4
   bone anisotropy   3
Material surroundingtissues
   surroundingtissues scatteringCoeff   2
   surroundingtissues absorptionCoeff   1
   surroundingtissues refractiveIndex   4
   surroundingtissues anisotropy   3
Material subcutaneousfat
   subcutaneousfat scatteringCoeff   2
   subcutaneousfat absorptionCoeff   1
   subcutaneousfat refractiveIndex   4
   subcutaneousfat anisotropy   3
Material skin
   skin scatteringCoeff   2
   skin absorptionCoeff   1
   skin refractiveIndex   4
   skin anisotropy   3
Material air
   air scatteringCoeff   0
   air absorptionCoeff   0
   air refractiveIndex   1.0
   air anisotropy   0.0
Material air
   air scatteringCoeff   0
   air absorptionCoeff   0
   air refractiveIndex   1.0
   air anisotropy   0.0

MS exterior air
MS append tongue
MS append larynx
MS append tumour
MS append teeth
MS append bone
MS append surroundingtissues
MS append subcutaneousfat
MS append skin
MS append air
package require FullMonte
set fn "/sims//home/alinouri/Desktop/my_project/proj1.tclmesh.vtk"

VTKMeshReader R
   R filename $fn
   R read

set M [R mesh]
MaterialSet MS

concat /home/alinouri/Desktop/my_project/proj1.opt
concat /home/alinouri/Desktop/my_project/proj1.source
TetraVolumeKernel k
   k packetCount 10000
   k source C
   k geometry $M
   k materials MS
   k runSync
set ODC [k results]
puts "Results available:"
for { set i 0 } { $i < [$ODC size] } { incr i } {
   set res [$ODC getByIndex $i]
   puts " type=[[$res type] name] name=[$res name]"
}
EnergyToFluence EF
   EF geometry $M
   EF materials MS
   EF source [$ODC getByName "VolumeEnergy"]
   EF inputEnergy
   EF outputFluence
   EF update
VTKMeshWriter W
W filename "/12/.out.vtk"
W addData "Fluence" [EF result]
W mesh $M
W write
TextFileMatrixWriter TW
   TW filename "/sims/183test21.phi_v.txt"
   TW source [EF result]
   TW write
