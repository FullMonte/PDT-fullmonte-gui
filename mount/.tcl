package require FullMonte
set fn "/sims/.mesh.vtk"

VTKMeshReader R
   R filename $fn
   R read

set M [R mesh]
MaterialSet MS

source /sims/_opt.tcl
source /sims/_src.tcl
TetraVolumeKernel k
   k packetCount 
   k source C
   k geometry $M
   k materials MS
   k runSync
set ODC [k results]
puts "Results available:"
for { set i 0 } { $i < [$ODC size] } { incr i } {
   set res [$ODC getByIndex $i]
   puts " type=[[$res type] name] name=[$res name]"
}
EnergyToFluence EF
   EF geometry $M
   EF materials MS
   EF source [$ODC getByName "VolumeEnergy"]
   EF inputEnergy
   EF outputFluence
   EF update
VTKMeshWriter W
W filename /sims/.out.vtk
W addData "Fluence" [EF result]
W mesh $M
W write
TextFileMatrixWriter TW
   TW filename "/sims/.phi_v.txt"
   TW source [EF result]
   TW write
