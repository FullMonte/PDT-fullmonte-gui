#ifndef PDTGUIMAINWINDOW_H
#define PDTGUIMAINWINDOW_H

#include <vector>


#include <QMainWindow>
#include <QString>

#include "Qt/Widgets/PDTWorkPageWidget.h"
#include "Qt/Widgets/scriptselector.h"


using namespace std;

namespace Ui {
class PDTGuiMainWindow;
}

class PDTGuiMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    //constructor with default of no parent
    explicit PDTGuiMainWindow(QWidget *parent = 0);
    ~PDTGuiMainWindow();

public slots:
    void setStatusBarMessage(const QString);
    void setSourceMenuEnabled(bool);
    void setToolMenuEnabled(bool);
    void setToolBarEnabled(bool);


private slots:
    void checkTabWidgetStatus();

    void createProject();
    void loadProject();
    PDTWorkPageWidget* loadMeshSuccess(QString proj_name,QString filePath);
    void showCoordinateAxis(PDTWorkPageWidget* newWorkPageWidget);


    QString createProjectDirectory();

    // Source creation callbacks
    void createPlanePlacement();
    void createLineSource();
    void createPointSource();
    void createFiberSource();

    // Create VTK tools
    void createClipper();

    // File saver/Loader
    void saveSourceFile();
    void loadSourceFile();
    void saveOpticalFile();
    void readTcl();
    void testRun();
    void closeWorkPageWidget(int);

    //Run FullMonte
    void runFullMonte(simtype simultype);
    void runFullMonte_prompt();

    void saveProject();

    void Info();

    void trymenu();




private:
    Ui::PDTGuiMainWindow *ui;

    std::vector<PDTWorkPageWidget*> pdtWorkPages;

    void setUpQtConnections();

    void setGuiEnable(bool);

    PDTWorkPageWidget* createNewWorkPage();

    void clearAllWorkPages();

    // Menu
    void initializeMenu();
    void activateMenu();

    TclProjInfo* tempProjInfo;

    scriptselector ScriptSelector;

};

#endif // PDTGUIMAINWINDOW_H
