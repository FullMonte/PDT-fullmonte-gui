#include "pdtguimainwindow.h"
#include "ui_pdtguimainwindow.h"

#include <vtkRendererCollection.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkSmartPointer.h>
#include <vtkRenderWindowInteractor.h>

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QTableWidget>
#include <QFile>
#include <vtkOrientationMarkerWidget.h>
#include <vtkAxesActor.h>
#include "Qt/Utils/MessageBoxUtils.h"
#include "Tcl/Tcl.h"
#include "Tcl/TclReader.h"
#include <QString>
#include <QComboBox>

#include <qfiledialog.h>
#include "Qt/Widgets/scriptselector.h"





/*
 * pdtguimainwindow.cpp is responsible for running the program. Strictly speaking
 * that maybe the job of main.cpp but there is nothing to modify there. main.cpp
 * creates and instance of pdtguimainwindow and runs it.
 */


/* All the functions that are visible as soon as the program is run (before loading mesh or anything)
 * can be modified through pdtguimainwindow*/

/*The graphics relating to pdtguimainwindow can be modified through the drop down menu of Qt creator.
 * Click on Drop down menu "File"-->"Open File Or Project". Under "Application/" you can find
 * PDTGuiMainWindow.ui"*/

/*Make sure you look at PDTWorkpageWidget as well !!!!*/






//constructor that involves initialization list
PDTGuiMainWindow::PDTGuiMainWindow(QWidget *parent) :
    QMainWindow(parent),//QMainWindow is a superclass of Mainwindow
    ui(new Ui::PDTGuiMainWindow) {

    ui->setupUi(this);

    ui->menuBar->setNativeMenuBar(false);//to display menu bar

    // Maxmize to the screen
    showMaximized();

    setUpQtConnections();



    // Clear the default tabs in the tab widget
    while (ui->pdtWorkTabs->count() != 0){
        delete ui->pdtWorkTabs->currentWidget();
    }


}

PDTGuiMainWindow::~PDTGuiMainWindow() {
    delete ui;
    clearAllWorkPages();
}


/*In order to understand this function you have to understand the signal slot mechanism
 * of Qt. Here is a tutorial link : http://doc.qt.io/archives/qt-4.8/signalsandslots.html.
 * Anytime a button is added on the GUI, its function has to be specified. Qt Creator
 * has ways of simplifying the link between SIGNAL and SLOT through the GUI design but you should be comfortable
 * writing these commands yourself*/

/*Example :
 * connect(connect(ui->actionNewProject, SIGNAL(triggered()), this, SLOT(createProject()));
 * There is a button called actionNewProject which is a part of the ui class or more precisely
 * this->ui->actionNewProject. once this button is clicked, a triggered signal is generated.
 * This signal will execute the commands inside the function createProject. Create project is a
 * part of the "this" class (3rd parameter) */


void PDTGuiMainWindow::setUpQtConnections() {
    // Connections on menu
    connect(ui->actionNewProject, SIGNAL(triggered()), this, SLOT(createProject()));

    // Connections on source creation
    connect(ui->actionCreateLineSource, SIGNAL(triggered()), this, SLOT(createLineSource())); //create line source
    connect(ui->actionPoint,SIGNAL(triggered()),this,SLOT(createPointSource()));//create point source
    connect(ui->actionCut_end_Fiber,SIGNAL(triggered()),this,SLOT(createFiberSource()));//create fiber source
    connect(ui->actionCreatePlanePlacement, SIGNAL(triggered()), this, SLOT(createPlanePlacement())); //create plane source


    // Connections on saver/loader
    connect(ui->actionSaveSourceFile, SIGNAL(triggered()), this, SLOT(saveSourceFile()));//save file
    connect(ui->actionLoadSourceFile, SIGNAL(triggered()), this, SLOT(loadSourceFile()));//load file
    connect(ui->actionSave_opt_prop_file,SIGNAL(triggered()),this,SLOT(saveOpticalFile()));//save.opt
    connect(ui->actionTclReader,SIGNAL(triggered()),this,SLOT(readTcl()));//read .tcl
    connect(ui->actionTest_Run,SIGNAL(triggered()),this,SLOT(testRun()));//run test
    connect(ui->pdtWorkTabs,SIGNAL(tabCloseRequested(int)),this,SLOT(closeWorkPageWidget(int))); //close current tab page
    connect(ui->actionLoad_Project_4,SIGNAL(triggered()),this,SLOT(loadProject()));//load project



    // Connections on tab widget
    connect(ui->pdtWorkTabs, SIGNAL(currentChanged(int)), this, SLOT(checkTabWidgetStatus()));

    // Connections for vtk tools
    connect(ui->actionClipper, SIGNAL(triggered()), this, SLOT(createClipper()));

    //Running programs
    connect(ui->actionRun_FullMonte,SIGNAL(triggered()),this,SLOT(runFullMonte_prompt()));



    //Save project
    connect(ui->actionSave_Project,SIGNAL(triggered()),this,SLOT(saveProject()));
    connect(ui->actionMesh_Info,SIGNAL(triggered(bool)),this,SLOT(Info()));
    connect(ui->actionTry,SIGNAL(triggered(bool)),this,SLOT(trymenu()));



    bool success = connect(&(this->ScriptSelector),SIGNAL(fullmonte_signal(simtype)),this,SLOT(runFullMonte(simtype)));







}


void PDTGuiMainWindow::setGuiEnable(bool enable) {

}

/* Create a project by specifying directory location and name*/

void PDTGuiMainWindow::createProject() {

    /* Get the name of the project from the user. Store other information
     * of project in tempProjInfo. tempProjInfo is an instance of the class
    TclProjInfo. This class stores names of tcl files, directory paths and project name*/

    QString projName=this->createProjectDirectory();

    QString filePath;
    if(!projName.isEmpty()){
        filePath = QFileDialog::getOpenFileName(this,"Choose mesh file");
    }
    QFileInfo info(filePath);

    if (filePath.isEmpty()) {
        return ;
    }

    //Loads the mesh into the GUI
    loadMeshSuccess(projName,filePath);


    //add optical propety (air)
    //air is by default a part of the optical properties
     PDTWorkPageWidget* curWorkPage = (PDTWorkPageWidget*) ui->pdtWorkTabs->currentWidget(); 
     curWorkPage->addAirProperty();
     curWorkPage->addPropertyToTable();



}



PDTWorkPageWidget* PDTGuiMainWindow::loadMeshSuccess(QString projName,QString filePath){

    // Show a loading dialog simultaneously
    QMessageBox msgBox(this);
    msgBox.setWindowTitle("Loading ....");
    msgBox.setText("Reading Files, please wait ...");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setStandardButtons(QMessageBox::NoAll);
    msgBox.open();
    QCoreApplication::processEvents();
    ui->statusBar->showMessage(tr("File loading"));

    PDTWorkPageWidget* newWorkPage = createNewWorkPage();

    bool success;
    if(newWorkPage->loadFile(filePath)){
        // Create new working page to tab
        //QString project_name = info.baseName();
        ui->pdtWorkTabs->setEnabled(true);
        int index = ui->pdtWorkTabs->addTab(newWorkPage, projName);
        ui->pdtWorkTabs->setCurrentIndex(index);
        // if project is created succesfully TclProjectInfo is set
        newWorkPage->setTclProjInfo(tempProjInfo);
        //set coordinate system
        newWorkPage->showCoordinateAxis();
        //setup sliderbar for material
        newWorkPage->set_up_sliderbar();

    } else {
        delete newWorkPage;
        QString failure_message = tr("Unable to read the file! ");
        MessageBoxUtils::showErrorMessageBox(failure_message, this);
        ui->statusBar->showMessage(failure_message);
        newWorkPage=NULL;
    }

    msgBox.close();
    return newWorkPage;

}


QString PDTGuiMainWindow::createProjectDirectory(){
    //expecting the user to create a new directory
    // set the project path to the new directory

    QString project_path = QFileDialog::getExistingDirectory(0,"Set project directory");
    QFileInfo my_info(project_path);
    QString proj_name=my_info.baseName();
    TclProjInfo* new_proj_info= new TclProjInfo();


    // Determine the file paths of 3 .tcl files & file that stores mesh path
    // based on the specified project path
    // project name is set to the directory name

    QString tcl_file_path = project_path+"/"+proj_name+".tcl";
    QString opt_file_path = project_path+"/"+proj_name+"_opt.tcl";
    QString src_file_path = project_path+"/"+proj_name +"_src.tcl";
    QString mesh_path_file_path = project_path+"/"+proj_name+"_mesh_path.txt";

    qDebug()<<"Project path from createProjectDirectory";
    qDebug()<<project_path;
    new_proj_info->set_proj_dir(project_path);
    new_proj_info->set_tcl_file_path(tcl_file_path);
    new_proj_info->set_opt_file_path(opt_file_path);
    new_proj_info->set_src_file_path(src_file_path);
    new_proj_info->set_mesh_path_file_path(mesh_path_file_path);

    tempProjInfo = new_proj_info;

    return proj_name;

}

/* Loading a pre-existing project.
 *
 */

void PDTGuiMainWindow::loadProject(){
    //TclReader is a text parser
    TclReader load_project;
    //set project directory path
    //detail in TclReader.cpp about how to construct path and set/read project directory or .tcl file
    bool success=load_project.set_proj_dir();
    if(!success){

        return;
    }

    /* TclProjInfo contains info such as:
     * directory paths and filepaths
     *Refer to TclProjInfo.cpp for more info
     */

    TclProjInfo* proj_info = load_project.get_project_info();//store in space point by proj_info
    QString project_path = proj_info->get_tcl_file_path(); //.tcl path
    QFileInfo file_info(project_path); //construct file_info using project_path
    QString proj_name = file_info.baseName();

    QString mesh_path = load_project.read_mesh_path();//get mesh path from load_project

    /* Create a new workpagewidget and load the mesh identified
    by "mesh_path" onto the workpagewidget*/

    PDTWorkPageWidget* newWorkPage = loadMeshSuccess(proj_name,mesh_path);

    if(newWorkPage!=NULL){
        load_project.set_PDTWorkPageWidget(newWorkPage);
        load_project.set_WorkPage_TclProjInfo();
        //  load optical properties
        std::vector<OpticalProperty*> optical_properties=load_project.optical_reader();

        for(unsigned i=0;i<optical_properties.size();i++){
            newWorkPage->newOpticalProperty(optical_properties[i]);
        }

        newWorkPage->addAirProperty();

        newWorkPage->addPropertyToTable();

        newWorkPage->set_slider_label();

        //load sources
       std::vector<PDTVtkSource*> source_result =load_project.source_reader();
       newWorkPage->TclSetSourceTable(source_result);
       newWorkPage->set_annotations();

    }


}






void PDTGuiMainWindow::showCoordinateAxis(PDTWorkPageWidget* newWorkPageWidget){
    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
      vtkSmartPointer<vtkRenderWindowInteractor>::New();

//    newWorkPageWidget->get
//    renderWindowInteractor->SetRenderWindow(newWorkPageWidget->ui->qvtkWidget->GetRenderWindow()->GetRenderers()->GetFirstRenderer());


    vtkSmartPointer<vtkAxesActor> axes =
      vtkSmartPointer<vtkAxesActor>::New();

    vtkSmartPointer<vtkOrientationMarkerWidget> widget =
      vtkSmartPointer<vtkOrientationMarkerWidget>::New();
    widget->SetOutlineColor( 0.9300, 0.5700, 0.1300 );
    widget->SetOrientationMarker( axes );
    widget->SetInteractor( renderWindowInteractor );
    widget->SetViewport( 0.0, 0.0, 0.4, 0.4 );
    widget->SetEnabled( 1 );
    widget->InteractiveOn();

}


void PDTGuiMainWindow::clearAllWorkPages(){

    for(PDTWorkPageWidget* w : pdtWorkPages) {
        delete w;
    }

    ui->pdtWorkTabs->clear();
}

void PDTGuiMainWindow::createLineSource() {
    PDTWorkPageWidget* curWorkPage = (PDTWorkPageWidget*) ui->pdtWorkTabs->currentWidget();
    curWorkPage->createLineSource();
}

void PDTGuiMainWindow::createFiberSource(){
    PDTWorkPageWidget* curWorkPage = (PDTWorkPageWidget*) ui->pdtWorkTabs->currentWidget();
    curWorkPage->createFiberSource();
}

void PDTGuiMainWindow::createPointSource(){
    PDTWorkPageWidget* curWorkPage = (PDTWorkPageWidget*) ui->pdtWorkTabs->currentWidget();
    curWorkPage->createPointSource();
}

void PDTGuiMainWindow::createClipper() {
    PDTWorkPageWidget* curWorkPage = (PDTWorkPageWidget*) ui->pdtWorkTabs->currentWidget();
    curWorkPage->createClipper();
}

void PDTGuiMainWindow::createPlanePlacement() {
    PDTWorkPageWidget* curWorkPage = (PDTWorkPageWidget*) ui->pdtWorkTabs->currentWidget();
    curWorkPage->createPlanePlacement();
}

void PDTGuiMainWindow::setStatusBarMessage(const QString message) {
    ui->statusBar->showMessage(message);
}

void PDTGuiMainWindow::saveSourceFile() {
    PDTWorkPageWidget* curWorkPage = (PDTWorkPageWidget*) ui->pdtWorkTabs->currentWidget();

    ProjectInfo& projectInfo = curWorkPage->getProjectInfo();
    QString fileName = projectInfo.getSourceFilePath();

    if (fileName.isEmpty()) {
        fileName = QFileDialog::getSaveFileName(this, tr("Save source list"), "",
                                   tr("XML files (*.xml)"));

        if (fileName.isEmpty()) {
            return ;
        } else {
            QFileInfo info(fileName);
            if (info.suffix() != "xml") {
                fileName += ".xml";
            }
        }
    }

    curWorkPage->saveSourceFile(fileName);
}



void PDTGuiMainWindow::loadSourceFile() {
    PDTWorkPageWidget* curWorkPage = (PDTWorkPageWidget*) ui->pdtWorkTabs->currentWidget();


    QString fileName = QFileDialog::getOpenFileName(this, tr("Load Source File"), "", tr("XML files(*.xml)"));

    if (fileName.isEmpty())
        return;

    curWorkPage->loadSourceFile(fileName);

}

void PDTGuiMainWindow::saveOpticalFile(){
    PDTWorkPageWidget* curWorkpage = (PDTWorkPageWidget* ) ui->pdtWorkTabs->currentWidget();

    QString fileName = QFileDialog::getSaveFileName(this,"Save optical property list","",tr("XML files(*.xml)"));

    if(fileName.isEmpty()){
        return;
    }

    curWorkpage->saveOpticalFile(fileName);
}



void PDTGuiMainWindow::runFullMonte_prompt(){


    //this->ScriptSelector= new scriptselector();
    //emit ScriptSelector.fullmonte_signal();
    (&ScriptSelector)->show();


}

void PDTGuiMainWindow::runFullMonte(simtype simultype){

    qDebug()<<"Somehow entered runFullMonte";
   // exit(0);


    //Get the current PDTWorkPageWidget
    PDTWorkPageWidget* curWorkPage = (PDTWorkPageWidget*) ui->pdtWorkTabs->currentWidget();



    //Tcl needs the PDTWorkPageWidget to work
    Tcl myTcl(curWorkPage,simultype);


    //The sequential procedure of running the project can be seen inside the setup_file function
    // The user may decide to cancel the run midway
    // valid checks if user has cancelled
    bool valid=myTcl.setup_file(false);


    if(valid){
        myTcl.run_full_monte();
    }



}

void PDTGuiMainWindow::saveProject(){
    PDTWorkPageWidget* curWorkPage = (PDTWorkPageWidget*) ui->pdtWorkTabs->currentWidget();

    Tcl myTcl(curWorkPage);

    //parameter boolean indicates saving
    bool valid = myTcl.setup_file(true);




}

void PDTGuiMainWindow::Info(){
    PDTWorkPageWidget* curWorkPage = (PDTWorkPageWidget*) ui->pdtWorkTabs->currentWidget();

    pVTKRenderStatus render_status = curWorkPage->getRenderStatus();
    RenderMode render_mode=render_status.getRenderMode();

    QString render_text;

    if(render_mode ==GEOMETRIC_MODE){
        render_text="Geometric mode";
    }

    else if(render_mode==VOLUME_MODE){
        render_text="Volume Mode";
    }
    else{
        render_text = "Unknown";
    }

    int numRegions = curWorkPage->numberOfRegions()-1;
    QMessageBox infoBox;
    QString text = "Number of regions: ";
    text+=QString::number(numRegions)+"\n";
    text+="Render mode: "+render_text;

    infoBox.setText(text);
    infoBox.setWindowTitle("Mesh Info");
    infoBox.exec();
}

void PDTGuiMainWindow::readTcl(){
    PDTWorkPageWidget* curWorkPage = (PDTWorkPageWidget*) ui->pdtWorkTabs->currentWidget();

    TclReader myReader(curWorkPage);
    myReader.set_proj_dir();
    myReader.set_WorkPage_TclProjInfo();

    std::vector<OpticalProperty*> optical_properties=myReader.optical_reader();

    std::vector<PDTVtkSource*> source_result =myReader.source_reader();


    curWorkPage->TclSetSourceTable(source_result);




    for(unsigned i=0;i<optical_properties.size();i++){
        curWorkPage->newOpticalProperty(optical_properties[i]);
    }

}


void PDTGuiMainWindow::testRun(){
   Tcl test_Tcl;
   test_Tcl.Test();





}


PDTWorkPageWidget* PDTGuiMainWindow::createNewWorkPage() { PDTWorkPageWidget* newWorkPage = new PDTWorkPageWidget(this);

    connect(newWorkPage, &PDTWorkPageWidget::sendStatusBarMessage,
            this, &PDTGuiMainWindow::setStatusBarMessage);

    return newWorkPage;
}

void PDTGuiMainWindow::setSourceMenuEnabled(bool enabled) {
    //ui->menuSources->setEnabled(enabled);


    ui->actionCreateLineSource->setEnabled(enabled);
    ui->actionCreatePlanePlacement->setEnabled(enabled);
}

void PDTGuiMainWindow::setToolMenuEnabled(bool enabled) {
    ui->actionClipper->setEnabled(enabled);
}

void PDTGuiMainWindow::setToolBarEnabled(bool enabled) {
    if (enabled) {
        //ui->sourceBar->show();
        //ui->vtkToolBar->show();
    } else {

        //ui->sourceBar->hide();
        //ui->vtkToolBar->hide();
    }
}
void PDTGuiMainWindow::initializeMenu() {
    setSourceMenuEnabled(false);
    setToolMenuEnabled(false);
    ui->actionSaveSourceFile->setEnabled(false);
    ui->actionLoadSourceFile->setEnabled(false);
    ui->actionSave_opt_prop_file->setEnabled(false);
    ui->actionPoint->setEnabled(false);
    //ui->actionLoad_Project_4->setEnabled(false);
    ui->actionSave_Project->setEnabled(false);
    ui->actionRun_FullMonte->setEnabled(false);


}

void PDTGuiMainWindow::activateMenu() {
    setSourceMenuEnabled(true);
    setToolMenuEnabled(true);
    ui->actionSaveSourceFile->setEnabled(true);
    ui->actionLoadSourceFile->setEnabled(true);
    ui->actionPoint->setEnabled(true);
    ui->actionSave_opt_prop_file->setEnabled(true);
    ui->actionSave_Project->setEnabled(true);
    ui->actionRun_FullMonte->setEnabled(true);
}

void PDTGuiMainWindow::checkTabWidgetStatus() {
    if (ui->pdtWorkTabs->currentIndex() != -1) {
        qDebug() << "Current Widget is " << ui->pdtWorkTabs->currentIndex();
        activateMenu();
        setToolBarEnabled(true);
    } else {
        initializeMenu();
        setToolBarEnabled(false);
    }
}


void PDTGuiMainWindow::closeWorkPageWidget(int pageIndex){
    // Info has to be saved before closing

    PDTWorkPageWidget* closing_widget = (PDTWorkPageWidget*) ui->pdtWorkTabs->widget(pageIndex);


    // saving the state of the project

    Tcl save_project(closing_widget);
    save_project.setup_file(true);

    ui->pdtWorkTabs->removeTab(pageIndex);
}


void PDTGuiMainWindow::trymenu(){

    scriptselector* myselector = new scriptselector();
    myselector->show();

}
