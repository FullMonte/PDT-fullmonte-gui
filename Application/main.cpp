#include "pdtguimainwindow.h"
#include <QApplication>
#include <QFile>
#include <QDebug>
#include <QTextStream>

/*The two main files that have to be carefully read to understand the flow of the GUI are:"
1. ApplicationLib/pdtguimainwindow.cpp
2. Qt/Widgets/PDTWorkPageWidget.cpp
*/




int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //load dark theme from Qt/Resources/qdarkstyle/style.qrc
    QFile f(":qdarkstyle/style.qss");
    if(!f.exists()){
        //qDebug class output a string to provide debug information
        qDebug() << "Unable to set stylesheet,file not found\n";
    }else{
        //read file and set theme
        f.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
    }

    PDTGuiMainWindow w;
    //Mainwindow is not shown by default so w.show is needed
    w.show();

    return a.exec();
}
