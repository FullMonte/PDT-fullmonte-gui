#pragma once

#include <assert.h>
#include <qstring.h>
#include <qfileinfo.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkDICOMImageReader.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkImageData.h>
#include <vtkDataReader.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyData.h>
#include <vtkNew.h>
#include <vtkFieldData.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include "vtkDataSetMapper.h"
#include "vtkPointData.h"
#include "vtkLogLookupTable.h"
#include "vtkScalarBarActor.h"
#include "../../OpticalProperty/OpticalProperty.h"
#include <vtkThreshold.h>

/*
This class is used to read different types of files and return 
specific value needed. 
*/
class pVTKReader
{
public:
    pVTKReader(QString file_extension,QString filePath);

    pVTKReader();

    ~pVTKReader();

    // Transfer the data from disk to reader. The file extension
    // determines which reader to store in. Return false if file is not read successfully
    bool read(QString file_extension, QString filePath, vtkSmartPointer<vtkDataSetMapper> &dataSetMapper);

	//Return the output object of the current file
	vtkDataObject* GetOutput() const;

	vtkAlgorithmOutput* GetOutputPort() const;

//	//return the image data from DICOM reader
//	//only return when the reader reads DICOM type file
//	vtkImageData* GetImageData() const;

//	//return the data set from unstructured grid reader
//	//only return when reader reads vtk type file
//	vtkUnstructuredGrid* GetUnstructuredGrid() const;

	//Return the scalar range of the object
	double* GetScalarRange() const;

	QString GetReaderFileType() const;
	
	bool isReaderEmpty() const;

    int getMax();

    void SetMapperColorArray(vtkSmartPointer<vtkDataSetMapper> &dataSetMapper,int index=0);

    vtkSmartPointer<vtkLogLookupTable> createLookUpTable();

    vtkSmartPointer<vtkLookupTable> setColorScale(vtkSmartPointer<vtkDataSetMapper> &dataSetMapper);

    vtkSmartPointer<vtkScalarBarActor> setBarColorMap(vtkSmartPointer<vtkLookupTable> lookuptable
                                                               , vtkSmartPointer<vtkDataSetMapper> mapper, int array_index=0);


    void set_annotations(vtkSmartPointer<vtkLookupTable> lookuptable
                         ,vtkSmartPointer<vtkDataSetMapper> mapper,
                         std::vector<OpticalProperty*> opticalPropertyList,
                         int index=0);


    void set_color_scale(vtkSmartPointer<vtkScalarBarActor> color_scale);


    vtkSmartPointer<vtkScalarBarActor> get_color_scale();


    vtkSmartPointer<vtkDataSetMapper> get_dataSetMapper();

    void set_dataSetMapper(vtkSmartPointer<vtkDataSetMapper> dataSetMapper);

    vtkSmartPointer<vtkLookupTable> get_lin_lookupTable();


    std::vector<const char*> getArrayNames();

    vtkSmartPointer<vtkUnstructuredGridReader> getUGReader();
    vtkSmartPointer<vtkThreshold> getFilter();



private:
    vtkSmartPointer<vtkDataReader> dataReader;

	vtkSmartPointer<vtkUnstructuredGridReader> UGReader;

    vtkSmartPointer<vtkThreshold> filter;

	vtkSmartPointer<vtkDICOMImageReader> DICOMReader;

	QString cur_file_type;

	bool isEmpty;


    vtkSmartPointer<vtkScalarBarActor> color_scale;


    vtkSmartPointer<vtkDataSetMapper> dataSetMapper;

    vtkSmartPointer<vtkLookupTable> lin_lookupTable;

    int maxID;


};

