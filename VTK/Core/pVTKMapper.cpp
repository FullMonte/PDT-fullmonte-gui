#include "pVTKMapper.h"


pVTKMapper::pVTKMapper()
{
	mapper = NULL;
	UGVolumeMapper = NULL;
}


pVTKMapper::~pVTKMapper()
{
}


vtkSmartPointer<vtkMapper> pVTKMapper::GetMapper(const pVTKReader reader) {
	if (mapper != NULL)
		mapper->Delete();
	mapper = vtkSmartPointer<vtkDataSetMapper>::New();
	mapper->SetInputConnection(reader.GetOutputPort());
	return mapper;
}


//Map the data into volume from reader for volume rendering use
vtkSmartPointer<vtkUnstructuredGridVolumeMapper> pVTKMapper::GetUGVolumeMapper(const pVTKReader reader){
	if (UGVolumeMapper != NULL)
		UGVolumeMapper->Delete();
	//vtkSmartPointer<vtkUnstructuredGridGeometryFilter> filter =
	//	vtkSmartPointer<vtkUnstructuredGridGeometryFilter>::New();
	//filter->SetInputConnection(reader.GetOutputPort());
	////If the current file is not .vtk file , send error message
	//UGVolumeMapper = vtkSmartPointer<vtkUnstructuredGridVolumeZSweepMapper>::New();
	//UGVolumeMapper->SetInputConnection(filter->GetOutputPort());

	// Make sure we have only tetrahedra.
	UGVolumeMapper = vtkSmartPointer<vtkProjectedTetrahedraMapper>::New();
	UGVolumeMapper->SetInputConnection(reader.GetOutputPort());

	return UGVolumeMapper;
}
