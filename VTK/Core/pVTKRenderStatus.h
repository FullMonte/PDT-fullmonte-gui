#pragma once

#include <QString>

enum RenderMode {
    NULL_MODE,
    GEOMETRIC_MODE,
    VOLUME_MODE
};

enum GeometryType {
    NULL_TYPE,
    UNSTRUCTURED_GRID,
    STRUCTURED_GRID,
    POLY_DATA
};

class pVTKRenderStatus {
public :
    void setRenderMode(RenderMode mode) { renderMode = mode; }
    RenderMode getRenderMode() {return renderMode; }

    void setGeometryType(GeometryType type) { geometryType = type; }
    GeometryType getGeometryType() {return geometryType; }

private :
    RenderMode renderMode = NULL_MODE;
    GeometryType geometryType = NULL_TYPE;
};
