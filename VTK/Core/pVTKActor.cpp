#include "pVTKActor.h"


pVTKActor::pVTKActor()
{
	DSActor =
		vtkSmartPointer<vtkActor>::New();
	PDActor =
		vtkSmartPointer<vtkActor>::New();

    //Set Source color
    PDActor->GetProperty()->SetColor(0.855, 0.647, 0.125);

	volume =
		vtkSmartPointer<vtkVolume>::New();
}


pVTKActor::~pVTKActor()
{

}


vtkVolume* pVTKActor::GetVolume() const{
	return volume;
}

vtkActor* pVTKActor::GetPolyDataActor() const{
	return PDActor;
}

vtkActor* pVTKActor::GetDataSetActor() const{
	return DSActor;
}


//Automatically set the color transfer function and opacity function for volume based on scalar range from reader
void pVTKActor::autoSetVolume(pVTKReader* reader, vtkAbstractVolumeMapper* volumeMapper){
    double* range = reader->GetScalarRange();

//	if (reader->GetReaderFileType() == "vtk")
//		range = reader->GetUnstructuredGrid()->GetScalarRange();
//	else if (reader->GetReaderFileType() == "dcm")
//		range = reader->GetImageData()->GetScalarRange();


	// Create transfer mapping scalar value to opacity.
	vtkSmartPointer<vtkPiecewiseFunction> opacityTransferFunction =
		vtkSmartPointer<vtkPiecewiseFunction>::New();
	opacityTransferFunction->AddPoint(range[0], 0.00);
	opacityTransferFunction->AddPoint(range[1], 1.00);

	// Create transfer mapping scalar value to color.
	vtkSmartPointer<vtkColorTransferFunction> colorTransferFunction =
		vtkSmartPointer<vtkColorTransferFunction>::New();
	if (reader->GetReaderFileType() == "vtk"){
		colorTransferFunction->AddRGBPoint(range[0], 0.0, 0.0, 0.0);
		colorTransferFunction->AddRGBPoint((range[0] + (range[1] - range[0]) / 3.0), 0.0, 0.0, 255.0);
		colorTransferFunction->AddRGBPoint((range[0] + (range[1] - range[0]) * 2.0 / 3.0), 0.0, 255.0, 0.0);
		colorTransferFunction->AddRGBPoint(range[1], 255.0, 0, 0);
	}
	else if (reader->GetReaderFileType() == "dcm"){
		colorTransferFunction->AddRGBPoint(range[0], 0.0, 0.0, 0.0);
		colorTransferFunction->AddRGBPoint((range[0] + (range[1] - range[0]) / 3), 0.0, 0.0, 1.0);
		colorTransferFunction->AddRGBPoint((range[0] + (range[1] - range[0]) * 2.0 / 3.0), 0.0, 1.0, 0.0);
		colorTransferFunction->AddRGBPoint(range[1], 1.0, 0, 0);
	}
	
	volume->GetProperty()->SetColor(colorTransferFunction);
	volume->GetProperty()->SetScalarOpacity(opacityTransferFunction);
	volume->SetMapper(volumeMapper);
	volume->Update();
}


//Reutn current used prop3D
vtkProp3D* pVTKActor::GetProp3D(pVTKRenderStatus curStatus) const{
    if (curStatus.getRenderMode()== GEOMETRIC_MODE)
		return DSActor;
    else if (curStatus.getRenderMode() == VOLUME_MODE)
		return volume;
}


double* pVTKActor::GetCenter(pVTKRenderStatus curStatus) const{
    if (curStatus.getRenderMode() == GEOMETRIC_MODE)
		return DSActor->GetCenter();
    else if (curStatus.getRenderMode() == VOLUME_MODE)
		return volume->GetCenter();
}


double* pVTKActor::GetBounds(pVTKRenderStatus curStatus) const{
    if (curStatus.getRenderMode() == GEOMETRIC_MODE)
		return DSActor->GetBounds();
    else if (curStatus.getRenderMode() == VOLUME_MODE)
		return volume->GetBounds();
}

void pVTKActor::setOpacity(double opacity, pVTKRenderStatus curStatus){
    if (curStatus.getRenderMode() == GEOMETRIC_MODE)
        return DSActor->GetProperty()->SetOpacity(opacity);
    else if (curStatus.getRenderMode() == VOLUME_MODE)
        return;
}
