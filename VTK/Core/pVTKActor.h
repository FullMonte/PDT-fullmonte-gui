#pragma once

#include <vtkActor.h>
#include <vtkProp3D.h>
#include <vtkProperty.h>
#include <vtkSmartPointer.h>
#include <vtkVolume.h>
#include <vtkPiecewiseFunction.h>
#include <vtkColorTransferFunction.h>
#include <vtkVolumeProperty.h>
#include <vtkUnstructuredGrid.h>
#include <vtkImageData.h>
#include <vtkLookupTable.h>

#include "pVTKReader.h"
#include "pVTKRenderStatus.h"
/*
 * This class is for managing actors for different purposes
 * DSActor is for the object rendered in geometric method
 * Volume is for the object rendered in volume rendering method
 * PDActor is specified for line probes
 */

class pVTKActor
{
private:
	//actor for dataset
	vtkSmartPointer<vtkActor> DSActor;

	//actor for polydata
	vtkSmartPointer<vtkActor> PDActor;

	//volume
	vtkSmartPointer<vtkVolume> volume;

public:
    pVTKActor();
    ~pVTKActor();

	vtkVolume* GetVolume() const;

	vtkActor* GetPolyDataActor() const;

	vtkActor* GetDataSetActor() const;

	//Reutn current used prop3D
    vtkProp3D* GetProp3D(pVTKRenderStatus curStatus) const;

	//Return the center of the current actor
    double* GetCenter(pVTKRenderStatus curStatus) const;

	//Return the bounds of the current actor
    double* GetBounds(pVTKRenderStatus curStatus) const;

	//Automatically set the color transfer function and opacity function for volume based on scalar range from reader
    void autoSetVolume(pVTKReader* reader, vtkAbstractVolumeMapper* volumeMapper);

    void setOpacity(double opacity, pVTKRenderStatus curStatus);
};

