#include "pVTKReader.h"
using namespace std;

#include <QDebug>
#include <iostream>
#include <fstream>
#include "vtkLookupTable.h"
#include <vtkStringArray.h>
#include <vtkAbstractArray.h>
#include <vtkDataObject.h>
#include <vtkPolyData.h>
#include <vtkThreshold.h>



pVTKReader::pVTKReader(){
	isEmpty = true;
}


pVTKReader::~pVTKReader()
{

}


// This piece of code basically assumes unstructured grid.
//What you really have to look at is UGReader
bool pVTKReader::read(QString file_extension, QString filePath,vtkSmartPointer<vtkDataSetMapper> &dataSetMapper){
	if (file_extension == "vtk"){
		isEmpty = false;
		cur_file_type = "vtk";

        vtkNew<vtkDataReader> temp_reader;
        temp_reader->SetFileName(filePath.toStdString().c_str());

        if(temp_reader->IsFilePolyData())
            dataReader = vtkSmartPointer<vtkPolyDataReader>::New();
        else if(temp_reader->IsFileUnstructuredGrid()){
            dataReader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
            if(dataReader->IsA("vtkPolyDataReader")){
                qDebug()<<"It is indeed";

            }
            else{
                //always enter this
                qDebug()<<"Nopesjgk";
            }
        }





        dataReader->ReadAllColorScalarsOn();
        dataReader->SetFileName(filePath.toStdString().c_str());
        dataReader->Update();

        filter = vtkSmartPointer<vtkThreshold>::New();

//		UGReader->ReadAllColorScalarsOn();
        UGReader= vtkSmartPointer<vtkUnstructuredGridReader>::New();
        UGReader->SetFileName(filePath.toStdString().c_str());
        UGReader->Update();
        this->SetMapperColorArray(dataSetMapper);



    } else if (file_extension == "dcm"){
		isEmpty = false;
		cur_file_type = "dcm";

		//For dicom file, it not only read a single file, but also reads all files with the same series IDs
		//To satisfy this, we should both set the file path and the which folder contains this file to DICOM reader
		DICOMReader->SetFileName(filePath.toStdString().c_str());
		QFileInfo pathInfo(filePath);
		DICOMReader->SetDirectoryName(pathInfo.absolutePath().toStdString().c_str());

		DICOMReader->Update();
    } else {
        return false;
    }

    //store dataSetMapper for later use in set_annotations

    this->dataSetMapper=dataSetMapper;

    return true;


}


std::vector<const char*> pVTKReader::getArrayNames(){
    vtkSmartPointer<vtkUnstructuredGrid> myGrid = UGReader->GetOutput();

    vtkSmartPointer<vtkCellData> cell_data;

    cell_data = myGrid->GetCellData();

    std::vector<const char*> arrayNames;

    unsigned size = cell_data->GetNumberOfArrays();

    for(unsigned i=0;i<size;i++){
        const char* current_name = cell_data->GetArrayName(i);
        arrayNames.push_back(current_name);

    }

    return arrayNames;


}



// Sets the scalar range for dataSetMapper
//Sets the Color Array being used
void pVTKReader::SetMapperColorArray(vtkSmartPointer<vtkDataSetMapper> &dataSetMapper, int index){ //
    vtkSmartPointer<vtkUnstructuredGrid> myGrid=UGReader->GetOutput();


    vtkSmartPointer<vtkCellData> cell_data;

    cell_data=myGrid->GetCellData();

    int num_arrays = cell_data->GetNumberOfArrays();

    const char* fluence_arr_name;

    fluence_arr_name = cell_data->GetArrayName(index);

    vtkSmartPointer<vtkDataArray> fluence_data = cell_data->GetArray(fluence_arr_name);
    qDebug()<<"Fluence array name is "<<fluence_arr_name;


    int num_components = fluence_data->GetNumberOfComponents();

    qDebug() << "Number of components is: " << num_components;


    double range[2] = {0.0, 0.0};
    fluence_data->GetRange(range);


    qDebug()<<"get maxId "<<fluence_data->GetMaxId();
    qDebug()<<"Range is "<<range[0]<<" "<<range[1];
    maxID = range[1];

    dataSetMapper->SetScalarRange(range);

    dataSetMapper->SelectColorArray(fluence_arr_name);


    //used by set_annotation
    this->dataSetMapper=dataSetMapper;


}


vtkSmartPointer<vtkLogLookupTable> pVTKReader::createLookUpTable(){
       vtkSmartPointer<vtkLogLookupTable> lookup_table = vtkSmartPointer<vtkLogLookupTable>::New();
       lookup_table->SetNumberOfTableValues(256);
       //lookup_table->SetHueRange(0.9,1);
       lookup_table->Build();

       return lookup_table;

}



vtkSmartPointer<vtkLookupTable> pVTKReader::setColorScale(vtkSmartPointer<vtkDataSetMapper> &dataSetMapper){




    vtkSmartPointer<vtkLookupTable> lookupTable2 =
        vtkSmartPointer<vtkLookupTable>::New();

       double range[2];

       dataSetMapper->GetScalarRange(range);

       qDebug()<<"Range in setColorScale is "<<range[1];
      const char* current_name= dataSetMapper->GetArrayName();
      unsigned num_vals = range[1]-range[0]+1;

      //Since labels have to appear at the very top and very bottom
      //We need 1 more label than the number of regions
      //That means the last label is useless and has to show a region number that does not exist
      lookupTable2->SetNumberOfTableValues(range[1]+1);
      qDebug()<<"Number of table values is "<<range[1];


      range[1]=range[1];
     // lookupTable2->SetRange(range);
      lookupTable2->SetRange(0,range[1]+1);
      lookupTable2->SetHueRange(0.677,1);
      lookupTable2->Build();


      return lookupTable2;

}


vtkSmartPointer<vtkScalarBarActor> pVTKReader::setBarColorMap(vtkSmartPointer<vtkLookupTable> lookuptable
                                                           ,vtkSmartPointer<vtkDataSetMapper> mapper,
                                                           int array_index){

    const char* current_name = mapper->GetArrayName();

    qDebug()<<"Current name is"<<current_name;

    vtkSmartPointer<vtkScalarBarActor> color_scale=vtkSmartPointer<vtkScalarBarActor>::New();


    color_scale->SetLookupTable(lookuptable);




    color_scale->SetMaximumNumberOfColors(256);
    double range[2];
    mapper->GetScalarRange(range);


     qDebug()<<"Range[1] is inside setBarColorMap "<<range[1];


    // Check to see if the Array name is region
    //If it is region display as many labels as necessary
    //For Fluence 5 would be sufficient
    unsigned num_vals;
    if(strcmp(current_name,"Region")==0){

        range[1]=range[1]+2;
     }
    else{
        range[1]=5;
    }
    //color_scale->SetNumberOfLabels(range[1]);
    color_scale->SetNumberOfLabels(0);


    vtkStdString my_var= lookuptable->GetAnnotation(2);
    qDebug()<<"my_var value is "<<my_var;

    vtkVariant the_value;


//    lookuptable->SetAnnotation(0.,"0");
   // lookuptable->SetAnnotation(5.,"5");

//    for(unsigned i=0;i<5;i++){
//        vtkStdString value= to_string(i);
//        lookuptable->SetAnnotation(value,value);
//    }


    vtkSmartPointer<vtkUnstructuredGrid> myGrid=UGReader->GetOutput();

    vtkSmartPointer<vtkCellData> cell_data;

    cell_data=myGrid->GetCellData();


    color_scale->SetTitle(cell_data->GetArrayName(array_index));

    //store lookuptable for later use in set_annotations

    this->lin_lookupTable=lookuptable;

    return color_scale;



}

void pVTKReader::set_annotations(vtkSmartPointer<vtkLookupTable> lookuptable
                                 ,vtkSmartPointer<vtkDataSetMapper> mapper,
                                 std::vector<OpticalProperty*> opticalPropertyList,
                                 int index){



    //Get the current Array name

    char* array_name=mapper->GetArrayName();


    qDebug()<<"Inside set_annotations array_name is "<<array_name;

    //Because of Region's discrete nature it has to be handled diffrently
    //Also need to replace numbers with user input labels for regions

    double range[2];
    if(strcmp(array_name,"Region")==0){
        mapper->GetScalarRange(range);

        //Remove all the labels first

        for(unsigned j=0;j<range[1];j++){
            vtkStdString value = to_string(j);
            lookuptable->SetAnnotation(value,"");

        }
        //i declared outside scope to store the value at which iteration stops
        // optical property names fill up the labels first
        //Remaining position are assigned numbers


        unsigned i;
        //for loop prevents from the top edge being labeled
        for(i=0;i<opticalPropertyList.size() && i<=range[1];i++){
            vtkStdString value = to_string(i);
            vtkStdString label = opticalPropertyList[i]->name.toUtf8().constData();
            lookuptable->SetAnnotation(value,label);
        }

        for(unsigned k=i;k<=range[1];k++){
            vtkStdString value = to_string(k);
            lookuptable->SetAnnotation(value,value);
        }


    }


    else{

        // we arbitrarily choose to show 5 numbers only
        mapper->GetScalarRange(range);
        double increment_value = range[1]/4;

        for(unsigned i=0;i<5;i++){
            vtkStdString index = to_string(i);
            vtkStdString value = to_string(i*increment_value);
            lookuptable->SetAnnotation(value,value);
        }



    }

}


void pVTKReader::set_color_scale(vtkSmartPointer<vtkScalarBarActor> color_scale){

    this->color_scale=color_scale;

}


vtkSmartPointer<vtkScalarBarActor> pVTKReader::get_color_scale(){
    return color_scale;
}

vtkSmartPointer<vtkDataSetMapper> pVTKReader::get_dataSetMapper(){
    return dataSetMapper;
}

void pVTKReader::set_dataSetMapper(vtkSmartPointer<vtkDataSetMapper> dataSetMapper){
    this->dataSetMapper = dataSetMapper;
}

vtkSmartPointer<vtkLookupTable> pVTKReader::get_lin_lookupTable(){
    return this->lin_lookupTable;
}


QString pVTKReader::GetReaderFileType() const{
	return cur_file_type;
}


vtkAlgorithmOutput* pVTKReader::GetOutputPort() const{
    if (cur_file_type == "vtk") {
        qDebug() << "return vtk data";
        //return dataReader->GetOutputPort();
        return UGReader->GetOutputPort();
    } else if (cur_file_type == "dcm") {
        return DICOMReader->GetOutputPort();
    } else
		return NULL;
}


////return the image data from DICOM reader
//vtkImageData* pVTKReader::GetImageData() const{
//	assert(DICOMReader != NULL);
//	return DICOMReader->GetOutput();
//}


////return the data set from unstructured grid reader
//vtkUnstructuredGrid* pVTKReader::GetUnstructuredGrid() const{
//	assert(UGReader != NULL);
//	return UGReader->GetOutput();
//}

bool pVTKReader::isReaderEmpty() const{
	return isEmpty;
}



//Return the output object of the current file
vtkDataObject* pVTKReader::GetOutput() const{
    if (cur_file_type == "vtk"){
        //return UGReader->GetOutputDataObject(0);

//        vtkDataObject* data;
//        vtkPolyData* P = vtkPolyData::SafeDownCast(data);
        return dataReader->GetOutputDataObject(0);
    }
	else if (cur_file_type == "dcm")
		return DICOMReader->GetOutputDataObject(0);
}

//Return the scalar range of the object
double* pVTKReader::GetScalarRange() const{
    if (cur_file_type == "vtk"){
        if(dataReader->IsFileUnstructuredGrid()){
            vtkUnstructuredGridReader* ugReader
                    = vtkUnstructuredGridReader::SafeDownCast(dataReader);
            return ugReader->GetOutput()->GetScalarRange();
        }else if (dataReader->IsFilePolyData()){
            vtkSmartPointer<vtkPolyDataReader> polyReader
                    = vtkPolyDataReader::SafeDownCast(dataReader);
            return polyReader->GetOutput()->GetScalarRange();
        }
    }
	else if (cur_file_type == "dcm")
		return DICOMReader->GetOutput()->GetScalarRange();
}

vtkSmartPointer<vtkUnstructuredGridReader> pVTKReader::getUGReader(){
    return this->UGReader;
}

vtkSmartPointer<vtkThreshold> pVTKReader::getFilter(){
    return this->filter;
}

int pVTKReader::getMax(){
    int max = maxID;
    cout << "max is " << max <<"getting max from getmax function" <<endl;
    return max;
}






