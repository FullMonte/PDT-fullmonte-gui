#pragma once

#include "pVTKReader.h"
#include <assert.h>
#include <vtkMapper.h>
#include <vtkDataSetMapper.h>
#include <vtkVolumeMapper.h>
#include <vtkUnstructuredGridVolumeMapper.h>
#include <vtkUnstructuredGridVolumeRayCastMapper.h>
#include <vtkUnstructuredGridGeometryFilter.h>
#include <vtkUnstructuredGridVolumeZSweepMapper.h>
#include <vtkProjectedTetrahedraMapper.h>
#include <vtkDataSetTriangleFilter.h>
#include <vtkSmartPointer.h>

/*
* This class is used to return the correspoinding mapper 
* according to the reader and reader file type
*/
class pVTKMapper
{
private:

	vtkSmartPointer<vtkMapper> mapper;

	vtkSmartPointer<vtkUnstructuredGridVolumeMapper> UGVolumeMapper;

public:
    pVTKMapper();

    ~pVTKMapper();

	//Map the data into corresponding mapper from reader for actor use
    vtkSmartPointer<vtkMapper> GetMapper(const pVTKReader reader);

	//Return an unstructured grid volume mapper for volume rendering
    vtkSmartPointer<vtkUnstructuredGridVolumeMapper> GetUGVolumeMapper(const pVTKReader reader);



};

