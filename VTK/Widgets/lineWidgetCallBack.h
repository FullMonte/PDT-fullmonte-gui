#ifndef LINEWIDGETCALLBACK_H
#define LINEWIDGETCALLBACK_H

#include <iostream>
#include <sstream>

#include <QLineEdit>
#include <QDebug>

#include <vtkCommand.h>
#include <vtkLineWidget.h>
#include <vtkSmartPointer.h>

#include "LightSource/PDTVtkPlanePlacement.h"

class FreeLineWidgetCallBack : public vtkCommand
{
	//This is the call back class used to get end points of line widget
public:
    static FreeLineWidgetCallBack *New()
	{
        return new FreeLineWidgetCallBack;
	}
public:
    FreeLineWidgetCallBack(){}

    void setLineEdits (QLineEdit* point1LineEdits[3], QLineEdit* point2LineEdits[3]) {
        for(int i = 0; i < 3; i++){
            this->point1LineEdits[i] = point1LineEdits[i];
            this->point2LineEdits[i] = point2LineEdits[i];
        }
    }

	virtual void Execute(vtkObject *caller, unsigned long eventId, void *callData){
        vtkLineWidget *lineWidget = reinterpret_cast<vtkLineWidget*>(caller);
        double endpoint1[3];
        double endpoint2[3];
        lineWidget->GetPoint1(endpoint1);
        lineWidget->GetPoint2(endpoint2);
        QString QS_point1[3];
        QString QS_point2[3];
        for (int i = 0; i < 3; i++){
            QS_point1[i].sprintf("%f", endpoint1[i]);
            point1LineEdits[i]->setText(QS_point1[i]);
            QS_point2[i].sprintf("%f", endpoint2[i]);
            point2LineEdits[i]->setText(QS_point2[i]);
        }
	}

private:
    QLineEdit *point1LineEdits[3];
    QLineEdit *point2LineEdits[3];

};

class PlanePlacementLineWidgetCallBack : public vtkCommand
{
    //This is the call back class used to get end points of line widget
public:
    static PlanePlacementLineWidgetCallBack *New()
    {
        return new PlanePlacementLineWidgetCallBack;
    }
public:
    PlanePlacementLineWidgetCallBack(){}

    void setLineEdits (QLineEdit* planePosLineEdits[2], QLineEdit* depthsLineEdits[2]) {
        for(int i = 0; i < 2; i++){
            this->planePosLineEdits[i] = planePosLineEdits[i];
            this->depthsLineEdits[i] = depthsLineEdits[i];
        }
    }

    void setPlanePlacement(PDTVtkPlanePlacement* planePlacement) {
        assert(planePlacement != nullptr);
        this->planePlacement = planePlacement;
    }

    virtual void Execute(vtkObject *caller, unsigned long eventId, void *callData){
        vtkLineWidget *lineWidget = reinterpret_cast<vtkLineWidget*>(caller);
        double endpoint1[3], endpoint2[3];
        lineWidget->GetPoint1(endpoint1);
        lineWidget->GetPoint2(endpoint2);

        // Project to plane placement
        assert(planePlacement != nullptr);
        double planePos1[3], planePos2[3];
        planePlacement->projectToPlane(endpoint1, planePos1);
        planePlacement->projectToPlane(endpoint2, planePos2);

        double planePos[2] = {(planePos1[0]+planePos2[0])/2.0, (planePos1[1]+planePos2[1])/2.0};
        double depths[2] = {planePos1[2], planePos2[2]};
        QString qs_planePos[2] = {QString::number(planePos[0]), QString::number(planePos[1])};
        QString qs_depths[2] = {QString::number(depths[0]), QString::number(depths[1])};

        for (int i = 0; i < 2; i++){
            planePosLineEdits[i]->setText(qs_planePos[i]);
            depthsLineEdits[i]->setText(qs_depths[i]);
        }

        qDebug() << "Line source placement callback debug";
        qDebug() << "Plane Position 1 (" << planePos1[0] << "," << planePos1[1] << "," << planePos1[2] << ")" ;
        qDebug() << "Plane Position 2 (" << planePos2[0] << "," << planePos2[1] << "," << planePos2[2] << ")" ;
    }

private:
    QLineEdit *planePosLineEdits[2];
    QLineEdit *depthsLineEdits[2];
    PDTVtkPlanePlacement * planePlacement;

};

#endif 
