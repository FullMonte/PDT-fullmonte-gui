#ifndef POINTWIDGETCALLBACK_H
#define POINTWIDGETCALLBACK_H

#include <iostream>
#include <sstream>

#include <QLineEdit>
#include <QDebug>

#include <vtkCommand.h>
#include <vtkPointWidget.h>
#include <vtkSmartPointer.h>

class FreePointWidgetCallBack : public vtkCommand{


public:
    static FreePointWidgetCallBack *New(){
        return new FreePointWidgetCallBack;
    }

public:
    FreePointWidgetCallBack(){}

    void setPointEdit (QLineEdit* PointPositionEdit[3]){
        for(int i=0;i<3;i++){
            this->pointPositionEdit[i] = PointPositionEdit[i];
        }
    }

    virtual void Execute(vtkObject *caller,unsigned long eventId,void *callData){
        qDebug()<<"Inside callback execute";

        vtkPointWidget *pointWidget = reinterpret_cast<vtkPointWidget*>(caller);
        double pointPositon[3];
        pointWidget->GetPosition(pointPositon);
        QString QS_point[3];
        qDebug()<<pointPositon[0]<<" "<<pointPositon[1]<<" "<<pointPositon[2];
        for(int i=0;i<3;i++){

            QS_point[i].sprintf("%f",pointPositon[i]);
            pointPositionEdit[i]->setText(QS_point[i]);

        }



    }


private:
    QLineEdit *pointPositionEdit[3];

};


#endif // POINTWIDGETCALLBACK_H
