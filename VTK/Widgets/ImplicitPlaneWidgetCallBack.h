#ifndef IMPLICITPLANECALLBACK_H
#define IMPLICITPLANECALLBACK_H

#include <vtkAlgorithm.h>
#include <vtkSmartPointer.h>
#include <vtkPlane.h>
#include <vtkImplicitPlaneWidget.h>

#include <vtkCommand.h>

#include <QDebug>

class VTKImplicitPlaneWidgetCall : public vtkCommand {
    //this is the call back class we use to get the clipped data every time
public:

    static VTKImplicitPlaneWidgetCall *New()
    {
        return new VTKImplicitPlaneWidgetCall;
    }
public:
    virtual void Execute(vtkObject *caller, unsigned long eventId, void *callData)
    {
        (void)eventId;
        (void*)callData;

        vtkImplicitPlaneWidget *pWidget = reinterpret_cast<vtkImplicitPlaneWidget*>(caller);
        {
            // update the clip plane and the second renderer
            pWidget->GetPlane(m_plane);
            m_algorithm->Update();
        }
    }


    void setPlane(vtkPlane* other){ m_plane = other; }

    void setAlgorithm(vtkAlgorithm* other) { m_algorithm = other; }
private:
    vtkPlane* m_plane;

    vtkAlgorithm* m_algorithm;
};


#endif
