#ifndef PLANEWIDGETCALLBACK_H
#define PLANEWIDGETCALLBACK_H

#include <QLineEdit>
#include <vtkCommand.h>
#include <vtkPlaneWidget.h>
#include <vtkSmartPointer.h>

class PlaneWidgetCallBack : public vtkCommand
{
	//This is the call back class used to get end points of line widget
public:
	static PlaneWidgetCallBack *New()
	{
		return new PlaneWidgetCallBack;
	}
public:
    PlaneWidgetCallBack(){}

    void setLineEdits (QLineEdit* centerLineEdits[3], QLineEdit* normalLineEdits[3]) {
        for(int i = 0; i < 3; i++){
            this->centerLineEdits[i] = centerLineEdits[i];
            this->normalLineEdits[i] = normalLineEdits[i];
        }
    }

	virtual void Execute(vtkObject *caller, unsigned long eventId, void *callData){
        vtkPlaneWidget *planeWidget = reinterpret_cast<vtkPlaneWidget*>(caller);
        double center[3];
        double normal[3];
        planeWidget->GetCenter(center);
        planeWidget->GetNormal(normal);
        QString QS_center[3];
        QString QS_normal[3];
        for (int i = 0; i < 3; i++){
            QS_center[i].sprintf("%f", center[i]);
            centerLineEdits[i]->setText(QS_center[i]);
            QS_normal[i].sprintf("%f", normal[i]);
            normalLineEdits[i]->setText(QS_normal[i]);
        }
	}

private:
    QLineEdit *centerLineEdits[3];
    QLineEdit *normalLineEdits[3];

};

#endif
