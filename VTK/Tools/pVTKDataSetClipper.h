#ifndef PVTKDATASETCLIPPER_H
#define PVTKDATASETCLIPPER_H

#include "pVTKTool.h"

#include <vtkClipDataSet.h>
#include <vtkImplicitPlaneWidget.h>
#include <vtkPlane.h>

class pVTKDataSetClipper : public pVTKTool {
public:
    pVTKDataSetClipper();
    ~pVTKDataSetClipper();

    virtual void update() override;

private:
    void setupWidget(vtkImplicitPlaneWidget* planeWidget, vtkPlane* planeFunc, vtkClipDataSet* clipper);
    void setupAlgorithm(vtkClipDataSet* clipper, vtkPlane* planeFunc);

};


#endif
