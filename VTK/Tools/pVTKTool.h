#ifndef PVTKTOOL_H
#define PVTKTOOL_H

#include <vtk3DWidget.h>
#include <vtkActor.h>
#include <vtkAlgorithm.h>
#include <vtkImplicitFunction.h>
#include <vtkSmartPointer.h>

#include "VTK/Core/pVTKReader.h"

class pVTKTool {
public:
    void setActor(vtkActor* actor) { dataActor = actor; }
    void setInput(pVTKReader* reader) { this->reader = reader; }
    void setRenderer(vtkRenderer* renderer) { this->renderer = renderer; }

    vtkActor* getActor() { return dataActor; }
    pVTKReader* getInput() { return reader; }
//     Set tool enable/disabled
    void setEnabled(bool enabled) {
        if (enabled) {
            toolWidget->On();
        } else {
            toolWidget->Off();
        }
    }

    virtual void update() = 0 ;

protected:
    vtkSmartPointer<vtk3DWidget> toolWidget;
    vtkSmartPointer<vtkAlgorithm> toolAlgorithm;
    vtkSmartPointer<vtkImplicitFunction> algorithmFunction;

    pVTKReader* reader;
    vtkActor* dataActor;
    vtkRenderer* renderer;

};

#endif
