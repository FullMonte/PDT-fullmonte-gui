#include "pVTKDataSetClipper.h"

#include <assert.h>

#include <vtkCommand.h>
#include <vtkDataSetMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>

#include "VTK/Widgets/ImplicitPlaneWidgetCallBack.h"

pVTKDataSetClipper::pVTKDataSetClipper() {
    toolWidget = vtkSmartPointer<vtkImplicitPlaneWidget>::New();
    toolAlgorithm = vtkSmartPointer<vtkClipDataSet>::New();
    algorithmFunction =  vtkSmartPointer<vtkPlane>::New();
}

pVTKDataSetClipper::~pVTKDataSetClipper() {

}
void pVTKDataSetClipper::update() {
    assert(dataActor != nullptr);
    assert(reader != nullptr);
    assert(renderer != nullptr);

    vtkImplicitPlaneWidget* implicitPlane = vtkImplicitPlaneWidget::SafeDownCast(toolWidget.Get());
    vtkClipDataSet* clipper = vtkClipDataSet::SafeDownCast(toolAlgorithm.Get());
    vtkPlane* planeFunc = vtkPlane::SafeDownCast(algorithmFunction.Get());

    setupWidget(implicitPlane, planeFunc, clipper);
    setupAlgorithm(clipper, planeFunc);
}

void pVTKDataSetClipper::setupWidget(vtkImplicitPlaneWidget* implicitPlane, vtkPlane* planeFunc, vtkClipDataSet* clipper) {
	implicitPlane->SetPlaceFactor(1.25);
	implicitPlane->OutlineTranslationOff();
	implicitPlane->OriginTranslationOff();
	implicitPlane->DrawPlaneOff();
    implicitPlane->SetCurrentRenderer(renderer);
    implicitPlane->SetInteractor(renderer->GetRenderWindow()->GetInteractor());
    implicitPlane->SetProp3D(dataActor);

    vtkSmartPointer<VTKImplicitPlaneWidgetCall> pCall = vtkSmartPointer<VTKImplicitPlaneWidgetCall>::New();
    pCall->setPlane(planeFunc);
    pCall->setAlgorithm(clipper);
    implicitPlane->AddObserver(vtkCommand::EndInteractionEvent, pCall);

    implicitPlane->PlaceWidget();
}

void pVTKDataSetClipper::setupAlgorithm(vtkClipDataSet* clipper, vtkPlane* planeFunc) {
    assert(reader->GetOutputPort() != nullptr);
    clipper->SetInputConnection(reader->GetOutputPort());
	clipper->SetClipFunction(planeFunc);

    vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
    mapper->SetInputConnection(clipper->GetOutputPort());
    dataActor->SetMapper(mapper);

	clipper->Update();
}

