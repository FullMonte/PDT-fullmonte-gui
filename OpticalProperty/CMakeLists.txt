add_subdirectory(IO)
SET(CMAKE_INCLUDE_CURRENT_DIR ON)

Set(OpticalProperty_SRC
    OpticalProperty.cpp
    OpticalProperty.h






    )


add_library(OpticalProperty ${OpticalProperty_SRC})
set_target_properties(OpticalProperty PROPERTIES LINKER_LANGUAGE CXX)
qt5_use_modules(OpticalProperty Core Widgets Gui)
#qt5_use_modules(PQtWidgets Core Gui Widgets)
