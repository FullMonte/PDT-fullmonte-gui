#include "OpticalProperty.h"

#include <QMessageBox>

#include <QDebug>


OpticalProperty::OpticalProperty()
{

}

OpticalProperty::OpticalProperty(QString Name,QString absorp_coeff,QString scattering_coeff,QString refractive_index,QString anisotropy_factor){

    this->name=Name;
    this->absorption=absorp_coeff;
    this->scattering=scattering_coeff;
    this->refractive_index=refractive_index;
    this->anistropy_factor=anisotropy_factor;

}


OpticalProperty::OpticalProperty(QString Name){
    this->name=Name;
}




std::map<QString, QString> OpticalProperty::get_property_map(){
    std::map<QString,QString> property_map;



    property_map["scatteringCoeff"]=this->scattering;
    property_map["absorptionCoeff"]=this->absorption;
    property_map["refractiveIndex"]=this->refractive_index;
    property_map["anisotropy"]=this->anistropy_factor;


    return property_map;

}



void OpticalProperty::set_scattering(QString scatter){
    this->scattering=scatter;
}

void OpticalProperty::set_absorption(QString absorp){
    this->absorption=absorp;
}

void OpticalProperty::set_refraction(QString refract){
    this->refractive_index=refract;

}

void OpticalProperty::set_anisotropy(QString aniso){
    this->anistropy_factor=aniso;
}


bool OpticalProperty::check_property_vals(){


    bool ok;
    double result;
    result=this->absorption.toDouble(&ok);
    qDebug()<<"result is "<<result;
    if(!ok){
        this->message_box("Absorption coefficient must be a number");
        return false;
    }

    if(result < 0){
        this->message_box("Absorption coefficient must be >= 0");
        return false;
    }



    result = this->scattering.toDouble(&ok);
    if(!ok){
        this->message_box("Scattering coeffcient must be a number");
        return false;
    }
    if(result<0){
        this->message_box("Scattering coefficinet must be >= 0");
        return false;
    }


    result = this->anistropy_factor.toDouble(&ok);

    if(!ok){
        this->message_box("Anisotropy factor must be a number");
        return false;
    }
    if(result < -1 || result>1){
        this->message_box("Anistropy factor must be within 1 and -1 (inclusive)");
        return false;
    }




    result = this->refractive_index.toDouble(&ok);

    if(!ok){
        this->message_box("Refractive index must be a number");
        return false;
    }

    if(result < 1){
        this->message_box("Refractive index must be >= 1");
        return false;
    }




    return true;
}


void OpticalProperty::message_box(QString message){
    QMessageBox my_box;
    my_box.setText(message);
    my_box.exec();

}

