#include "OpticalPropertyListIO.h"

//XML titles

const QString OpticalPropertyListIO::PROPERTY="property";
const QString OpticalPropertyListIO::OPTICALLIST="opticallist";


//Assigning values to attributes

const QString OpticalPropertyListIO::ID="id";
const QString OpticalPropertyListIO::NAME="name";
const QString OpticalPropertyListIO::ABSORPTION="absorption";
const QString OpticalPropertyListIO::ANISTROPY_FACTOR="anistropy_factor";
const QString OpticalPropertyListIO::REFRACTIVE_INDEX="refractive_index";
const QString OpticalPropertyListIO::SCATTERING="scattering";




