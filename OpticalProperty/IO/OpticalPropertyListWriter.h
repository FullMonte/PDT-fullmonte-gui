#ifndef OPTICALPROPERTYLISTWRITER_H
#define OPTICALPROPERTYLISTWRITER_H

#include "OpticalPropertyListIO.h"
#include <QString>
#include <OpticalProperty/OpticalProperty.h>
#include <QXmlStreamWriter>

class OpticalPropertyListWriter: public OpticalPropertyListIO
{
public:
    OpticalPropertyListWriter(const QString filepath);

    bool writeOpticalPropertyList(std::vector<OpticalProperty*> OpticalPropertyList);

private:
    void writeOpticalProperty(QXmlStreamWriter &, std::vector<OpticalProperty*>&,int );

};

#endif // OPTICALPROPERTYLISTWRITER_H
