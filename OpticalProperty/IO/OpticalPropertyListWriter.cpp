#include "OpticalPropertyListWriter.h"
#include "OpticalPropertyListIO.h"

#include <QFile>
#include <QDebug>

OpticalPropertyListWriter::OpticalPropertyListWriter(const QString filepath):OpticalPropertyListIO(filepath)
{

}

bool OpticalPropertyListWriter::writeOpticalPropertyList(std::vector<OpticalProperty*> OpticalPropertyList){

    QFile file(m_filePath);
    if(! file.open((QIODevice::WriteOnly))){
        qDebug()<<"Failed to write to file (optical property)";
        return false;

    }

    QXmlStreamWriter stream(&file);
    stream.setAutoFormatting(true);
    stream.writeStartDocument();
    stream.writeStartElement(OPTICALLIST);

    unsigned list_size = OpticalPropertyList.size();

    for(unsigned i=0;i<list_size;i++){
        stream.writeStartElement(PROPERTY);
        stream.writeAttribute(NAME,OpticalPropertyList[i]->name);



        stream.writeEndElement(); // end of one property

    }

    stream.writeEndElement();// end of OPTICALLIST
    stream.writeEndDocument();
    file.close();
    return true;
}


void OpticalPropertyListWriter::writeOpticalProperty(QXmlStreamWriter & s, std::vector<OpticalProperty*> & list, int sid){

    //list[sid]->toXml(s);
}
