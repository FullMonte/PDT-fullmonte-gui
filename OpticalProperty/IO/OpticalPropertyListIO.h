#ifndef OPTICALPROPERTYLISTIO_H
#define OPTICALPROPERTYLISTIO_H

#include <QString>

class OpticalPropertyListIO
{
public:
    OpticalPropertyListIO(const QString filePath):m_filePath(filePath){}


    //Xml title
    static const QString PROPERTY;
    static const QString OPTICALLIST;
    //Attributes

    static const QString ID;
    static const QString NAME;
    static const QString ABSORPTION;
    static const QString SCATTERING;
    static const QString ANISTROPY_FACTOR;
    static const QString REFRACTIVE_INDEX;



protected:
QString m_filePath;
};

#endif // OPTICALPROPERTYLISTIO_H
