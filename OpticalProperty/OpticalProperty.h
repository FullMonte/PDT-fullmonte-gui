#ifndef OPTICALPROPERTY_H
#define OPTICALPROPERTY_H

#include <string>
#include <QString>
#include <map>


class OpticalProperty
{
public:
    OpticalProperty();
    OpticalProperty(QString Name,QString absorp_coeff,QString scattering_coeff,QString refractive_index,QString anisotropy_factor);
    OpticalProperty(QString);

    std::map<QString,QString> get_property_map();



    void set_scattering(QString);
    void set_absorption(QString);
    void set_refraction(QString);
    void set_anisotropy(QString);

    bool check_property_vals();
    void message_box(QString message);




    QString name;
    QString absorption;
    QString scattering;
    QString anistropy_factor;
    QString refractive_index;

};

#endif // OPTICALPROPERTY_H
