#include "TclProjInfo.h"

TclProjInfo::TclProjInfo()
{

}


TclProjInfo::TclProjInfo(QString proj_dir,QString tcl_file_path,
                         QString src_file_path,QString opt_file_path,
                         QString mesh_path_fie_path){


    this->proj_dir = proj_dir;
    this->tcl_file_path = tcl_file_path;
    this->src_file_path=src_file_path;
    this->opt_file_path=opt_file_path;
    this->mesh_path_file_path=mesh_path_fie_path;
}


void TclProjInfo::set_proj_dir(QString path){
    proj_dir = path;
}

void TclProjInfo::set_tcl_file_path(QString path){
    tcl_file_path=path;
}

void TclProjInfo::set_opt_file_path(QString path){
    opt_file_path=path;
}

void TclProjInfo::set_src_file_path(QString path){
    src_file_path=path;
}

void TclProjInfo::set_mesh_path_file_path(QString path){
    mesh_path_file_path = path;
}


QString TclProjInfo::get_proj_dir(){
    return proj_dir;
}

QString TclProjInfo::get_tcl_file_path(){
    return tcl_file_path;
}

QString TclProjInfo::get_src_file_path(){
    return src_file_path;
}

QString TclProjInfo::get_opt_file_path(){
    return opt_file_path;
}


QString TclProjInfo::get_mesh_path_file_path(){
    return mesh_path_file_path;
}





