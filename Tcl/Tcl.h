#ifndef TCL_H
#define TCL_H

#include <QString>
#include <QFile>
#include <QTextStream>
#include <OpticalProperty/OpticalProperty.h>
#include "Qt/Widgets/PDTWorkPageWidget.h"
#include "LightSource/SourceList.h"
#include "LightSource/pdtvtkpointsource.h"
#include "LightSource/PDTVtkLineSource.h"
#include "LightSource/PDTVtkFiberSource.h"
#include "TclProjInfo.h"
#include <vtkPointWidget.h>


#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkAxesActor.h>
#include <vtkOrientationMarkerWidget.h>
#include <QVTKWidget.h>
#include <QEventLoop>
#include "Qt/Widgets/scriptselector.h"
#include <QDir>




#include "vtkSmartPointer.h"

class Tcl : public QObject

{

    Q_OBJECT
public:
    //Tcl(QObject *parent):QObject(parent){}
    explicit Tcl(QObject* parent=0);
    Tcl(PDTWorkPageWidget* currWorkPage, simtype simultype=none);
    virtual ~Tcl();
    bool set_proj_dir();
    bool setup_file(bool);
    void run_full_monte();
    void get_mesh_path(bool);
    void create_material_set();
    void create_optical_file();
    void create_source();
    void create_source_file();
    void create_source_file_multiple();
    bool set_up_kernel(bool);
    void results();
    void energy_to_fluence();
    std::vector<QString> mesh_fluence_append();
    void fluence_to_text(std::vector<QString> output_info);
    void lineFluence_to_text(std::vector<double> output_info);
    void set_GUI_parent_dir();
    void set_current_working_dir();
    void check_AVX();
    void set_Mount_path();
    void container_command_file();
    void copy_files_to_mock();
    void execute();
    void post_execution();
    bool check_fullmonte_success();
    void check_proj_dir_output_files(QString vtk_file_path,QString phi_v_file_path);
    //also sets the mesh_file_path_file
    void copy_files_from_mount(QString,QString,QString);
    void delete_files_from_mount(QString,QString,QString,bool);

    void visualization();
    void Test();

    int return_decimal(int sci_not);

    void create_execute_docker();

    void set_proj_output_dir();

    void classification();

    void getLineInfo(vector<double> line);

    QString getProjDir();

    void setRun_FM(bool flag);

    void set_fluence_tcl_file();
    void set_Matrix_Reader_to_text();

    // Constant characters

    const QString MATERIAL= "Material";
    const QString TAB="   ";
    const QString SPACE=" ";
    const QString QUOTE= "\"";
    const QString CURLY_BRACKET_OPEN="{";
    const QString CURLY_BRACKET_CLOSE="}";
    const QString DOLLAR_SIGN="$";
    const QString SLASH="/";



private:

    double* line;
    bool run_FM = false;
    QString proj_dir;
    QString proj_output_dir;
    QString GUI_parent_dir;
    QString working_dir;
    QFileInfo* tcl_info;
    QFileInfo* opt_info;
    QFileInfo* src_info;

    QFile* tcl_file;
    QFile* fluence_tcl_file;
    QFile* sources_file;
    QFile* optical_file;
    QFile* mesh_file_path_file;

    TclProjInfo* tcl_proj_info;

    bool AVX;


    QString docker_container_path="/home/ninan3/native_build/FullMonteSW/Build/ReleaseGCC";
    QString SOURCE_COMMAND = "source";
    PDTWorkPageWidget* workPageWidget;
    const std::vector< QString> optical_property_names={"scatteringCoeff",
                                                          "absorptionCoeff",
                                                          "refractiveIndex",
                                                          "anisotropy"};


    simtype simultype=none;








    // Docker .sh file path
    const QString AVX_path="/usr/local/FullMonteSW-AVX/bin/tclmonte.sh";
    const QString AVX2_path="tclmonte.sh";
    const QString AVX_name = "AVX.txx";
    const QString AVX2_name = "AVX2.txx";
    const QString container_command_name ="container_command.txt";
    const QString execute_docker_name = "execute_docker.sh";


    const QString check_AVX_command ="check_AVX/./check_AVX.sh";
    const QString AVX_file_path="check_AVX/check_AVX.sh";


    QString dummy;

    QProcess* get_parent_dir;


private slots:
    //void process_finished();
    void Avx_output();
    void parent_dir_output();




};

#endif // TCL_H
