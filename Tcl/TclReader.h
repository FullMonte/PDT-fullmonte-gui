#ifndef TCLREADER_H
#define TCLREADER_H
#include<QString>
#include "OpticalProperty/OpticalProperty.h"
#include "LightSource/PDTVtkSource.h"
#include "LightSource/PDTVtkLineSource.h"
#include "LightSource/pdtvtkpointsource.h"
#include "LightSource/PDTVtkFiberSource.h"
#include "LightSource/SourceList.h"
#include "TclProjInfo.h"

#include "Qt/Widgets/PDTWorkPageWidget.h"





class TclReader
{
public:
    TclReader();
    TclReader(PDTWorkPageWidget* currWorkpage);
    std::vector<OpticalProperty*> optical_reader();
    OpticalProperty* first_word_in_optical_property_list(QString first_word);
    QStringList split_words(QString line);

    std::vector<PDTVtkSource*> source_reader();
    QString quotation_pos(QString);

    PDTVtkSource* source_in_sources(QString);
    QString sourceType_in_sources(QString source_name);


    double* get_point_between_quotes(QString position);

    bool is_point(QString);

    void test_function();

    bool set_proj_dir();

    std::vector<PDTVtkSource*> create_source_list();

    void set_PDTWorkPageWidget(PDTWorkPageWidget* currentWorkPageWidget);

    void set_WorkPage_TclProjInfo();

    QString read_mesh_path();

    void set_TclProjInfo(TclProjInfo* proj_info);


    TclProjInfo* get_project_info();


private:
    //list of optical properties
    PDTWorkPageWidget* curWorkPageWidget;
    TclProjInfo* proj_info;
    QString proj_dir;
    QString tcl_file_path;
    QString opt_file_path;
    QString src_file_path;
    QString mesh_path_file_path;
    std::vector<OpticalProperty *> optical_properties;
    //std::vector<std::pair<PDTVtkSource*,QString>> sources;
    std::vector<std::tuple<PDTVtkSource*,QString,QString>> sources;
    //change data structure from pair to tuple in order to save additional information
    //previously store PDTVtkSource,Name of Source, now PDTVtkSource,Name of Source,Type of source
    //purpose of doing this is because need to know which power belongs to which type of PDTVTkSource



    //constant strings
    const QString MATERIAL="Material";
    const QString SCATTERING="scatteringCoeff";
    const QString ABSORPTION="absorptionCoeff";
    const QString REFRACTIVE="refractiveIndex";
    const QString ANISOTROPY="anisotropy";



    const QString TAB="   ";
    const QString SPACE=" ";
    const QString QUOTE= "\"";
    const QString SLASH="/";


    const QString LINE="Line";
    const QString POINT="Point";
    const QString FIBER="Fiber";
    const QString POSITION="position";
    const QString SETENDPOINT="endpoint";
    const QString FIBERPOS = "fiberPos";
    const QString FIBERDIR = "fiberDir";
    const QString RADIUS = "radius";
    const QString NA = "numericalAperture";
    const QString POWER = "power";


    double default_pos[3]={0.0,0.0,0.0};
    double default_num = 0.01;

};

#endif // TCLREADER_H
