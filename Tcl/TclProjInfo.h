#ifndef TCLPROJINFO_H
#define TCLPROJINFO_H

#include <QString>

class TclProjInfo
{
public:
    TclProjInfo();
    TclProjInfo(QString proj_dir,QString tcl_file_path,
                             QString src_file_path,QString opt_file_path,
                             QString mesh_path_file_path);

    void set_proj_dir(QString);
    void set_tcl_file_path(QString);
    void set_src_file_path(QString);
    void set_opt_file_path(QString);
    void set_mesh_path_file_path(QString);


    QString get_proj_dir();
    QString get_tcl_file_path();
    QString get_src_file_path();
    QString get_opt_file_path();
    QString get_mesh_path_file_path();



private:
    QString proj_dir;
    QString tcl_file_path;
    QString src_file_path;
    QString opt_file_path;
    QString mesh_path_file_path;

};

#endif // TCLPROJINFO_H
