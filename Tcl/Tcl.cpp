#include "Tcl.h"
#include <QMessageBox>
#include <string>
#include <QInputDialog>
#include <QProcess>
#include <iostream>
#include <QProcess>
#include <QFileDialog>
#include <QEventLoop>
#include <QCoreApplication>
#include <vtkSphereSource.h>
#include <QSignalMapper>
#include "Qt/Widgets/scriptselector.h"
#define GetCurrentDir _getcwd


/*This class generates .tcl files that enable the running of fullmonte
 * A large portion of this code is hardcoded text
 * And the parameters passed in from the gui "fill the blanks"*/

Tcl::Tcl(QObject *parent):
    QObject(parent)
{

}

Tcl::~Tcl()
{
    //delete this;
}


// Creates 3 files:
// .tcl , .src , .opt
// .src and .opt concatenated in .tcl

Tcl::Tcl(PDTWorkPageWidget* currWorkpage,simtype simultype){
    // set the current current page widget
    workPageWidget = currWorkpage;
    this->simultype= simultype;


}

// set project directory and 3 .tcl files

bool Tcl::set_proj_dir(){

    bool valid = false;
    tcl_proj_info = workPageWidget->getTclProjInfo();

    // If proj_dir is not already chosen
    // after the if statement tcl_proj_info is valid regardless of which path taken
    QString tcl_file_path;
    if(tcl_proj_info==NULL){
        tcl_file_path = QFileDialog::getSaveFileName(workPageWidget);
        // exit if the user chooses cancel
        if(tcl_file_path.isEmpty()){
            return valid;
        }
        //QFileInfo tcl_info(tcl_file_path+".tcl");
        tcl_info = new QFileInfo(tcl_file_path + ".tcl");
        proj_dir = tcl_info->absoluteDir().absolutePath();

        tcl_proj_info = new TclProjInfo();
        tcl_proj_info->set_proj_dir(proj_dir);
        tcl_proj_info->set_tcl_file_path(tcl_file_path+".tcl");
        tcl_proj_info->set_src_file_path(tcl_file_path+"_src.tcl");
        tcl_proj_info->set_opt_file_path(tcl_file_path+"_opt.tcl");


        //Project is set. Info has to be stored in PDTWorkPageWidget
        workPageWidget->setTclProjInfo(tcl_proj_info);

    }


    proj_dir = tcl_proj_info->get_proj_dir();
    tcl_file = new QFile(tcl_proj_info->get_tcl_file_path());
    tcl_info = new QFileInfo(*tcl_file);

    sources_file = new QFile(tcl_proj_info->get_src_file_path());
    src_info = new QFileInfo(*sources_file);

    optical_file = new QFile(tcl_proj_info->get_opt_file_path());
    opt_info = new QFileInfo(*optical_file);




    valid = true;
    return(valid);

}

// function used to save project state
// Optical properties and source

bool Tcl::setup_file(bool save){
    // especify project directory and 3 .tcl file paths
    bool valid = set_proj_dir();
    if(!valid){
        return false;
    }

    if(run_FM){
        fluence_tcl_file->open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out2(fluence_tcl_file);
        out2 << "package require FullMonte" << endl;
        fluence_tcl_file->close();
        this->get_mesh_path(false);//write into _phi.tcl
        this->set_Matrix_Reader_to_text();//write reader to _phi.tcl
    }



    else{
        tcl_file->open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(tcl_file);
        out<<"package require FullMonte"<<endl;
        tcl_file->close();
        //write into tcl
        this->get_mesh_path(true);//write into .tcl
        this->create_material_set();
        this->create_source();
        valid =this->set_up_kernel(save);
        //check for user pressing on cancel
        if(!valid){
            return false;
        }
        this->results();
        this->energy_to_fluence();
        std::vector<QString> file_info=this->mesh_fluence_append();
        this->fluence_to_text(file_info);
    }

    workPageWidget->set_slider_label();
    line = workPageWidget->getline();
    vector<double> line_info;
    for(int i = 0; i< 6 ;i++){
        line_info.push_back(*(line+i));
    }
    this->lineFluence_to_text(line_info);

}

//function used to run fullmonte
void Tcl::run_full_monte(){
    this->check_AVX();//check AVX/AVX2
    this->set_GUI_parent_dir();
    this->set_current_working_dir();
    this->copy_files_to_mock();//copy files need to project dir
    this->execute();
    this->post_execution();



}

//write VTKMeshReader to .tcl
void Tcl::get_mesh_path(bool tcl){

    // set fn "FILEPATH"
    QFileInfo fileInfo = workPageWidget->getMeshFileInfo();
    QTextStream out;
    if(tcl){
        tcl_file->open(QIODevice::Append | QIODevice::Text);
        out.setDevice(tcl_file);
    }
    else{
        fluence_tcl_file->open(QIODevice::Append | QIODevice::Text);
        out.setDevice(fluence_tcl_file);
    }


    QString file_path = proj_dir + "/"+tcl_info->baseName()+".mesh.vtk";
    QString final = "set fn" + SPACE + QUOTE + file_path + QUOTE;
    out<<final<<endl;
    out<<endl;

    out<<"VTKMeshReader R"<<endl;
    const QString TAB="   ";
    out<<TAB + "R filename $fn"<<endl;
    out<<TAB + "R read"<<endl;
    out<<"set M [R mesh]"<<endl;
    if(tcl)
    tcl_file->close();
    else
    fluence_tcl_file->close();

    // store the filepath to the mesh inside the project directory
    if(tcl){
    mesh_file_path_file = new QFile(tcl_proj_info->get_mesh_path_file_path());
    mesh_file_path_file->open(QIODevice::Text | QIODevice::WriteOnly);
    QTextStream out_mesh_file(mesh_file_path_file);
    out_mesh_file<<fileInfo.absoluteFilePath();
    mesh_file_path_file->close();
    }

}

//write Material MS to .tcl
void Tcl::create_material_set(){


    std::vector<OpticalProperty*> opticalPropertyList=workPageWidget->getOpticalPropertyList();

    tcl_file->open(QIODevice::Append | QIODevice::Text);
    QTextStream out(tcl_file);

    out<<"MaterialSet MS"<<endl;
    out<<endl;

    // source opt_file inside the main tcl file
    // The path sourced should be inside the container

    QString concat_opt_file = "source" + SPACE + proj_dir + "/"+ opt_info->fileName();
    out<<concat_opt_file<<endl;


    this->create_optical_file();

    tcl_file->close();

}


//write optical file to .tcl
void Tcl::create_optical_file(){
    std::vector<OpticalProperty*> opticalPropertyList=workPageWidget->getOpticalPropertyList();
    optical_file->open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out_sources(optical_file);

    std::map <QString,QString> property_map;



    for(unsigned i=0;i<opticalPropertyList.size();i++){
        property_map= opticalPropertyList[i]->get_property_map();
        out_sources<<MATERIAL + SPACE + opticalPropertyList[i]->name<<endl;
        for(unsigned j=0;j<optical_property_names.size();j++){
            out_sources<<TAB + opticalPropertyList[i]->name + SPACE + optical_property_names[j] + TAB + property_map[optical_property_names[j]]<<endl ;
        }
    }


// Define air for exterior of MaterialSet

    out_sources<<endl;
    out_sources<<"MS exterior air"<<endl;

    for(unsigned i=0;i<opticalPropertyList.size();i++){
        if(opticalPropertyList[i]->name!="air"){
            out_sources<<"MS append "<<opticalPropertyList[i]->name<<endl;
        }
    }


    optical_file->close();
}



// write source file to .tcl file
void Tcl::create_source(){
    tcl_file->open(QIODevice::Append | QIODevice::Text);
    QTextStream out(tcl_file);


    QFileInfo source_file_info = sources_file->fileName();

    QString concat_source_file = "source"+SPACE+ proj_dir + "/"+src_info->fileName();

    out<<concat_source_file<<endl;
    /* Call different functions depending on
     * multiple/single simulation*/


    this->create_source_file();

    if(this->simultype==multiple)
        this->create_source_file_multiple();

    tcl_file->close();
}


//create content of _src.tcl file
void Tcl::create_source_file(){
    sources_file->open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out_optical(sources_file);

    SourceList* my_sourceList= workPageWidget->getSourceList();
    std::vector<int> SourcesID =my_sourceList->getSourcesID();
    PDTVtkSource* current_source;

    out_optical<<"Composite C"<<endl;
    for (unsigned i=0;i<SourcesID.size();i++){

        current_source = my_sourceList->getSourceById(SourcesID[i]);
        QString id=QString::number(SourcesID[i]);

        if(current_source->getSourceType()==POINT){
            double point[3];
            double power;
            PDTVtkPointSource* current_point_source = dynamic_cast<PDTVtkPointSource*>(current_source);
            power = current_point_source->GetPower();
            current_point_source->getEndpoints(point);

            out_optical<<"Point P"+id<<endl;
            out_optical<<TAB + "P" + id + SPACE +"power" + SPACE + QString::number(power)<<endl;
            out_optical<<TAB + "P" + id + SPACE + "position" + SPACE + QUOTE + QString::number(point[0]) + SPACE + QString::number(point[1]) + SPACE + QString::number(point[2]) + QUOTE<<endl;
            out_optical<<"C"+SPACE+"add"+SPACE+"P"+id<<endl;
        }
        else if(current_source->getSourceType()==FIBER){
            double center[3];
            double direction[3];
            double radius;
            double NA;
            double power;
            PDTVtkFiberSource* current_Fiber_source = dynamic_cast<PDTVtkFiberSource*>(current_source);
            current_Fiber_source->GetCenter(center);
            current_Fiber_source->GetDirection(direction);
            radius = current_Fiber_source->GetRadius();
            NA = current_Fiber_source->GetNA();
            power = current_Fiber_source->GetPower();
            out_optical<<"Fiber F"+id<<endl; // Fiber F
            //F power
            out_optical<<TAB+"F"+id+SPACE+"power"+SPACE+QString::number(power)<<endl;
            //F fiberPos "x  y  z"
            out_optical<<TAB+"F"+id+SPACE+"fiberPos"+SPACE+QUOTE+QString::number(center[0])+SPACE+QString::number(center[1])+SPACE+QString::number(center[2])+QUOTE<<endl;
            //F fiberDir "x  y  z"
            out_optical<<TAB+"F"+id+SPACE+"fiberDir"+SPACE+QUOTE+QString::number(direction[0])+SPACE+QString::number(direction[1])+SPACE+QString::number(direction[2])+QUOTE<<endl;
            //F radius
            out_optical<<TAB+"F"+id+SPACE+"radius"+SPACE+QString::number(radius)<<endl;
            //F numericalAperture
            out_optical<<TAB+"F"+id+SPACE+"numericalAperture"+SPACE+QString::number(NA)<<endl;
            //C add F
            out_optical<<"C"+SPACE+"add"+SPACE+"F"+id<<endl;
        }

        else{
            double point1[3];
            double point2[3];
            double power;
            PDTVtkLineSource* current_line_source = dynamic_cast<PDTVtkLineSource*>(current_source);

            //point 1 and point 2 passed by reference to set correct values
            power = current_line_source->GetPower();
            current_line_source->getEndpoints(point1,point2);
            out_optical<<"Line L"+id<<endl;
            out_optical<<TAB + "L" + id + SPACE +"power" + SPACE + QString::number(power)<<endl;
            out_optical<<TAB+"L"+id+SPACE+"endpoint 0"+SPACE+QUOTE+QString::number(point1[0])+SPACE+QString::number(point1[1])+SPACE+QString::number(point1[2])+QUOTE<<endl;
            out_optical<<TAB+"L"+id+SPACE+"endpoint 1"+SPACE+QUOTE+QString::number(point2[0])+SPACE+QString::number(point2[1])+SPACE+QString::number(point2[2])+QUOTE<<endl;

            out_optical<<"C"+SPACE+"add"+SPACE+"L"+id<<endl;
        }

    }

     sources_file->close();

}

void Tcl::create_source_file_multiple(){
    sources_file->open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream out_optical(sources_file);

    int line_array_idx =0;
    int point_array_idx =0;
    int fiber_array_idx =0;

    SourceList* my_sourceList= workPageWidget->getSourceList();
    std::vector<int> SourcesID =my_sourceList->getSourcesID();
    PDTVtkSource* current_source;


    for (unsigned i=0;i<SourcesID.size();i++){
        current_source = my_sourceList->getSourceById(SourcesID[i]);
        QString id=QString::number(SourcesID[i]);

        if(current_source->getSourceType()==POINT){
            double point[3];
            double power;
            PDTVtkPointSource* current_point_source = dynamic_cast<PDTVtkPointSource*>(current_source);
            current_point_source->getEndpoints(point);
            power = current_point_source->GetPower();
            /*
            out_optical<<"Point P"+id<<endl;
            out_optical<<TAB + "P" + id + SPACE + "position" + SPACE + QUOTE + QString::number(point[0]) + SPACE + QString::number(point[1]) + SPACE + QString::number(point[2]) + QUOTE<<endl;
            out_optical<<"C"+SPACE+"add"+SPACE+"P"+id<<endl;
            */

            out_optical<<"set point_source_array("<<point_array_idx<<")"+SPACE+QUOTE+QString::number(power) + QUOTE<<endl;
            point_array_idx++;
            out_optical<<"set point_sources_array("<<point_array_idx<<")"+SPACE + QUOTE + QString::number(point[0]) + SPACE + QString::number(point[1]) + SPACE + QString::number(point[2]) + QUOTE<<endl;
            point_array_idx++;

        }

        else if(current_source->getSourceType()==FIBER){
            double direction[3];
            double center[3];
            double power;
            double radius;
            double NA;
            PDTVtkFiberSource* current_fiber_source = dynamic_cast<PDTVtkFiberSource*>(current_source);
            current_fiber_source->GetCenter(center);
            current_fiber_source->GetDirection(direction);
            power = current_fiber_source->GetPower();
            radius = current_fiber_source->GetRadius();
            NA = current_fiber_source->GetNA();

            out_optical<<"set fiber_source_array("<<fiber_array_idx<<")"+SPACE+QUOTE+QString::number(power) + QUOTE<<endl;
            fiber_array_idx++;
            out_optical<<"set fiber_source_array("<<fiber_array_idx<<")"+SPACE+QUOTE+QString::number(radius) + QUOTE<<endl;
            fiber_array_idx++;
            out_optical<<"set fiber_source_array("<<fiber_array_idx<<")"+SPACE+QUOTE+QString::number(NA) + QUOTE<<endl;
            fiber_array_idx++;
            out_optical<<"set fiber_source_array("<<fiber_array_idx<<")"+SPACE+QUOTE+QString::number(direction[0])+SPACE+QString::number(direction[1])+SPACE+QString::number(direction[2])+QUOTE<<endl;
            fiber_array_idx++;
            out_optical<<"set fiber_source_array("<<fiber_array_idx<<")"+SPACE+QUOTE+QString::number(center[0])+SPACE+QString::number(center[1])+SPACE+QString::number(center[2])+QUOTE<<endl;
            fiber_array_idx++;


        }
        else{
            double point1[3];
            double point2[3];
            double power;
            PDTVtkLineSource* current_line_source = dynamic_cast<PDTVtkLineSource*>(current_source);

            //point 1 and point 2 passed by reference to set correct values
            current_line_source->getEndpoints(point1,point2);
            power = current_line_source->GetPower();
            /*
            out_optical<<"Line L"+id<<endl;
            out_optical<<TAB+"L"+id+SPACE+"endpoint 0"+SPACE+QUOTE+QString::number(point1[0])+SPACE+QString::number(point1[1])+SPACE+QString::number(point1[2])+QUOTE<<endl;
            out_optical<<TAB+"L"+id+SPACE+"endpoint 1"+SPACE+QUOTE+QString::number(point2[0])+SPACE+QString::number(point2[1])+SPACE+QString::number(point2[2])+QUOTE<<endl;
            out_optical<<"C"+SPACE+"add"+SPACE+"L"+id<<endl;
            */
            out_optical<<"set fiber_source_array("<<line_array_idx<<")"+SPACE+QUOTE+QString::number(power) + QUOTE<<endl;
            line_array_idx++;
            out_optical<<"set line_source_array("<<line_array_idx<<")"+SPACE+QUOTE+QString::number(point1[0])+SPACE+QString::number(point1[1])+SPACE+QString::number(point1[2])+QUOTE<<endl;
            line_array_idx++;
            out_optical<<"set line_source_array("<<line_array_idx<<")"+SPACE+QUOTE+QString::number(point2[0])+SPACE+QString::number(point2[1])+SPACE+QString::number(point2[2])+QUOTE<<endl;
            line_array_idx++;


        }
    }

    sources_file->close();
}



// the boolean indicates whether the function is called to save the project
// or in order to run fullmonte
bool Tcl::set_up_kernel(bool save){
    tcl_file->open(QIODevice::Append | QIODevice::Text);
    QTextStream out(tcl_file);

    out<<"TetraVolumeKernel k"<<endl;

    bool isOkPressed=true;
    double num_packet_count;

    if(save){
        num_packet_count=0;
    }

    else{
        num_packet_count = QInputDialog::getInt(0, "Packet Number",
        "Especify the number of packets",0,0, INFINITY, 1, &isOkPressed);

    }
    if(isOkPressed==false){
        return false;
    }

    //error checking for packet count
    if(num_packet_count==0 && !save){
        QMessageBox Error_messge;
        Error_messge.setIcon(QMessageBox::Critical);
        Error_messge.setText("packet count can't be zero");
        Error_messge.exec();
        this->set_up_kernel(save);
    }


    //QString packet_count = myDialog.textValue();

    QString packet_count = QString::number(num_packet_count,'f',0);


    out<<TAB +"k"+SPACE+"packetCount"+SPACE+packet_count<<endl;
    out<<TAB +"k"+SPACE+"source"+SPACE+"C"<<endl;
    out<<TAB+"k" + SPACE +"geometry"+SPACE+DOLLAR_SIGN+"M"<<endl;
    out<<TAB+"k"+SPACE+"materials"+SPACE+"MS"<<endl;

    out<<TAB+"k"+SPACE+"runSync"<<endl;


    tcl_file->close();

    return true;

}


void Tcl::results(){
    tcl_file->open(QIODevice::Append | QIODevice::Text);
    QTextStream out(tcl_file);

    out<<"set"+SPACE+"ODC"+SPACE+"[k results]"<<endl;
    out<<"puts"+SPACE+QUOTE+"Results available:"+QUOTE<<endl;
    out<<"for { set i 0 } { $i < [$ODC size] } { incr i } {"<<endl;
    out<<TAB+"set res [$ODC getByIndex $i]"<<endl;
    out<<TAB+"puts"+SPACE+QUOTE+" type=[[$res type] name] name=[$res name]"+QUOTE<<endl;
    out<<"}"<<endl;

    tcl_file->close();
}


//write energyToFluence to .tcl file
void Tcl::energy_to_fluence(){
    tcl_file->open(QIODevice::Append | QIODevice::Text);
    QTextStream out(tcl_file);
    out<<"EnergyToFluence EF"<<endl;
    out<<TAB+"EF geometry $M"<<endl;
    out<<TAB+"EF materials MS"<<endl;
    out<<TAB+"EF source [$ODC getByName"+SPACE+QUOTE+"VolumeEnergy"+QUOTE+"]"<<endl;
    out<<TAB+"EF inputEnergy"<<endl;
    out<<TAB+"EF outputFluence"<<endl;
    out<<TAB+"EF update"<<endl;

    tcl_file->close();



}

// The std::vector returned is unnecessary
std::vector<QString> Tcl::mesh_fluence_append(){
    tcl_file->open(QIODevice::Append | QIODevice::Text);
    QTextStream out(tcl_file);

    out<<"VTKMeshWriter W"<<endl;

    //Output file name and directory
    out<<"W filename"+SPACE+ QUOTE + proj_dir + "/" +tcl_info->baseName()+".out.vtk" + QUOTE<<endl;
    out<<"W addData"+SPACE+QUOTE+"Fluence"+QUOTE+SPACE+"[EF result]"<<endl;
    out<<"W mesh $M"<<endl;
    out<<"W write"<<endl;

    tcl_file->close();

    std::vector<QString> output_info;

    tcl_file->close();

    return output_info;

}

//write fluence to .tcl file
void Tcl::fluence_to_text(std::vector<QString> output_info){
    tcl_file->open(QIODevice::Append | QIODevice::Text);
    QTextStream out(tcl_file);

    out<<"TextFileMatrixWriter TW"<<endl;

    // Output file name and path

    out<<TAB+"TW filename"+SPACE+QUOTE+proj_dir+ "/" +tcl_info->baseName()+".phi_v.txt"+QUOTE<<endl;
    out<<TAB+"TW source [EF result]"<<endl;
    out<<TAB+"TW write"<<endl;

    tcl_file->close();

}

//write tcl script associate with get fluence function to _phi.tcl /.tcl file
void Tcl::lineFluence_to_text(std::vector<double> output_info){

    QTextStream out;
    if(run_FM){//if run_FM = true then fullmonte has been run before
        fluence_tcl_file->open(QIODevice::Append | QIODevice::Text);
        out.setDevice(fluence_tcl_file);
    }
    else{
        tcl_file->open(QIODevice::Append | QIODevice::Text);
        out.setDevice(tcl_file);
    }
    out<<"AccumulationLineQuery ALC"<< endl;

    out<<TAB+"ALC geometry $M"<<endl;

    if(run_FM)
        out<<TAB+"ALC source $tetraFluenceData"<< endl;
    else
        out<<TAB+"ALC source [EF result]"<<endl;

    //ALC endpoint $i "$x $y $z"
    out<<TAB+"ALC endpoint 0 "+QUOTE+QString::number(output_info[0]) +SPACE+ QString::number(output_info[1]) +SPACE + QString::number(output_info[2])+QUOTE<<endl;
    out<<TAB+"ALC endpoint 1 "+QUOTE+QString::number(output_info[3]) +SPACE+ QString::number(output_info[4]) +SPACE + QString::number(output_info[5])+QUOTE<<endl;
    out<<TAB+"ALC update"<< endl;
    out<<TAB+"print"+ QUOTE + "Line fluence: [$ALC total]" + QUOTE << endl;
    if(run_FM)
        fluence_tcl_file->close();
    else
        tcl_file->close();
}


void Tcl::set_GUI_parent_dir(){

   // because of cd .. the file is created in parent directory
   int results =  system("cd .. && pwd > parent_dir.txt");

   // as a result ../parent_dir.txt instead of parent_dir.txt
   QFile temp_file("../parent_dir.txt");
   temp_file.open(QIODevice::ReadOnly | QIODevice::Text);

   QTextStream temp_stream(&temp_file);


   // Store the parent directory path
   GUI_parent_dir = temp_stream.readLine();

   // close and delete the temporary file
   temp_file.close();
   temp_file.remove();

}

void Tcl::set_current_working_dir(){
    //QDir current;
    working_dir = proj_dir;

}


void Tcl::check_AVX(){
    // make the check_AVX.sh file executable
    QString change_mod_command  = "chmod +x "+AVX_file_path;
    QProcess* change_executable = new QProcess();
    change_executable->execute(change_mod_command);

    QProcess* check_AVX_process=new QProcess();

    //hasn't trigger the AVX_output function
    QObject::connect(check_AVX_process,SIGNAL(readyReadStandardOutput()),this,SLOT(Avx_output()));

    // runs the command stored in the file specified by check_AVX_command
    check_AVX_process->start(check_AVX_command);

}

void Tcl::parent_dir_output(){
}

void Tcl::Avx_output(){
    AVX=true;
}


void Tcl::execute(){


    //QProcess concat;

    QString prev_dir = working_dir;

    QDir::setCurrent(working_dir);
    // make the file used to execute docker executable and run it
    int success = system("chmod +x tclmonte.sh");

    QProcess execution;
    QEventLoop myLoop;

    QObject::connect(&execution,SIGNAL(finished(int)),&myLoop,SLOT(quit()));
    execution.setStandardOutputFile("log.txt");
    execution.start("./tclmonte.sh" + SPACE + tcl_info->baseName() + ".tcl");
    QMessageBox FullMonte_progress;
    FullMonte_progress.setText("This may take some time \n You can view result in "+ proj_dir + "/" + "log.txt");
    FullMonte_progress.exec();

    myLoop.exec();

    //reset directory
    //QDir::setCurrent(GUI_parent_dir+"/build-release");
    QDir::setCurrent(prev_dir);


}

void Tcl::post_execution(){
    // check for
    bool success=this->check_fullmonte_success();

    // copy output from mount to user's project directory
    //QString base_file_path = working_dir + "/mount/";
    //QString output_file_path_mount = base_file_path + tcl_info->baseName()+".out.vtk";
    //QString phi_v_file_path_mount = base_file_path + tcl_info->baseName()+".phi_v.txt";
    QString output_file_path_proj=proj_dir+SLASH+tcl_info->baseName()+".out.vtk";
    //QString phi_v_file_path_proj = proj_dir+SLASH+tcl_info->baseName()+".phi_v.txt";

    if(success){
        this->visualization();
        QDir currentDir;
        currentDir = proj_dir;
        //QString input = "FM_input";
        //currentDir.mkdir(input);
        QString output = "FM_output";
        currentDir.mkdir(output);
        run_FM = true;
        //set_proj_input_dir();
        set_proj_output_dir();
        classification();
    }

}



// project success evaluated based on the presence of
// <<project_name>>.out.vtk file
bool Tcl::check_fullmonte_success(){
    bool success;

    QString base_name = tcl_info->baseName();

    QStringList nameFilter(tcl_info->baseName()+".out.vtk");
    QDir des_directory(working_dir);
    QStringList file_name = des_directory.entryList(nameFilter);
    QMessageBox messagebox;
    if(file_name.size()==0){

        messagebox.setText("Project failed");
        messagebox.exec();
        success=false;
    }

    else{
        messagebox.setText("Project succeeded");
        messagebox.exec();
        success=true;
    }

    return success;


}

void Tcl::set_proj_output_dir(){
    proj_output_dir = proj_dir + "/" + "FM_output";
}

void Tcl::classification(){
    QString suffix_mesh= ".mesh.vtk";
    QString suffix_tcl = ".tcl";
    QString suffix_txt = ".txt";
    QString suffix_output = ".out.vtk";
    QString suffix_phi_v = ".phi_v.txt";

    //int success = QFile::copy(proj_dir + "/" + tcl_info->baseName() + suffix_mesh,proj_input_dir + "/" + tcl_info->baseName() + suffix_mesh);



    if (QFile::exists(proj_output_dir + "/" + tcl_info->baseName() + suffix_output))
    {
        QFile::remove(proj_output_dir + "/" + tcl_info->baseName() + suffix_output);
    }

    int succes = QFile::copy(proj_dir + "/" + tcl_info->baseName() + suffix_output,proj_output_dir + "/" + tcl_info->baseName() + suffix_output);

    if (QFile::exists(proj_output_dir + "/" + tcl_info->baseName() + suffix_phi_v))
    {
        QFile::remove(proj_output_dir + "/" + tcl_info->baseName() + suffix_phi_v);
    }

    int succe = QFile::copy(proj_dir + "/" + tcl_info->baseName() + suffix_phi_v,proj_output_dir + "/" + tcl_info->baseName() + suffix_phi_v);
    //int succ = QFile::copy(proj_dir + "/" + tcl_info->baseName() + suffix_tcl,proj_input_dir + "/" + tcl_info->baseName() + suffix_tcl);
    //int suc = QFile::copy(tcl_proj_info->get_mesh_path_file_path(),proj_input_dir + "/" + mesh_file_path_file->fileName());
    //int su = QFile::copy(proj_dir + "/" + opt_info->baseName() + suffix_tcl,proj_input_dir + "/" + opt_info->baseName() + suffix_tcl);
    //int s = QFile::copy(proj_dir + "/"+ src_info->baseName() + suffix_tcl,proj_input_dir + "/" + src_info->baseName() + suffix_tcl);

//    QFile temp_file0(tcl_proj_info->get_mesh_path_file_path());
//    temp_file0.remove();
//    QFile temp_file1(tcl_proj_info->get_opt_file_path());
//    temp_file1.remove();
//    QFile temp_file2(tcl_proj_info->get_src_file_path());
//    temp_file2.remove();
//    QFile temp_file3(tcl_proj_info->get_tcl_file_path());
//    temp_file3.remove();
    QFile temp_file4(proj_dir + "/" + tcl_info->baseName() + suffix_output);
    temp_file4.remove();
//    QFile temp_file5(proj_dir + "/" + tcl_info->baseName() + suffix_mesh);
//    temp_file5.remove();
    QFile temp_file6(proj_dir + "/" + tcl_info->baseName() + suffix_phi_v);
    temp_file6.remove();


}


void Tcl::visualization(){

    QMessageBox msgBox;
    msgBox.setWindowTitle("Loading ....");
    msgBox.setText("Reading Files, please wait ...");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setStandardButtons(QMessageBox::NoAll);
    msgBox.open();
    QCoreApplication::processEvents();


    workPageWidget->loadFile(proj_dir+SLASH+tcl_info->baseName()+".out.vtk");

    msgBox.close();
}


void Tcl::copy_files_to_mock(){
    // copy files from project directory to mount directory


//    QString source_base_file_path = "cp"+SPACE+proj_dir+"/";
//    QString source_path = proj_dir+"/";
//    QString dest_base_file_path =working_dir+"/mount/";
      QString native_build = "/usr/bin";
      QString tclMonte_name = "tclmonte";

    // copy the tcl file

    bool copy_success = QFile::copy(native_build + "/" + tclMonte_name + ".sh",proj_dir+ "/" + tclMonte_name + ".sh");

    // copy mesh file
    QFileInfo mesh_info = workPageWidget->getMeshFileInfo();
    if (QFile::exists(proj_dir+"/"+tcl_info->baseName()+".mesh.vtk"))
    {
        QFile::remove(proj_dir+"/"+tcl_info->baseName()+".mesh.vtk");
    }

    QFile::copy(mesh_info.absoluteFilePath(),proj_dir+"/"+tcl_info->baseName()+".mesh.vtk");


}

void Tcl::Test(){

    set_current_working_dir();

}

QString Tcl::getProjDir(){
    return this->proj_dir;
}

void Tcl::setRun_FM(bool flag){
    run_FM = flag;
}


void Tcl::set_fluence_tcl_file(){
   fluence_tcl_file = new QFile(tcl_info->baseName() + "_phi.tcl");
}

void Tcl::set_Matrix_Reader_to_text(){
   QString file_path = proj_dir + "/" + tcl_info->baseName()+".phi_v.txt";
   QTextStream out(fluence_tcl_file);
   fluence_tcl_file->open(QIODevice::Append | QIODevice::Text);
   out<<endl;
   out << "TextFileMatrixReader TR" << endl;
   out << TAB + "TR filename" + QUOTE + file_path + QUOTE << endl;
   out << TAB + "TR read" << endl;
   out << "set tetraFluenceData [$TR output]" << endl;
   out << endl;
   fluence_tcl_file->close();
}


