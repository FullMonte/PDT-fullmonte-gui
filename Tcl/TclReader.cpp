#include "TclReader.h"
#include <QFile>
#include <QTextStream>
#include <ostream>
#include <iostream>
#include <QMessageBox>
#include <cmath>
#include <QByteArray>
#include <QFileDialog>
#include "Tcl.h"


/*TclReader is a text parser. For convinience the files have been separated
 * to .tcl (the main tcl file) .opt(The optical properties) and .src(optical sources)
 * Only .opt and .src are parsed. And then the optical source table and optical property
 * table are updated accordingly*/




TclReader::TclReader(){

}


TclReader::TclReader(PDTWorkPageWidget* currWorkpage)
{
    curWorkPageWidget = currWorkpage;





}

void TclReader::set_PDTWorkPageWidget(PDTWorkPageWidget* currentWorkPageWidget){
    curWorkPageWidget=currentWorkPageWidget;
}

void TclReader::set_WorkPage_TclProjInfo(){
    curWorkPageWidget->setTclProjInfo(proj_info);
}



// function includes checks to make sure the required files are available
// saves the TclProjInfo
bool TclReader::set_proj_dir(){

    bool success= false;

    // allow user to especify the project directory
    proj_dir =  QFileDialog::getExistingDirectory();

    // exits function if proj_dir is empty (user presses cancel on QDialogBox)
    if(proj_dir.isEmpty()){
        return success;
    }

    // pattern recognition to find the name of the file with _opt.tcl
    QStringList nameFilter("*_opt.tcl");
    QDir directory(proj_dir);
    QStringList file_name = directory.entryList(nameFilter);

    // check opt file exists
    if(file_name.size()>0){

        //store the optical file path
        opt_file_path = proj_dir+SLASH+file_name[0];
    }
    else{
        QMessageBox error_msg;
        error_msg.setIcon(QMessageBox::Critical);
        error_msg.setText("file with patter _opt.tcl not found");
        error_msg.exec();



        return success;

    }

    // pattern recognition to find the name of the file with _src.tcl
    nameFilter.clear();
    nameFilter.append("*_src.tcl");
    file_name = directory.entryList(nameFilter);

    //store the optical file path

    if(file_name.size()>0){
        src_file_path = proj_dir+SLASH+file_name[0];
    }
    else{
        QMessageBox error_msg;
        error_msg.setIcon(QMessageBox::Critical);
        error_msg.setText("file with patter _src.tcl not found");
        error_msg.exec();

        return success;
    }


    nameFilter.clear();
    nameFilter.append("*.tcl");
    file_name = directory.entryList(nameFilter);

    // the project directory should only have 3 tcl files
    if(file_name.size()< 3){
        QMessageBox error_msg;
        error_msg.setIcon(QMessageBox::Critical);
        error_msg.setText("Incorrect number of .tcl files. Must be more than or equal to 3");
        error_msg.exec();

        return success;
    }

    else{
        QFileInfo src_info(src_file_path);
        QFileInfo opt_info(opt_file_path);
        for(unsigned i=0;i<file_name.size();i++){

            // the name of the main tcl file is found by choosing the 3rd file after detecting src and opt files
            if(file_name[i]!=src_info.fileName() && file_name[i]!=opt_info.fileName()){
                tcl_file_path = proj_dir + SLASH + file_name[i];
                break;
             }
          }


    }

    // check if the _mesh_path.txt exists

    nameFilter.clear();
    nameFilter.append("*_mesh_path.txt");
    file_name=directory.entryList(nameFilter);

    if(file_name.size()!=1){
        QMessageBox error_msg;
        error_msg.setIcon(QMessageBox::Critical);
        error_msg.setText("Could not find _mesh_path.txt");
        error_msg.exec();

        return success;
    }

    else{
        mesh_path_file_path=proj_dir + SLASH + file_name[0];

    }


     proj_info = new TclProjInfo(proj_dir,tcl_file_path,src_file_path,opt_file_path,mesh_path_file_path);

     success=true;
     return success;



}



QString TclReader::read_mesh_path(){

    QFile* mesh_add_file=new QFile(mesh_path_file_path);
    mesh_add_file->open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream read_mesh_path(mesh_add_file);

    QString file_path = read_mesh_path.readLine();

    mesh_add_file->close();

    return file_path;

}






std::vector<OpticalProperty*> TclReader::optical_reader(){
    // This is a hardcoded path that has to be changed

      //QString proj_dir= QFileDialog::getExistingDirectory()
      QFile file(opt_file_path);
      file.open(QIODevice::ReadOnly);
      QTextStream in(&file);

      QString line;

      // scan through all the file
      while(!in.atEnd()){
          line=in.readLine();


          QStringList splitted_words;
          if(line.size()!=0){
                splitted_words = split_words(line);

          // if the first word is material, the second word
          // is used to create an optical property with
          QString first_word=splitted_words[0];
          if(first_word==MATERIAL){
             if(splitted_words[1]!="air"){

                OpticalProperty* new_property= new OpticalProperty(splitted_words[1]);
                optical_properties.push_back(new_property);
              }
          }


          // if the first word refers to an already existing material
          // check second word is valid
          // set values (third word) based on what second word is
          else {
              OpticalProperty* current_property=(first_word_in_optical_property_list(splitted_words[0]));
              if (current_property!=NULL){
                QString second_word= splitted_words[1];

                if(second_word==SCATTERING){
                    current_property->set_scattering(splitted_words[2]);
                }
                else if(second_word==ABSORPTION){
                    current_property->set_absorption(splitted_words[2]);
                }

                else if(second_word==REFRACTIVE){
                    current_property->set_refraction(splitted_words[2]);
                }

                else if(second_word==ANISOTROPY){
                    current_property->set_anisotropy(splitted_words[2]);
                }



             }

          }
       }

    }

return optical_properties;


}


QStringList TclReader::split_words(QString line){
    line=line.replace(TAB,SPACE);

    QStringList splitted_words=line.split(SPACE);


     // remove empty words
        if (splitted_words[0]==""){
            splitted_words.removeFirst();
        }


    //return a list of words
    return splitted_words;
}

OpticalProperty* TclReader::first_word_in_optical_property_list(QString first_word){
    for(unsigned i=0;i<optical_properties.size();i++){
        if(this->optical_properties[i]->name==first_word){
            return optical_properties[i];
        }


    }

    return NULL;
}


// Returns array because Zach PDTWorkPageWidge source is array
std::vector<PDTVtkSource*> TclReader::source_reader(){
    QFile source_file(src_file_path);
    source_file.open(QIODevice::ReadOnly);
    QString line;
    QTextStream in(&source_file);
    std::vector<PDTVtkSource*> result;


    while(!in.atEnd()){
        line = in.readLine();
        QStringList splitted_words;
        if(line.size()!=0){

            // Splits words in a line based on spaces
            splitted_words = split_words(line);

            QString first_word = splitted_words[0];

            if(first_word==POINT || first_word==LINE ||first_word==FIBER){
                PDTVtkSource* new_source;
                // Parent pointer points to different child objects
                if(first_word==POINT){
                    new_source = new PDTVtkPointSource(default_pos,default_num);
                }
                else if(first_word==FIBER){
                    new_source = new PDTVtkFiberSource(default_num,default_pos,default_pos,default_num,default_num);

                }
                else{
                    new_source = new PDTVtkLineSource(default_pos,default_pos,default_num);
                }

                //std::pair<PDTVtkSource*,QString> new_pair;
                std::tuple<PDTVtkSource*,QString,QString> new_tuple;

                // splitted_words[1] is the name of the source
                //new_pair=std::make_pair(new_source,splitted_words[1]);
                new_tuple = std::make_tuple(new_source,splitted_words[1],splitted_words[0]);

                sources.push_back(new_tuple);
            }


            else {
                PDTVtkSource* current_source = source_in_sources(first_word);
                if(current_source!=NULL){
                    QString second_word = splitted_words[1];

                    // Position as specified within quotation marks
                    QString position = quotation_pos(line);
                    double * position_points;
                    position_points= get_point_between_quotes(position);

                    //Keyword position means object is a point
                    if(second_word == POSITION){

                        PDTVtkPointSource* p_source = (PDTVtkPointSource *) current_source;
                        p_source->setEndPoints(position_points);

                    }

                    else if(second_word == POWER){
                        if(sourceType_in_sources(first_word)== POINT){
                            PDTVtkPointSource* p_source = (PDTVtkPointSource *) current_source;
                            QString third_word = splitted_words[2];
                            p_source->SetPower(third_word.toDouble());
                        }
                        else if(sourceType_in_sources(first_word)== LINE){
                            PDTVtkLineSource* l_source = (PDTVtkLineSource*) current_source;
                            QString third_word = splitted_words[2];
                            l_source->SetPower(third_word.toDouble());
                        }
                        else if(sourceType_in_sources(first_word)==FIBER){
                            PDTVtkFiberSource* f_source = (PDTVtkFiberSource* ) current_source;
                            QString third_word = splitted_words[2];
                            f_source->SetPower(third_word.toDouble());
                        }
                    }

                    //keyword fiberPos/fiberDir/radius/numericalAperture means object is a fiber
                    else if(second_word == FIBERPOS){
                        PDTVtkFiberSource* f_source = (PDTVtkFiberSource* ) current_source;
                            //double *center = new double[3];
                            //f_source->SetCenter(center[0],center[1],center[2]);
                        f_source->SetCenter(position_points[0],position_points[1],position_points[2]);
                    }

                    else if(second_word == FIBERDIR){
                            //double *direction = new double[3];
                            //f_source->SetDirection(direction);
                        PDTVtkFiberSource* f_source = (PDTVtkFiberSource* ) current_source;
                        f_source->SetDirection(position_points[0],position_points[1],position_points[2]);
                    }
                    else if(second_word == RADIUS ){
                        PDTVtkFiberSource* f_source = (PDTVtkFiberSource* ) current_source;
                             //double *radius;
                            //*radius = f_source->GetRadius();
                        QString third_word = splitted_words[2];
                        f_source->SetRadius(third_word.toDouble());
                        if(f_source->GetNA()!= default_num){
                            f_source->update();
                        }

                    }
                    else if(second_word == NA){
                        PDTVtkFiberSource* f_source = (PDTVtkFiberSource* ) current_source;
                        QString third_word = splitted_words[2];
                        f_source->SetNA(third_word.toDouble());
                        if(f_source->GetRadius()!=default_num){
                            f_source->update();
                        }
                    }
                    // Keyword setendpoint means object is a line
                    else if(second_word==SETENDPOINT){
                        PDTVtkLineSource* l_source = (PDTVtkLineSource*) current_source;
                        QString third_word = splitted_words[2];
                        double  *first_point=new double[3];
                        double  *second_point=new double[3];

                        l_source->getEndpoints(first_point,second_point);

                        QString ZERO="0";

                        if(third_word==ZERO){
                               l_source->setEndpoints(position_points,second_point);
                        }

                        else{
                              l_source->setEndpoints(first_point,position_points);
                        }

                    }

                }

            }
        }

    }


    source_file.close();


    std::vector<PDTVtkSource*> source_result = create_source_list();
    return source_result;

}



std::vector<PDTVtkSource*> TclReader::create_source_list(){
    std::vector<PDTVtkSource*> sources_list;

    for(unsigned i=0;i<sources.size();i++){
        sources_list.push_back(std::get<0>(sources[i]));

    }

    return sources_list;

}


//REMOVE THIS FUNCTION
void TclReader::test_function(){


}





bool TclReader::is_point(QString line){

    bool result =  line.contains("position");

}

QString TclReader::quotation_pos(QString line){
    std::vector<unsigned> quote_pos;
    for(unsigned i=0;i<line.size();i++){
        if(line.at(i)==QUOTE){
            quote_pos.push_back(i);
        }
    }

   QString position = "";
   if(!quote_pos.empty()){
   position=line.mid(quote_pos[0]+1,quote_pos[1]-quote_pos[0]-1);
   }

   return position;
}

double* TclReader::get_point_between_quotes(QString position){
    QStringList splitted_pos = position.split(" ");

    double* result=new double();
    for(unsigned i=0; i<splitted_pos.size();i++){
        // make sure this number is too big
        // otherwise it can't be supported by toLong
        double current_int = splitted_pos[i].toDouble();
        result[i]=current_int;


    }




    return result;
}


PDTVtkSource* TclReader::source_in_sources(QString source_name){
    for(unsigned i=0;i<sources.size();i++){
        if(std::get<1>(sources[i])==source_name){
            return std::get<0>(sources[i]);
        }
    }

    return NULL;
}


QString TclReader::sourceType_in_sources(QString source_name){
    for(unsigned i=0;i<sources.size();i++){
        if(std::get<1>(sources[i])==source_name){
            return std::get<2>(sources[i]);
        }
    }

    return NULL;
}


TclProjInfo* TclReader::get_project_info(){
    return proj_info;
}
