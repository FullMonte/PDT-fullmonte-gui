#ifndef PDTVTKCUTENDFIBERSOURCE
#define PDTVTKCUTENDFIBERSOURCE

#include <vtkNew.h>
#include <vtkConeSource.h>


//#include <FullMonteSW/Geometry/Placement/PlanePlacementLineSource.hpp>  //don't need to set up a plane for cone

#include "PDTVtkSource.h"

class PDTVtkFiberSource : public PDTVtkSource {

public: //look at coneSource function members
    
    
    PDTVtkFiberSource();
    PDTVtkFiberSource(double Radius, double center[3],double direction[3],double NA,float w=1.0f,int resolution=20);
    ~PDTVtkFiberSource();

    //to be implemented
    

    void SetPower(double Power);

    double GetPower();

    void SetRadius(double Radius);

    double GetRadius();

    void SetResolution(int);

    int GetResolution();

    void SetCenter(double x,double y,double z);

    void GetCenter(double data[3]);

    void SetDirection(double x,double y,double z);

    void GetDirection(double data[3]);

    void SetNA(double NA);


    double GetNA();

    //vtkSmartPointer<vtkConeSource> GetConeSource();

    void SetCapping(vtkTypeBool);

    vtkTypeBool GetCapping();
    
    
    
    shared_ptr<PlacementMediatorBase> generatePlacementMediator(PDTVtkPlanePlacement* ) override;

    virtual void update() override;// need to change parameter

    virtual string printInfo() override;// need to change parameter


    //virtual void toXml(QXmlStreamWriter &) override; //need to change parameter

private:
        vtkNew<vtkConeSource> FiberSource;//has to look at vtk--source

};


#endif // PDTVTKCUTENDFIBERSOURCE

