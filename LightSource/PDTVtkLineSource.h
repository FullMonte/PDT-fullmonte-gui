#ifndef PDTVTKLINESOURCE_H
#define PDTVTKLINESOURCE_H

#include <vtkNew.h>
#include <vtkLineSource.h>

#include <FullMonteSW/Geometry/Placement/PlanePlacementLineSource.hpp>

#include "PDTVtkSource.h"

class PDTVtkLineSource : public PDTVtkSource {

public:
    PDTVtkLineSource();
    PDTVtkLineSource(double e0[3], double e1[3], float w=1.0f);

    ~PDTVtkLineSource();

    void SetPower(double Power);
    
    double GetPower();
    
    void getEndpoints(double e0[3], double e1[3]);

    void setEndpoints(double e0[3], double e1[3]);

    void getPlanePosition(double planePos[2], double depths[2]);

    // Set plane placement position
    void setPlanePosition(double planePos[2], double depths[2]);

    shared_ptr<PlacementMediatorBase> generatePlacementMediator(PDTVtkPlanePlacement* ) override;

    virtual void update() override;

    virtual string printInfo() override;

    virtual void toXml(QXmlStreamWriter &) override;

private:
	vtkNew<vtkLineSource> lineSource;

};


#endif


