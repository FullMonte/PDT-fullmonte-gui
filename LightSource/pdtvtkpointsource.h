#ifndef PDTVTKPOINTSOURCE_H
#define PDTVTKPOINTSOURCE_H


#include <vtkNew.h>
#include <vtkPointSource.h>

#include "PDTVtkSource.h"


class PDTVtkPointSource : public PDTVtkSource
{
public:
    PDTVtkPointSource();

    PDTVtkPointSource(double e0[3],float w=1.0f);

    ~PDTVtkPointSource();

    void SetPower(double Power);

    double GetPower();

    void getEndpoints(double e0[3]);

    void setEndPoints(double e0[3]);

   /*
    void getPlanePosition(double planePos[2], double depths[2]);

    // Set plane placement position
    void setPlanePosition(double planePos[2], double depths[2]);*/

    shared_ptr<PlacementMediatorBase> generatePlacementMediator(PDTVtkPlanePlacement* ) override;

    virtual void update() override;

    virtual string printInfo() override;

    virtual void toXml(QXmlStreamWriter &) override;

    unsigned int GetNumPointsInSource(){
        return pointSource->GetNumberOfPoints();
    }

    void SetNumPointInSource(int num){
        pointSource->SetNumberOfPoints(num);
    }

private:
    vtkNew<vtkPointSource> pointSource;
};

#endif // PDTVTKPOINTSOURCE_H
