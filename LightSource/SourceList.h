#ifndef SOURCELIST_H
#define SOURCELIST_H

#define FREE_PID 0

#include <vector>
#include <map>
#include <memory>

#include <vtkAppendPolyData.h>
#include <vtkNew.h>

#include "PDTVtkSource.h"
#include "PDTVtkPlanePlacement.h"

using namespace std;


struct SourceElement {
    // Source ID
    int sid;
    // PlanePlacement ID the source attaches to 
    // If it is a free source, attach to FREE_PID 
    int pid;
    shared_ptr<PDTVtkSource> source;
};


struct PlanePlacementElement {
    // PlanePlacement ID
    int pid;
    // All the source IDs attached to this plane placement
    vector<int> sources;    
    shared_ptr<PDTVtkPlanePlacement> planePlacement;
};


class SourceList {
public :
	SourceList(vtkSmartPointer<vtkRenderer> renderer);
	~SourceList();

    // Add one source to list, create a new id for the source
    void addSource(PDTVtkSource*, int pid = FREE_PID);

    // Insert source with given id
    // Return true if insertion is successful
    bool insertSource(PDTVtkSource*, int pid, int sid);

    // Add one source to list, create an new id for the plane
    void addPlanePlacement(PDTVtkPlanePlacement*);

    // Insert plane placement with given id
    // Return true if insertion is successful
    bool insertPlanePlacement(PDTVtkPlanePlacement*, int pid);

    // Return source by ID
    PDTVtkSource* getSourceById(int sid);
 
    // Remove one singal source from list by source ID 
    // Return true if successful 
    void removeSourceById(int);

    // Remove the all the sources on one plane by plane ID
    // Return true if successful
    void removeSourcesByPid(int) ;

    // Get plane placement map
    map<int, PlanePlacementElement> & getPlanePlacementMap() { return planePlacementMap; }

    // Get plane placement by PID
    PDTVtkPlanePlacement* getPlanePlacementById(int pid);

    // Enable/Disable highlight for the source
    void setSourceHighlightedById(int, bool);

    // Enable/Disable highlight for the plane placement and all the sources
    void setPlanePlacementHighlightedById(int, bool);

    // Clear all hight sources and planes
    void clearAllHighlights();

    // Show the source on the vtk view
    void showSource(int);

    //show all sources
    //uses function showSource
    void showAllSource();

    // Show the plane placement and its sources on the vtk view
    void showPlanePlacement(int);

    // Hide the source on the vtk view
    void hideSource(int);

    // Hide the plane placement and its sources on the vtk view
    void hidePlanePlacement(int);

    // Return true if it is a free sourc
    bool isFreeSource(int);

    // Get plane placement id of the source
    int getSourcePlanePlacementId(int);

    string getSourceInfoById(int);

    string getPlanePlacementInfoById(int);

    // Reset source list to intial state. Remove all polydata and sourceElement
    // Add trivial point and free plane placement
    void reset();

    // Check if the given id exitst in the list
    bool isSourceExist(int sid);
    bool isPlanePlacementExist(int pid);

    // The number of sources inserted
    std::vector<int> getSourcesID();

    int getID();
private :
    int curSid = 1;
    int curPid = 1000; 

    map<int, SourceElement> sourceMap;
    map<int, PlanePlacementElement> planePlacementMap;

    vtkSmartPointer<vtkRenderer> renderer;

};

#endif
