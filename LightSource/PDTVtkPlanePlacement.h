#ifndef PDTVTKPLANEPLACEMENT_H
#define PDTVTKPLANEPLACEMENT_H

#include <vector>
#include <memory>

#include <vtkActor.h>
#include <vtkNew.h>
#include <vtkPlaneSource.h>

#include <FullMonteSW/Geometry/Placement/PlanePlacement.hpp>
#include <FullMonteSW/Geometry/Placement/PlacementMediatorBase.hpp>

#include "PDTVtkObject.h"

class PDTVtkSource;

using namespace std;

const UnitVector3 dir1 = {0.0f,0.0f,1.0f};
const UnitVector3 dir2 = {1.0f,0.0f,0.0f};


class PDTVtkPlanePlacement : public PDTVtkObject<PlanePlacement> {

public:
    PDTVtkPlanePlacement(vtkPlaneSource* planeSource);
    PDTVtkPlanePlacement(double center[3], double normal[3], double origin[3], double point1[3], double point2[3]);

    PDTVtkPlanePlacement(double center[3], double normal[3]);
    ~PDTVtkPlanePlacement();

    void addSource(PDTVtkSource* );
    void removeSource(PDTVtkSource* );

    // Getters
    void getCenter(double center[3]);
    void getNormal(double normal[3]);

    // Update the position for fullmonte plane and sources
    void setCenter(double[3]);
    void setNormal(double[3]);


    void projectToPlane(double input[3], double result[3]);
    void invertToWorld(double planePos[3], double world[3]);

    // Highlight/Dishighlight the plane
    void setHighlight(bool) override;

    // Update on vtk view
    void update();

    virtual void toXml(QXmlStreamWriter & w) override;

    virtual string printInfo() override;

private:
    void construct(double center[3], double normal[3], double origin[3], double point1[3], double point2[3]);

    vtkNew<vtkPlaneSource> planeSource;
    vector<PDTVtkSource*> sources;
    vector<shared_ptr<PlacementMediatorBase>> planePlacementMediators;

    void getOrigin(double origin[3]);
    void getPoint1(double point1[3]);
    void getPoint2(double point2[3]);


};


#endif
