#include "PDTVtkPlanePlacement.h"

#include <vtkPolyDataMapper.h>

#include <QDebug>

#include "IO/SourceListIO.h"
#include "Utils/VectorMaths.h"
#include "Utils/QStringUtils.h"
#include "PDTVtkSource.h"

using namespace QStringUtils;

PDTVtkPlanePlacement::PDTVtkPlanePlacement(vtkPlaneSource* planeSourceRef) {
    this->objectType = PLACEMENT;
    
    // Origin, point1 and point2 are used to define the size and shape of the plane
    // Origin is not equal to center. Center is the center of the plane. Origin is together
    // with the point1 and point2 to define size and position.
    double center[3], normal[3];
    planeSourceRef->GetCenter(center);
    planeSourceRef->GetNormal(normal);

    VectorMaths::normalize(normal);

    double origin[3], point1[3], point2[3];
    planeSourceRef->GetOrigin(origin);
    planeSourceRef->GetPoint1(point1);
    planeSourceRef->GetPoint2(point2);

    construct(center, normal, origin, point1, point2);
} 

PDTVtkPlanePlacement::PDTVtkPlanePlacement(double center[3], double normal[3], double origin[3], double point1[3], double point2[3]) {
    construct(center, normal, origin, point1, point2);
}

void PDTVtkPlanePlacement::construct(double center[3], double normal[3], double origin[3], double point1[3], double point2[3]) {
    // Set the fullmonte object
    this->fullMonteData = unique_ptr<PlanePlacement>(new PlanePlacement());
    Basis & basis = fullMonteData->basis();

    Point3 c = {(float)center[0], (float)center[1], (float)center[2]};
    Vector3 n = {(float)normal[0], (float)normal[1], (float)normal[2]};
    Vector3 v0, v1, v2;
    if (n != dir1) {
        v0 = dir1;
    } else {
        v0 = dir2;
    }

    // v1 = v0 x n
    VectorMaths::cross(v0.data(), n.data(), v1.data());

    // v2 = v1 x n
    VectorMaths::cross(v1.data(), n.data(), v2.data());

    // Debug print
    qDebug() << "Create plane placement: ";
    qDebug() << "v0 (" << v0[0] << "," << v0[1] << "," << v0[2] << ")" ;
    qDebug() << "v2 (" << v2[0] << "," << v2[1] << "," << v2[2] << ")" ;
    qDebug() << "v1 (" << v1[0] << "," << v1[1] << "," << v1[2] << ")" ;
    qDebug() << "n ("  << n[0] << "," << n[1] << "," << n[2] << ")" ;
    qDebug() << "c ("  << c[0] << "," << c[1] << "," << c[2] << ")" ;

    basis = Basis(v1,v2,n,c);

    // Set the vtk object
    this->planeSource->SetOrigin(origin);
    this->planeSource->SetPoint1(point1);
    this->planeSource->SetPoint2(point2);
    this->planeSource->SetResolution(4, 4);
    this->planeSource->Update();

    //draw plane
    vtkNew<vtkPolyDataMapper> mapper;
    mapper->SetInputConnection(this->planeSource->GetOutputPort());
    actor->SetMapper(mapper.Get());
    actor->GetProperty()->SetColor(PDTVtkColor::WHITE[0], PDTVtkColor::WHITE[1], PDTVtkColor::WHITE[2]);
    actor->GetProperty()->SetRepresentationToWireframe();;
    actor->GetProperty()->LightingOff();
}

PDTVtkPlanePlacement::~PDTVtkPlanePlacement() {

}

void PDTVtkPlanePlacement::addSource(PDTVtkSource * pdtVtkSource) {
    planePlacementMediators.push_back(pdtVtkSource->generatePlacementMediator(this));
    sources.push_back(pdtVtkSource);
}

void PDTVtkPlanePlacement::removeSource(PDTVtkSource* pdtVtkSource) {
   std::remove(sources.begin(), sources.end(), pdtVtkSource);
}

void PDTVtkPlanePlacement::getCenter(double center[3]){
    Vector3 m_center = this->fullMonteData->basis().origin();
    for(int i = 0; i < 3; i++){
        center[i] = (double)m_center[i];
    }
}

void PDTVtkPlanePlacement::getNormal(double normal[3]){
    Vector3 m_normal = this->fullMonteData->basis().normal();
    for(int i = 0; i < 3; i++){
        normal[i] = (double)m_normal[i];
    }
}

void PDTVtkPlanePlacement::getOrigin(double origin[3]) {
    this->planeSource->GetOrigin(origin);
}

void PDTVtkPlanePlacement::getPoint1(double point1[3]) {
    this->planeSource->GetPoint1(point1);

}

void PDTVtkPlanePlacement::getPoint2(double point2[3]) {
    this->planeSource->GetPoint2(point2);
}

void PDTVtkPlanePlacement::setCenter(double center[3]){
    Basis & basis = fullMonteData->basis();
    Vector3 newCenter = {(float)center[0], (float)center[1], (float)center[2]};

    // TODO: This update is problemetic, need to update
    basis = Basis(basis.basis_x(), basis.basis_y(), basis.normal(), newCenter);

    for (auto mediator : planePlacementMediators){
        if(mediator->source() != nullptr){
            mediator->update();
        }
    }
}

void PDTVtkPlanePlacement::setNormal(double normal[3]){
    VectorMaths::normalize(normal);

    Basis & basis = fullMonteData->basis();
    Vector3 newNormal = {(float)normal[0], (float)normal[1], (float)normal[2]};

    // TODO: This update is problemetic, need to update
    basis = Basis(basis.basis_x(), basis.basis_y(), newNormal, basis.origin());

    for (auto mediator : planePlacementMediators){
        if(mediator->source() != nullptr){
            mediator->update();
        }
    }
}

void PDTVtkPlanePlacement::setHighlight(bool enable) {
    if(enable) {
        actor->GetProperty()->SetColor(PDTVtkColor::RED[0], PDTVtkColor::RED[1], PDTVtkColor::RED[2]);
    } else {
        actor->GetProperty()->SetColor(PDTVtkColor::WHITE[0], PDTVtkColor::WHITE[1], PDTVtkColor::WHITE[2]);
    }
}

void PDTVtkPlanePlacement::projectToPlane(double input[3], double result[3]){
    VectorMaths::project(input, this->fullMonteData->basis(), result);
}

void PDTVtkPlanePlacement::invertToWorld(double planePos[3], double world[3]) {
    Point3 p({(float)planePos[0], (float)planePos[1], (float)planePos[2]});
    Point3 w = this->fullMonteData->basis().invert(p);
    for (int i =0; i < 3; i++) {
        world[i] = w[i];
    }
}

void PDTVtkPlanePlacement::update() {
    double normal[3], center[3];
    getNormal(normal);
    getCenter(center);
    this->planeSource->SetNormal(normal);
    this->planeSource->SetCenter(center);
    this->planeSource->Update();
    for(auto source : sources){
        source->update();
    }
}

string PDTVtkPlanePlacement::printInfo() {
    Vector3 m_center = this->fullMonteData->basis().origin();
    Vector3 m_normal = this->fullMonteData->basis().normal();

    ostringstream ss;
    ss << "Plane Placement : "
       << setprecision(3)
       << "Origin ("
       << m_center[0] << "," << m_center[1] << "," << m_center[2] << ") "
       << "Normal ("
       << m_normal[0] << "," << m_normal[1] << "," << m_normal[2] << ") ";

    return ss.str();
}

void PDTVtkPlanePlacement::toXml(QXmlStreamWriter & w) {
    double normal[3], center[3], origin[3], point1[3], point2[3];
    getNormal(normal);
    getCenter(center);;
    getOrigin(origin);
    getPoint1(point1);
    getPoint2(point2);

    w.writeAttribute(SourceListIO::TYPE, SourceListIO::PLANE_PLACEMENT);
    w.writeAttribute(SourceListIO::NORMAL, positionToQString(normal, 3));
    w.writeAttribute(SourceListIO::CENTER, positionToQString(center, 3));
    w.writeAttribute(SourceListIO::ORIGIN, positionToQString(origin, 3));
    w.writeAttribute(SourceListIO::POINT1, positionToQString(point1, 3));
    w.writeAttribute(SourceListIO::POINT2, positionToQString(point2, 3));
}
