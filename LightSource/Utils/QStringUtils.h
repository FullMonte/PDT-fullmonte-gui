#ifndef QSTRINGUTILS_H
#define QSTRINGUTILS_H

#include <QString>
#include <QStringList>

namespace QStringUtils {

template <class T>
QString positionToQString(T* array, int len) {
    assert(array != nullptr);

    QStringList list;
    for(int i = 0 ; i < len; i++) {
        list.push_back(QString::number(array[i]));
    }

    return list.join(',');
}

void qStringToDoubleArray(const QString in, double* array, int len);

}



#endif
