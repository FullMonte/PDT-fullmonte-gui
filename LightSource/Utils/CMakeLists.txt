SET(CMAKE_INCLUDE_CURRENT_DIR ON)

SET(UTILS_SRC
	VectorMaths.h
	QStringUtils.h
        QStringUtils.cpp
)

add_library(LightSourceUtils ${UTILS_SRC})

target_link_libraries(LightSourceUtils FullMonteGeometry FullMontePlacement)
target_include_directories(LightSourceUtils PUBLIC ${CMAKE_SOURCE_DIR})
set_target_properties(LightSourceUtils PROPERTIES LINKER_LANGUAGE CXX)

qt5_use_modules(LightSourceUtils Core)



