#include "QStringUtils.h"

#include <assert.h>

void QStringUtils::qStringToDoubleArray(const QString in, double *array, int len) {
    assert(array != nullptr);

    QStringList list = in.split(',');
    for (int i = 0 ; i < len; i++) {
        array[i] = list[i].toDouble();
    }
}
