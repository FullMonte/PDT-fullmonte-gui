#ifndef VECTORMATHS_H
#define VECTORMATHS_H

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <FullMonteSW/Geometry/Basis.hpp>

using namespace Eigen;

// An Eigen wrapper for vector maths
namespace VectorMaths{
    // In-place normalize
    template <class T>
    void normalize(T a[3]) {
        assert(a != nullptr);
        Matrix<T, 3, 1> m(a);
        m.normalize();
//        cout << m << endl;
        Map<Matrix<T, 3, 1>>(a, m.rows(), m.cols()) = m;
    }

    // Normalize the array and store
    template <class T>
    void normalize(T a[3], T result[3]) {
        assert(a != nullptr);
        assert(result != nullptr);

        for (int i = 0 ; i < 3; i++){
            result[i] = a[i];
        }
        normalize(result);
    }

    // Cross product w = u x v
    template <class T>
    void cross(T u_[3], T v_[3], T w_[3]) {
        assert(u_ != nullptr);
        assert(v_ != nullptr);
        assert(w_ != nullptr);

        Matrix<T, 3, 1> u(u_), v(v_), w;
        w = u.cross(v);
        Map<Matrix<T, 3, 1>>(w_, w.rows(), w.cols()) = w;
    }

    // Implement an fullmonte data version cross product
    Vector3 cross(Vector3 u, Vector3 v) {
        Vector3f eigen_u (u[0], u[1], u[2]), eigen_v(v[0], v[1], v[2]);
        Vector3f result = eigen_u.cross(eigen_v);
        return Vector3({result.data()[0], result.data()[0], result.data()[0]});
    }

    // Covert world coordinates to plane coordinates
    void project(double w[3], Basis basis, double p[3]) {
        Vector3d worldPos(w);
        Vector3d origin((double)(basis.origin()[0]), (double)(basis.origin()[1]), (double)(basis.origin()[2]));
        Matrix3d m;
        m << (double)(basis.basis_x()[0]), (double)(basis.basis_y()[0]), (double)(basis.normal()[0]),
             (double)(basis.basis_x()[1]), (double)(basis.basis_y()[1]), (double)(basis.normal()[1]),
             (double)(basis.basis_x()[2]), (double)(basis.basis_y()[2]), (double)(basis.normal()[2]);

        // plane position = M^-1 * (w-o)
        Vector3d planePos = m.inverse() * (worldPos - origin);
        Map<Matrix<double, 3, 1>>(p, planePos.rows(), planePos.cols()) = planePos;
    }
}

#endif
