#include "SourceListWriter.h"

#include <vector>

#include <QDebug>
#include <QFile>

SourceListWriter::SourceListWriter(const QString filePath) :
    SourceListIO(filePath){

}

bool SourceListWriter::writeSourceList(SourceList& list) {
    QFile file(m_filePath);
    if (! file.open(QIODevice::WriteOnly)) {
        qDebug() << "Failed to write the file";
        return false;
    }

    QXmlStreamWriter stream(&file);
    stream.setAutoFormatting(true);
    stream.writeStartDocument();
    stream.writeStartElement("sourceList");
    
    auto & map = list.getPlanePlacementMap();
    for (auto iter = map.begin(); iter != map.end(); iter ++) {
        int planeId = iter->first;
        stream.writeStartElement(PLANE_PLACEMENT);
        stream.writeAttribute(ID, QString::number(planeId));
        writePlanePlacement(stream, list, planeId);

        vector<int> sourceIds = iter->second.sources;
        for (int id : sourceIds) {
            stream.writeStartElement(SOURCE);
            stream.writeAttribute(ID, QString::number(id));
            writeSource(stream, list, id);
            stream.writeEndElement(); // End of one source
        }

        stream.writeEndElement(); // End of one plane placement

    }

    stream.writeEndElement(); // sourceList
    stream.writeEndDocument();

    file.close();

    return true;
}

void SourceListWriter::writePlanePlacement(QXmlStreamWriter & s, SourceList & list, int pid) {
    if (pid == FREE_PID) {
        return ;
    }

    list.getPlanePlacementById(pid)->toXml(s);
}

void SourceListWriter::writeSource(QXmlStreamWriter & s, SourceList & list, int sid) {
    list.getSourceById(sid)->toXml(s);
}
