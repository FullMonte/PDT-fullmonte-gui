#ifndef SOURCELISTREADER_H
#define SOURCELISTREADER_H

#include <QXmlStreamReader>
#include <QXmlStreamAttributes>

#include "SourceListIO.h"
#include "LightSource/SourceList.h"

class SourceListReader : public SourceListIO {

public:
    SourceListReader(const QString filePath);
    bool readSourceList(SourceList&);

private:
    // The actual read function
    bool read(SourceList&, QXmlStreamReader&);

    // Return -1 if creation is not successful, otherwise return plane id
    int createPlanePlacement(SourceList&, const QXmlStreamAttributes &);

    // Return -1 if creation is not successful, otherwise return source id
    int createSource(SourceList&, const QXmlStreamAttributes &, int);

};




#endif
