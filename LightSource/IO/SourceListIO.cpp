#include "SourceListIO.h"


// Gerneral attributes
const QString SourceListIO::ID = "id";
const QString SourceListIO::TYPE = "type";
const QString SourceListIO::SOURCE = "source";
const QString SourceListIO::PLANE_PLACEMENT = "planePlacement";

// Line attributes
const QString SourceListIO::LINE_SOURCE = "line";
const QString SourceListIO::ENDPOINT1 = "endpoint1";
const QString SourceListIO::ENDPOINT2 = "endpoint2";
const QString SourceListIO::PLANE_POSITION = "planePosition";
const QString SourceListIO::DEPTHS = "depths";

// Plane attributes
const QString SourceListIO::SOURCES = "sources";
const QString SourceListIO::NORMAL = "normal";
const QString SourceListIO::CENTER = "center";
const QString SourceListIO::ORIGIN = "origin";
const QString SourceListIO::POINT1 = "point1";
const QString SourceListIO::POINT2 = "point2";


