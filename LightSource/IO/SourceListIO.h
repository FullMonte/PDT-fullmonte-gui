#ifndef SOURCELISTIO_H
#define SOURCELISTIO_H

#include <QString>

class SourceListIO {

public:
    SourceListIO(const QString filePath): m_filePath(filePath) {}

    // Gerneral attributes
    static const QString ID;
    static const QString TYPE;
    static const QString SOURCE;
    static const QString PLANE_PLACEMENT;

    // Line attributes
    static const QString LINE_SOURCE;
    static const QString ENDPOINT1;
    static const QString ENDPOINT2;
    static const QString PLANE_POSITION;
    static const QString DEPTHS;

    // Plane attributes
    static const QString SOURCES;
    static const QString NORMAL;
    static const QString CENTER;
    static const QString ORIGIN;
    static const QString POINT1;
    static const QString POINT2;

protected:
    QString m_filePath;
};

#endif
