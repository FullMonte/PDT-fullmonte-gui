#ifndef SOURCELISTWRITER_H
#define SOURCELISTWRITER_H


#include <QFile>
#include <QXmlStreamWriter>

#include "SourceListIO.h"
#include "LightSource/SourceList.h"


class SourceListWriter : public SourceListIO {
public:
    SourceListWriter(const QString filePath);
    bool writeSourceList(SourceList&);


private:
    void writePlanePlacement(QXmlStreamWriter &, SourceList& , int);
    void writeSource(QXmlStreamWriter &, SourceList&, int);

};


#endif
