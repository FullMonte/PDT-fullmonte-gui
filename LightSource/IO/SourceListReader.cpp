#include "SourceListReader.h"

#include <QDebug>
#include <QFile>

#include "LightSource/PDTVtkLineSource.h"
#include "LightSource/PDTVtkPlanePlacement.h"
#include "LightSource/Utils/QStringUtils.h"

using namespace QStringUtils;

SourceListReader::SourceListReader(const QString filePath) : 
    SourceListIO(filePath) {

}

bool SourceListReader::readSourceList(SourceList& list) {    
    QFile file(m_filePath);
    if (! file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return false;
    }

    QXmlStreamReader reader(&file);
    bool isValidSourceFile = true;

    if (reader.readNextStartElement()) {
        if (reader.name() == "sourceList") {
            isValidSourceFile = read(list, reader);
        } else {
            qDebug() << "Not a valid source list file";
            isValidSourceFile = false;
        }
    }

    reader.clear();
    file.close();

    return isValidSourceFile;
}

bool SourceListReader::read(SourceList& list, QXmlStreamReader& reader) {
    int curPid = -1;

    // Parse the source xml line by line
    while (reader.readNextStartElement() && !reader.hasError()) {
        qDebug() << "reading" << reader.name();

        if (reader.name() == PLANE_PLACEMENT) {
            curPid = createPlanePlacement(list, reader.attributes());
            if (curPid == -1) {
                qDebug() << "Plane placement creation failed. Skipping";
                reader.skipCurrentElement();
            } else {
                qDebug() << "Plane placement " << curPid << " is created successfully";
            }

            // Read each source
            while (reader.readNextStartElement() && !reader.hasError()) {
                if (reader.name() == SOURCE) {
                    int sid = createSource(list, reader.attributes(), curPid);
                    if (sid == -1) {
                        qDebug() << "Source creation failed. Skipping";
                    } else {
                        qDebug() << "Source " << sid << " is created successfully";
                    }
                } else {
                    qDebug() << "Unkown element " << reader.name();

                }
                reader.skipCurrentElement();
            }
        } else {
            qDebug() << "Unkown element " << reader.name();
        }
    }

    if (reader.hasError()) {
        qDebug() << reader.errorString();
        return false;
    }    

    return true;
}

int SourceListReader::createPlanePlacement(SourceList& list, const QXmlStreamAttributes & attributes) {
    if (!attributes.hasAttribute(ID)) {
        qDebug() << "Plane placement is missing ID.";
        return -1;
    }

    int pid = attributes.value(ID).toInt();
    if (pid == FREE_PID) {
        return pid;
    }

    if (!attributes.hasAttribute(NORMAL)  || !attributes.hasAttribute(CENTER)
            || !attributes.hasAttribute(ORIGIN)  || !attributes.hasAttribute(POINT1) || !attributes.hasAttribute(POINT2)) {
        qDebug() << "Plane placement is missing position information.";
        return -1;
    }

    // Actual creation for plane placement
    double center[3], normal[3], origin[3], point1[3], point2[3];
    qStringToDoubleArray(attributes.value(CENTER).toString(), center, 3);
    qStringToDoubleArray(attributes.value(NORMAL).toString(), normal, 3);
    qStringToDoubleArray(attributes.value(ORIGIN).toString(), origin, 3);
    qStringToDoubleArray(attributes.value(POINT1).toString(), point1, 3);
    qStringToDoubleArray(attributes.value(POINT2).toString(), point2, 3);

    PDTVtkPlanePlacement * planePlacement = new PDTVtkPlanePlacement(center, normal, origin, point1, point2);
    list.insertPlanePlacement(planePlacement, pid);

    return pid;
}

int SourceListReader::createSource(SourceList& list, const QXmlStreamAttributes & attributes, int pid) {
    if (!attributes.hasAttribute(ID)) {
        qDebug() << "Source is missing ID.";
        return -1;
    }

    // Create source and add to list here
    int sid = attributes.value(ID).toInt();
    QString type = attributes.value(TYPE).toString();

    if (type == LINE_SOURCE) {
        if (!attributes.hasAttribute(ENDPOINT1) || !attributes.hasAttribute(ENDPOINT2)) {
            qDebug() << "Source is missing position information.";
            return -1;
        }

        double e0[3], e1[3];
        qStringToDoubleArray(attributes.value(ENDPOINT1).toString(), e0, 3);
        qStringToDoubleArray(attributes.value(ENDPOINT2).toString(), e1, 3);
        PDTVtkLineSource* source = new PDTVtkLineSource(e0, e1);
        list.insertSource(source, pid, sid);

    } else {
        qDebug() << "Source type" << type << "is not supported";
        return -1;
    }

    return sid;
}

