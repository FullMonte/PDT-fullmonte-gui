#include "PDTVtkLineSource.h"

#include <array>
#include <memory>
#include <iomanip>

#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>

#include <QDebug>

#include <FullMonteSW/Geometry/Sources/Line.hpp>

#include "Utils/QStringUtils.h"
#include "IO/SourceListIO.h"

#include <sstream>

using namespace QStringUtils;

//constructor
PDTVtkLineSource::PDTVtkLineSource(){

}

//second constructor with two array and one float as pass in arguement
PDTVtkLineSource::PDTVtkLineSource(double e0[], double e1[], float w) {
    assert(e0 != nullptr);//abort program execution of e0 or e1 is null
    assert(e1 != nullptr);

	// Store source in full monte data
    array<float,3> ep0 = {(float)e0[0], (float)e0[1], (float)e0[2]}; //c++ template array consists of three elements of float
    array<float,3> ep1 = {(float)e1[0], (float)e1[1], (float)e1[2]};

    this->fullMonteData = unique_ptr<Source::Line>(new Source::Line(w, ep0, ep1));
    this->sourceType = LINE;

    // Add souce to vtk view
    lineSource->SetPoint1(e0);
    lineSource->SetPoint2(e1);
    lineSource->Update();

    //draw line
    vtkNew<vtkPolyDataMapper> mapper;
    mapper->SetInputConnection(lineSource->GetOutputPort());
    actor->SetMapper(mapper.Get());
    actor->GetProperty()->SetColor(PDTVtkColor::ORANGE[0], PDTVtkColor::ORANGE[1], PDTVtkColor::ORANGE[2]);
}

PDTVtkLineSource::~PDTVtkLineSource() {

}

shared_ptr<PlacementMediatorBase> PDTVtkLineSource::generatePlacementMediator(PDTVtkPlanePlacement* planePlacement) {
    shared_ptr<PlanePlacementLineSource> pm (new PlanePlacementLineSource);
    pm->source(this->fullMonteData.get());
    pm->placement(planePlacement->getFullMonteData());

    // Change of basis
    Source::Line* line = (Source::Line *)this->fullMonteData.get();
    Point3 e0 = line->endpoint(0);//setting two end point using 3D coordinates
    Point3 e1 = line->endpoint(1);

    double worldPos0[3] = {(double)e0[0], (double)e0[1], (double)e0[2]};
    double worldPos1[3] = {(double)e1[0], (double)e1[1], (double)e1[2]};
    double planePos0[3], planePos1[3];
    planePlacement->projectToPlane(worldPos0, planePos0);
    planePlacement->projectToPlane(worldPos1, planePos1);

    pm->planePosition({(float)planePos0[0], (float)planePos0[1]});
    // clear endpoints first
    pm->pull(pm->length());
    pm->extend(-pm->tipDepth());

    pm->push(planePos0[2]);
    pm->extend(planePos1[2] - planePos0[2]);

    qDebug()<< "Tip depth" << pm->tipDepth();

    placementMediator = pm;
    return pm;
}


double PDTVtkLineSource::GetPower(){
    Source::Line* line = (Source::Line*)this->fullMonteData.get();
    double power = (double)line->power();
    return power;
}

void PDTVtkLineSource::SetPower(double Power){
    Source::Line* line = (Source::Line*)this->fullMonteData.get();
    line->power((float)Power);
}

void PDTVtkLineSource::getEndpoints(double e0[3], double e1[3]) {
    Source::Line* line = (Source::Line *)this->fullMonteData.get();
    Point3 p0 = line->endpoint(0);
    Point3 p1 = line->endpoint(1);

    for (int i = 0; i < 3; i++){
        e0[i] = (double)p0[i];
        e1[i] = (double)p1[i];
    }
}

void PDTVtkLineSource::setEndpoints(double e0[3], double e1[3]) {
    Source::Line* line = (Source::Line *)this->fullMonteData.get();
    line->endpoint(0, {(float)e0[0], (float)e0[1], (float)e0[2]});
    line->endpoint(1, {(float)e1[0], (float)e1[1], (float)e1[2]});
}

void PDTVtkLineSource::setPlanePosition(double planePos[2], double depths[2]) {
    assert(placementMediator.get() != nullptr);
    PlanePlacementLineSource* planePlacementLineSource = (PlanePlacementLineSource *)this->placementMediator.get();
    planePlacementLineSource->planePosition({(float)planePos[0], (float)planePos[1]});

    // Due to the Full Monte data's shortcomming, we are not able to simpily set the endDepths
    // TODO: Add helper function in FullMonte
    float oldLength = planePlacementLineSource->length();
    float tailDepth = planePlacementLineSource->tipDepth() - oldLength;
    planePlacementLineSource->push((float)depths[0]-tailDepth);
    planePlacementLineSource->extend(depths[1]-depths[0]-oldLength);

    planePlacementLineSource->update();
} 

void PDTVtkLineSource::getPlanePosition(double planePos[2], double depths[2]){
    assert(placementMediator.get() != nullptr);
    PlanePlacementLineSource* planePlacementLineSource = (PlanePlacementLineSource *)this->placementMediator.get();
    Point2 m_planePos = planePlacementLineSource->planePosition();
    Point2 m_depths = {planePlacementLineSource->tipDepth() - planePlacementLineSource->length(),
                            planePlacementLineSource->tipDepth()};
    for (int i = 0 ; i < 2; i++){
        planePos[i] = (double) m_planePos[i];
        depths[i] = (double) m_depths[i];
    }
}

void PDTVtkLineSource::update() {
    Source::Line* line = (Source::Line *)this->fullMonteData.get();
    Point3 e0 = line->endpoint(0);
    Point3 e1 = line->endpoint(1);
    lineSource->SetPoint1((double)e0[0], (double)e0[1], (double)e0[2]);
    lineSource->SetPoint2((double)e1[0], (double)e1[1], (double)e1[2]);
    lineSource->Update();


}

string PDTVtkLineSource::printInfo() {
    Source::Line* line = (Source::Line *)this->fullMonteData.get();
    double power = line->power();
    Point3 e0 = line->endpoint(0);
    Point3 e1 = line->endpoint(1);

    ostringstream ss;
    ss << "Line Source : "
       << setprecision(3)
       << "power"
       << power
       << "Endpoint1 ("
       << e0[0] << "," << e0[1] << "," << e0[2] << ") "
       << "Endpoint2 ("
       << e1[0] << "," << e1[1] << "," << e1[2] << ") ";

    if (placementMediator.get() != nullptr) {
        PlanePlacementLineSource* planePlacementLineSource = (PlanePlacementLineSource *)this->placementMediator.get();
        Point2 m_planePos = planePlacementLineSource->planePosition();
        Point2 m_depths = {planePlacementLineSource->tipDepth() - planePlacementLineSource->length(),
                                planePlacementLineSource->tipDepth()};
        ss << "Plane position ("
           << m_planePos[0] << "," << m_planePos[1] << ") "
           << "Depths ("
           << m_depths[0] << "," << m_depths[1] << ") ";

    }

    return ss.str();
}

void PDTVtkLineSource::toXml(QXmlStreamWriter & w) {
    double e1[3], e2[3];
    getEndpoints(e1, e2);
    w.writeAttribute(SourceListIO::TYPE, SourceListIO::LINE_SOURCE);
    w.writeAttribute(SourceListIO::ENDPOINT1, positionToQString(e1, 3));
    w.writeAttribute(SourceListIO::ENDPOINT2, positionToQString(e2, 3));

    if (placementMediator.get() != nullptr) {
        double planePos[2], depths[2];
        getPlanePosition(planePos, depths);
        w.writeAttribute(SourceListIO::PLANE_POSITION, positionToQString(planePos, 2));
        w.writeAttribute(SourceListIO::DEPTHS, positionToQString(depths, 2));
    }
}
