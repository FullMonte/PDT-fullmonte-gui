#ifndef PDTVTKSOURCE_H
#define PDTVTKSOURCE_H

#include <memory>

#include <vtkActor.h>
#include <vtkNew.h>

#include <FullMonteSW/Geometry/Placement/PlacementMediatorBase.hpp>
#include <FullMonteSW/Geometry/Sources/Abstract.hpp>

#include "PDTVtkPlanePlacement.h"
#include "PDTVtkObject.h"

using namespace std;

enum PDTVtkSouceType {
    LINE,
    POINT,
    FIBER
    //cut-end fibre to be implemented
};

class PDTVtkSource : public PDTVtkObject<Source::Abstract> {
public :
    PDTVtkSource () { objectType = SOURCE; }
    ~PDTVtkSource() {}
   
    // Getters
    PDTVtkSouceType getSourceType() { return sourceType; }

    virtual shared_ptr<PlacementMediatorBase> generatePlacementMediator(PDTVtkPlanePlacement* ) = 0;

protected :
    PDTVtkSouceType sourceType;
    shared_ptr<PlacementMediatorBase> placementMediator;
};


#endif
