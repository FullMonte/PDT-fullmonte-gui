#include "pdtvtkpointsource.h"

#include <array>
#include <memory>
#include <iomanip>

#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>

#include <QDebug>

#include <FullMonteSW/Geometry/Sources/Point.hpp>

#include "Utils/QStringUtils.h"
#include "IO/SourceListIO.h"


using namespace QStringUtils;

PDTVtkPointSource::PDTVtkPointSource(){

}

PDTVtkPointSource::PDTVtkPointSource(double e0[3],float w){

    assert(e0 !=nullptr);

    array<float,3> ep0 = {(float)e0[0],(float)e0[1],(float)e0[2]};
    this->fullMonteData = unique_ptr<Source::Point>(new Source::Point(w,ep0));
    this->sourceType=POINT;

    //Add source to vtk view

    pointSource->SetCenter(e0);
    pointSource->Update();

    vtkNew<vtkPolyDataMapper> mapper;
    mapper->SetInputConnection(pointSource->GetOutputPort());
    actor->SetMapper(mapper.Get());
    actor->GetProperty()->SetColor(PDTVtkColor::ORANGE[0], PDTVtkColor::ORANGE[1], PDTVtkColor::ORANGE[2]);




}


PDTVtkPointSource::~PDTVtkPointSource(){

}

double PDTVtkPointSource::GetPower(){
    Source::Point* point = (Source::Point *) this->fullMonteData.get();
    double power = (double)point->power();
    return power;
}

void PDTVtkPointSource::SetPower(double Point){
    Source::Point* point = (Source::Point *) this->fullMonteData.get();
    point->power((float)Point);
    
}

void PDTVtkPointSource::getEndpoints(double e0[3]){

    Source::Point* my_point = (Source::Point *) this->fullMonteData.get();
    Point3 m_point= my_point->position();

    for(int i=0;i<3;i++){
        e0[i]=(double)m_point[i];
    }

}


void PDTVtkPointSource::setEndPoints(double e0[3]){

    assert(fullMonteData.get() != nullptr);

    Source::Point * point = (Source::Point *) (this->fullMonteData.get());
    assert(point != nullptr);

    Point3 e0_f;

    //Jeff's takes float instead of double
    for(int i=0;i<3;i++){
        e0_f[i]=(float) e0[i];
    }
    point->position(e0_f);


}


//Update the vtk object based on the pdt_object
//Called immediately after PointSourcePlacementWidget::modifySource



void PDTVtkPointSource::update(){
    Source::Point *point = (Source::Point *) this->fullMonteData.get();
    Point3 e0= point->position();
    std::array<double,3> e0_d;

    // vtk takes double
    for(int i=0;i<3;i++){
        e0_d[i]=(double) e0[i];
    }
    pointSource->SetCenter(e0_d[0],e0_d[1],e0_d[2]);

    pointSource->Update();

}


shared_ptr<PlacementMediatorBase> PDTVtkPointSource::generatePlacementMediator(PDTVtkPlanePlacement* ) {

    std::cout<<"Point Source generate placement mediator"<<std::endl;

}

string PDTVtkPointSource::printInfo() {
    Source::Point* point = (Source::Point*)this->fullMonteData.get();
    double power = point->power();
    Point3 center = point->position();


    ostringstream ss;
    ss << "Point Source : "
       << setprecision(3)
       << "power"
       << power
       << "Position ("
       << center[0] << "," << center[1] << "," << center[2] << ") ";

    return ss.str();
}


void PDTVtkPointSource::toXml(QXmlStreamWriter & w){

    std::cout<<"Point Source toXml"<<std::endl;
}

