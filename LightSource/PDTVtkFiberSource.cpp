#include "PDTVtkFiberSource.h"
#include <array>
#include <memory>
#define _USE_MATH_DEFINES
#include <iomanip>
#include <cmath>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>

#include <QDebug>

#include <FullMonteSW/Geometry/Sources/Fiber.hpp>

#include "Utils/QStringUtils.h"
#include "IO/SourceListIO.h"

#include <sstream>


using namespace QStringUtils;

//constructor
PDTVtkFiberSource::PDTVtkFiberSource(){
    
}

//second constructor with two array and one float as pass in arguement

PDTVtkFiberSource::PDTVtkFiberSource(double Radius, double center[3],double direction[3],double NA,float w,int resolution){
    cout << "new fiber source construction" << endl;
    assert(center!= nullptr);//make sure array is not empty
    assert(direction!= nullptr);
    array<float,3> center_FullMonte = {(float)center[0],(float)center[1],(float)center[2]};
    array<float,3> direction_FullMonte = {(float)direction[0],(float)direction[1],(float)direction[2]};
    
    //constructor for Fiber
    this->fullMonteData = unique_ptr<Source::Fiber>(new Source::Fiber(w,NA,direction_FullMonte,center_FullMonte,Radius));
    this->sourceType = FIBER;


    //add source to VTK view
    //calculate the correct height by using radius and angle of cone
    FiberSource->SetHeight(Radius/tan(asin(NA)));
    FiberSource->SetRadius(Radius);
    FiberSource->SetCenter(center[0],center[1],center[2]);
    FiberSource->SetDirection(direction[0],direction[1],direction[2]);
    FiberSource->SetAngle(asin(NA)*180/M_PI);//asin returns angle in radians, convert to degree because VTK receives degreee
    FiberSource->SetCapping(true);
    FiberSource->SetResolution(20);
    FiberSource->Update();

    //draw cone
    vtkNew<vtkPolyDataMapper> mapper;
    mapper->SetInputConnection(FiberSource->GetOutputPort());
    actor->SetMapper(mapper.Get());
    actor->GetProperty()->SetColor(PDTVtkColor::YELLOW[0],PDTVtkColor::YELLOW[1],PDTVtkColor::YELLOW[3]);
}
 

PDTVtkFiberSource::~PDTVtkFiberSource() {

}


void PDTVtkFiberSource::SetPower(double Power){
    Source::Fiber* fiber = (Source::Fiber*)this->getFullMonteData();
    fiber->power((float)Power);
}

double PDTVtkFiberSource::GetPower(){
    Source::Fiber* fiber = (Source::Fiber*)this->getFullMonteData();
    double power = (double)fiber->power();
    return power;
}


void PDTVtkFiberSource::SetRadius(double Radius){
    Source::Fiber* fiber = (Source::Fiber*)this->getFullMonteData();
    fiber->radius((float)Radius);
}


double PDTVtkFiberSource::GetRadius(){
    Source::Fiber* fiber = (Source::Fiber*)this->getFullMonteData();
    double radius = (double)fiber->radius();
    return radius;
}



void PDTVtkFiberSource::SetCenter(double x,double y,double z){
    Source::Fiber* fiber = (Source::Fiber*)this->getFullMonteData();
    array<float,3> center = {(float)x,(float)y,(float)z};
    fiber->fiberPos(center); 
}


void PDTVtkFiberSource::GetCenter(double data[3]){
    Source::Fiber* fiber = (Source::Fiber*)this->getFullMonteData();
    
    for(int i =0;i < 3;i++){
        data[i] = (double)fiber->fiberPos()[i];
    }
}

void PDTVtkFiberSource::SetDirection(double x,double y,double z){
    Source::Fiber* fiber = (Source::Fiber*)this->getFullMonteData();
    array<float,3> direction = {(float)x,(float)y,(float)z};
    fiber->fiberDir(direction);    
}


void PDTVtkFiberSource::GetDirection(double data[3]){
    Source::Fiber* fiber = (Source::Fiber*)this->getFullMonteData();
    
    for(int i = 0; i <3 ;i++){
        data[i] = (double)fiber->fiberDir()[i];
    }
}


void PDTVtkFiberSource::SetNA(double NA){
    Source::Fiber* fiber = (Source::Fiber*)this->getFullMonteData();
    fiber->numericalAperture((float)NA);
}


double PDTVtkFiberSource::GetNA(){
    Source::Fiber* fiber = (Source::Fiber*)this->getFullMonteData();
    double NA = (double)fiber->numericalAperture();
    return NA;
}



shared_ptr<PlacementMediatorBase> PDTVtkFiberSource::generatePlacementMediator(PDTVtkPlanePlacement* ) {

    std::cout<<"Fiber Source generate placement mediator"<<std::endl;

}


void PDTVtkFiberSource::update() {
    Source::Fiber* fiber = (Source::Fiber*)this->fullMonteData.get();
    
    double power = (double)fiber->power();
    double radius = (double)fiber->radius();
    double center[3] = {(double)fiber->fiberPos()[0],(double)fiber->fiberPos()[1],(double)fiber->fiberPos()[2]};
    double direction[3] = {(double)fiber->fiberDir()[0],(double)fiber->fiberDir()[1],(double)fiber->fiberDir()[2]};
    double NA = (double)fiber->numericalAperture();
    

    FiberSource->SetHeight(radius/tan(asin(NA)));
    FiberSource->SetRadius(radius);
    FiberSource->SetCenter(center);
    FiberSource->SetDirection(direction);
    FiberSource->SetAngle(asin(NA)*180/M_PI);
    FiberSource->Update();

}

string PDTVtkFiberSource::printInfo() {
    Source::Fiber* fiber = (Source::Fiber*)this->fullMonteData.get();
    float power = fiber->power();
    float radius = fiber->radius();
    Point3 center = fiber->fiberPos();
    Point3 direction = fiber->fiberDir();
    float NA = fiber->numericalAperture();

    ostringstream ss;
    ss <<"Fiber Source: "
       <<setprecision(3)
       <<"power"
       << power
       <<"radius "
       << radius <<" "
       <<"center ("
       << center[0]<<","<< center[1] <<","<< center[2] << ") "
       <<"direction ("
       << direction[0]<<","<<direction[1]<<","<<direction[2]<<") "
       <<"NA "
       << NA;

    return ss.str();
}




