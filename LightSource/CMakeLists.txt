add_subdirectory(Utils)
add_subdirectory(IO)

SET(CMAKE_INCLUDE_CURRENT_DIR ON)

SET(LIGHT_SOURCE_SRC
        PDTVtkObject.h
	PDTVtkLineSource.h
        PDTVtkLineSource.cpp
	PDTVtkPlanePlacement.cpp
	PDTVtkPlanePlacement.h
	PDTVtkSource.h
        PDTVtkSource.h
        SourceList.h
        SourceList.cpp
        pdtvtkpointsource.h
        pdtvtkpointsource.cpp
        PDTVtkFiberSource.h
        PDTVtkFiberSource.cpp
)

add_library(LightSource ${LIGHT_SOURCE_SRC})

target_link_libraries(LightSource FullMonteGeometry FullMontePlacement FullMonteTIMOS)
target_link_libraries(LightSource LightSourceUtils LightSourceIO)
target_include_directories(LightSource PUBLIC ${CMAKE_SOURCE_DIR})
qt5_use_modules(LightSource Core)


