#ifndef PDTVTKOBJECT_H
#define PDTVTKOBJECT_H

#include <memory>

#include <QXmlStreamWriter>

#include <vtkActor.h>
#include <vtkNew.h>
#include <vtkProperty.h>

using namespace std;

enum PDTVtkObjectType {
    SOURCE,
    PLACEMENT
};

class PDTVtkColor {
public:
    static constexpr double YELLOW [3] = {1.0, 1.0, 0.0};
    static constexpr double BLUE [] =  {0.0, 0.0, 1.0};
    static constexpr double RED [] = {1.0, 0.0, 0.0};
    static constexpr double GREEN [] =  {0.0, 1.0, 0.0};
    static constexpr double ORANGE [] =  {1.0, 0.5, 0.0};
    static constexpr double WHITE [] =  {1.0, 1.0, 1.0};
};


template <class FullMonteDataType> class PDTVtkObject {
public :
    PDTVtkObject () {}
    ~PDTVtkObject() {}
   
    // Getters
    vtkActor* getActor() { return actor.Get(); }
    FullMonteDataType* getFullMonteData() { return fullMonteData.get(); }
    PDTVtkObjectType getObjectType() {return objectType; }

    // Highlight/Dishighlight object in vtk view
    virtual void setHighlight(bool enable) {
        if(enable) {
            actor->GetProperty()->SetColor(PDTVtkColor::RED[0], PDTVtkColor::RED[1], PDTVtkColor::RED[2]);
        } else {
            actor->GetProperty()->SetColor(PDTVtkColor::ORANGE[0], PDTVtkColor::ORANGE[1], PDTVtkColor::ORANGE[2]);
        }
    }

    // Update the vtk object to match the fullMonte data
    virtual void update() = 0;

    // Return the information about the object
    virtual string printInfo() = 0;

    virtual void toXml(QXmlStreamWriter & w) {}

protected :
    vtkNew<vtkActor> actor;

    unique_ptr<FullMonteDataType> fullMonteData;

    PDTVtkObjectType objectType;
    

};

#endif
