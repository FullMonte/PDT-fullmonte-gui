#include "SourceList.h"

#include <vtkPointSource.h>
#include <vtkRenderer.h>

SourceList::SourceList(vtkSmartPointer<vtkRenderer> renderer) {
    this->renderer = renderer;
    reset();
}

SourceList::~SourceList() {
    reset();
}

void SourceList::addSource(PDTVtkSource * source, int pid) {
    insertSource(source, pid, curSid);
}

bool SourceList::insertSource(PDTVtkSource* source, int pid, int sid) {
    assert (source != nullptr);

    if (isSourceExist(sid)) {
        cout << "Warning: Inserting source id already exists in source list" << endl;
        return false;
    }

    // Create an new source element
    SourceElement s = {
        sid,   // SID
        pid,    // PID
        shared_ptr<PDTVtkSource>(source)    // PDTVtkSource ptr
    };

    sourceMap.insert(make_pair(sid, s));

    // Add the source to renderer
    renderer->AddActor(source->getActor());

    auto iter = planePlacementMap.find(pid);
    assert(iter != planePlacementMap.end());

    PlanePlacementElement & p = iter->second;
    p.sources.push_back(sid);

    // Add source to plane placement if it is not free source
    if (pid != FREE_PID) {
        p.planePlacement->addSource(source);
    }

    // Avoid id conflict
    if (sid >= curSid) {
        curSid = sid + 1;
    } 

    return true;
}

void SourceList::addPlanePlacement(PDTVtkPlanePlacement* planePlacement) {
    insertPlanePlacement(planePlacement, curPid);
}

bool SourceList::insertPlanePlacement(PDTVtkPlanePlacement* planePlacement, int pid) {
    if (isPlanePlacementExist(pid)) {
        cout << "Warning: Inserting plane placement id already exists in list" << endl;
        return false;
    }

    PlanePlacementElement p; 
	p.pid = pid;
	p.planePlacement = shared_ptr<PDTVtkPlanePlacement>(planePlacement);

	this->planePlacementMap.insert(make_pair(pid, p));

    // Add the source the renderer
    renderer->AddActor(planePlacement->getActor());

    // Avoid id conflict
    if (pid >= curPid) {
        curPid = pid + 1;
    } 

    return true;
}

void SourceList::reset() {
    for (auto iter = planePlacementMap.begin(); iter != planePlacementMap.end(); iter ++) {
        removeSourcesByPid(iter->first);
    }

    this->sourceMap.clear();
    this->planePlacementMap.clear();

    PlanePlacementElement freePlane;
    freePlane.pid = FREE_PID;
    planePlacementMap.insert(make_pair(FREE_PID, freePlane));
}

PDTVtkSource* SourceList::getSourceById(int sid) {
    assert(isSourceExist(sid));
    return sourceMap.at(sid).source.get();
}

void SourceList::removeSourceById(int sid) {
    assert(isSourceExist(sid));
    // Remove from the list and vtk view
    int pid = sourceMap.at(sid).pid;
    shared_ptr<PDTVtkSource> source =sourceMap.at(sid).source;
    renderer->RemoveActor(source->getActor());

    // Erase from the plane placement
    vector<int> & sources = planePlacementMap.at(pid).sources;
    sources.erase(std::find(sources.begin(), sources.end(), sid));

    if (pid != FREE_PID) {
        planePlacementMap.at(pid).planePlacement->removeSource(source.get());
    }

    sourceMap.erase(sid);
}

void SourceList::removeSourcesByPid(int pid) {
    assert(isPlanePlacementExist(pid));
    if (pid != FREE_PID) {
        // Remove from the list and vtk view
        PDTVtkPlanePlacement* planePlacement = planePlacementMap.at(pid).planePlacement.get();
        renderer->RemoveActor(planePlacement->getActor());
    }

    // Remove all its sources
    for (int sid : planePlacementMap.at(pid).sources) {
        removeSourceById(sid);
    }
    planePlacementMap.erase(pid);
}

void SourceList::setSourceHighlightedById(int sid, bool enable) {
    assert(isSourceExist(sid));
    sourceMap.at(sid).source->setHighlight(enable);
}

void SourceList::setPlanePlacementHighlightedById(int pid, bool enable) {
    assert(isPlanePlacementExist(pid));
    if(pid != FREE_PID){
        planePlacementMap.at(pid).planePlacement->setHighlight(enable);
    }
    for(int id : planePlacementMap.at(pid).sources){
        sourceMap.at(id).source->setHighlight(true);
    }
}

bool SourceList::isSourceExist(int sid) {
    return sourceMap.find(sid) != sourceMap.end();
}

bool SourceList::isPlanePlacementExist(int pid) {
    return planePlacementMap.find(pid) != planePlacementMap.end();
}

PDTVtkPlanePlacement* SourceList::getPlanePlacementById(int pid) {
    assert(pid != FREE_PID);
    assert(isPlanePlacementExist(pid));
    return planePlacementMap.at(pid).planePlacement.get();
}

void SourceList::clearAllHighlights() {
    for (auto iter = sourceMap.begin(); iter != sourceMap.end(); iter ++){
        iter->second.source->setHighlight(false);
    }
    for (auto iter = planePlacementMap.begin(); iter != planePlacementMap.end(); iter ++){
        if(iter->first != FREE_PID) {
            iter->second.planePlacement->setHighlight(false);
        }
    }
}

void SourceList::showSource(int sid) {
    assert(isSourceExist(sid));
    cout<<"Entered show source"<<endl;
    renderer->AddActor(sourceMap.at(sid).source->getActor());
}


void SourceList::showAllSource(){

    for(std::map<int, SourceElement>::iterator it=sourceMap.begin();it!=sourceMap.end();++it){
        showSource(it->first);
    }



}

void SourceList::showPlanePlacement(int pid){
    assert(isPlanePlacementExist(pid));
    if (pid != FREE_PID){
        renderer->AddActor(planePlacementMap.at(pid).planePlacement->getActor());
    }
    for(int id : planePlacementMap.at(pid).sources){
        showSource(id);
    }
}

void SourceList::hideSource(int sid) {
    assert(isSourceExist(sid));
    renderer->RemoveActor(sourceMap.at(sid).source->getActor());
}

void SourceList::hidePlanePlacement(int pid){
    assert(isPlanePlacementExist(pid));
    if (pid != FREE_PID){
        renderer->RemoveActor(planePlacementMap.at(pid).planePlacement->getActor());
    }
    for(int id : planePlacementMap.at(pid).sources){
        hideSource(id);
    }
}

bool SourceList::isFreeSource(int sid) {
    assert(isSourceExist(sid));
    return sourceMap.at(sid).pid == FREE_PID;
}

int SourceList::getSourcePlanePlacementId(int sid) {
    assert(isSourceExist(sid));
    return sourceMap.at(sid).pid;
}

string SourceList::getSourceInfoById(int sid) {
    assert(isSourceExist(sid));
    return "ID " + to_string(sid) + " " + sourceMap.at(sid).source->printInfo();
}

string SourceList::getPlanePlacementInfoById(int pid) {
    assert(isPlanePlacementExist(pid));
    if (pid == FREE_PID) {
            return "Free Plane Placement";
    }
    return "ID " + to_string(pid) + " " + planePlacementMap.at(pid).planePlacement->printInfo();
}

std::vector<int> SourceList::getSourcesID(){
    std::vector<int> SourceIDs;
    for( map<int, SourceElement>::iterator it=sourceMap.begin(); it !=sourceMap.end();++it){
        SourceIDs.push_back(it->first);
    }
    return SourceIDs;
}

int SourceList::getID(){

    return this->curSid;
}
