#ifndef PDTOPTICALTREEWIDGET_H
#define PDTOPTICALTREEWIDGET_H

#include <QTreeWidget>

#include <QTreeWidgetItem>

#include "qtreewidget.h"

#include <QPoint>

class PDTOpticalTreeWidget : public QTreeWidget{

    Q_OBJECT
public:
    PDTOpticalTreeWidget(QWidget *parent);



public slots:
    // signal emitted to invoke function in PDTWorkPageWidget
    void emit_prepare_menu_PDTWPW(QString item_name);
    void emit_delete_item(QString item_name,QPoint _Position);
    void onItemClicked(QTreeWidgetItem*,int);
    void prepareMenu(const QPoint& pos);




private slots:


signals:
    void modify_signal(QString item_name);
    void delete_item_signal(QString item_name,QPoint _Position);



};

#endif // PDTOPTICALTREEWIDGET_H
