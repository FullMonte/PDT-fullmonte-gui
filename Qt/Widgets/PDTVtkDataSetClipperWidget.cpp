#include "PDTVtkDataSetClipperWidget.h"
#include "ui_PDTVtkToolWidget.h"

#include <vtkActor.h>
#include <vtkRenderer.h>

#include "VTK/Core/pVTKReader.h"
#include "VTK/Tools/pVTKDataSetClipper.h"

PDTVtkDataSetClipperWidget::PDTVtkDataSetClipperWidget(pVTKReader* reader, vtkActor* actor, vtkRenderer* renderer,
                                                       PDTWorkPageWidget* workPage, QWidget *parent):
    PDTVtkToolWidget(reader, actor, workPage, parent)
{
    // Setup vtk tool
    this->vtkTool = unique_ptr<pVTKDataSetClipper>(new pVTKDataSetClipper());
    this->vtkTool->setRenderer(renderer);
    this->vtkTool->setActor(actor);
    this->vtkTool->setInput(reader);

    type = DATASET_CLIPPER;

}
