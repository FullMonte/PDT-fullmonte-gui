#include "PDTSourceTreeWidget.h"
#include <QMenu>
#include <QMouseEvent>

#define COLUMN_0 0
#define COLUMN_1 1

//two types of source point and line
const QString FREE_SOURCE_STR = QString("Free Source");
const QString LINE_STR = QString("Line");
const QString POINT_STR = QString("Point");
const QString Fiber_STR = QString("Fiber");

PDTSourceTreeWidget::PDTSourceTreeWidget(QWidget* parent): QTreeWidget(parent) {
	setContextMenuPolicy(Qt::CustomContextMenu);
    setSelectionMode(QAbstractItemView::ExtendedSelection);
    setSelectionBehavior(QAbstractItemView::SelectRows);

    connect(this,
            SIGNAL(customContextMenuRequested(QPoint)),
            SLOT(prepareMenu(QPoint)));

    connect(this,
            SIGNAL(itemClicked(QTreeWidgetItem*,int)),
            SLOT(onItemClicked(QTreeWidgetItem*,int)));

}

PDTSourceTreeWidget::~PDTSourceTreeWidget() {

}

void PDTSourceTreeWidget::update() {
    assert(m_sourceList != nullptr);

    this->clear();

	auto & planePlacementMap = m_sourceList->getPlanePlacementMap();
	for (auto iter = planePlacementMap.begin(); iter != planePlacementMap.end(); iter ++) {

		QTreeWidgetItem* newPlaneItem = new QTreeWidgetItem(this, PLANE_ITEM_TYPE);	
        if (iter->first == FREE_PID) {
            newPlaneItem->setText(COLUMN_0, FREE_SOURCE_STR); //column 0 is type of source or source ID
        } else {
            newPlaneItem->setText(COLUMN_0, QString::number(iter->first));
        }
        newPlaneItem->setText(COLUMN_1, QString("PlanePlacement"));//column 1 by default is planeplacement
        newPlaneItem->setExpanded(true);

		// Add each source on the plane placement to the table
        vector<int> & sourceIds = iter->second.sources;
        for (int id : sourceIds) {
            QTreeWidgetItem* newSourceItem = new QTreeWidgetItem(newPlaneItem, SOURCE_ITEM_TYPE);//add source and id
            PDTVtkSource* source = m_sourceList->getSourceById(id);
            newSourceItem->setText(COLUMN_0, QString::number(id));//column 0 is ID
            newSourceItem->setText(COLUMN_1, sourceTypeToQString(source->getSourceType())); //column 1 is sourcetype
		}
	}
}

QString PDTSourceTreeWidget::sourceTypeToQString(PDTVtkSouceType type) {
    switch(type) {
        case PDTVtkSouceType::LINE :
            return LINE_STR;
        case PDTVtkSouceType::POINT:
            return POINT_STR;
        case PDTVtkSouceType::FIBER:
            return Fiber_STR;
		default:
			return QString("");
    }
}

void PDTSourceTreeWidget::clearSelection() {
    this->selectionModel()->clearSelection();
    m_sourceList->clearAllHighlights();

    emit(renderWindow());
}

void PDTSourceTreeWidget::prepareMenu(const QPoint & pos) {
	const QTreeWidgetItem* item = itemAt(pos);
	if(item != nullptr) {
        switch (item->type()) {
            case SOURCE_ITEM_TYPE://source placement
                prepareSourceMenu(item, viewport()->mapToGlobal(pos));
				break;
            case PLANE_ITEM_TYPE://plane placement
                preparePlanePlacementMenu(item, viewport()->mapToGlobal(pos));
		}
	}
}

//source menu
void PDTSourceTreeWidget::prepareSourceMenu(const QTreeWidgetItem * item, const QPoint & pos) {
    QMenu menu;
    QAction remove("Delete", &menu), modify("Modify", &menu);

    int sourceId = getId(item);
    connect(&modify, &QAction::triggered, [=](){ emit modifySourceSignal(getSourceType(item), sourceId); });
    connect(&remove, &QAction::triggered, [=](){ emit removeSource(sourceId); });

    menu.addAction(&modify);//modify
    menu.addAction(&remove);//delete
    menu.exec(pos);
}

//
void PDTSourceTreeWidget::preparePlanePlacementMenu(const QTreeWidgetItem * item, const QPoint & pos) {
    QMenu menu;
    QMenu* subSourcesMenu = menu.addMenu("Add Source");

    int pid = getId(item);

    QAction remove("Delete", &menu), modify("Modify", &menu);
    QAction createLineSource("Line", subSourcesMenu);

    connect(&remove, &QAction::triggered, [=](){ emit removePlane(pid); });
    connect(&modify, &QAction::triggered, [=](){ emit modifyPlaneSignal(pid); });
    connect(&createLineSource, &QAction::triggered, [=](){ emit createSourceSignal(LINE, pid); });

    subSourcesMenu->addAction(&createLineSource);
    menu.addAction(&modify);
    menu.addAction(&remove);

    menu.exec(pos);
}

void PDTSourceTreeWidget::removeSource(int sid) {
    m_sourceList->removeSourceById(sid);
    update();
    emit(renderWindow());
}

void PDTSourceTreeWidget::removePlane(int pid) {
    m_sourceList->removeSourcesByPid(pid);
    update();
    emit(renderWindow());
}

int PDTSourceTreeWidget::getId(const QTreeWidgetItem* item) {
    if (item->text(COLUMN_0) == FREE_SOURCE_STR){
        return FREE_PID;
    } else {
        return item->text(COLUMN_0).toInt();
    }
}

PDTVtkSouceType PDTSourceTreeWidget::getSourceType(const QTreeWidgetItem * item) {
    if (item->text(COLUMN_1) == LINE_STR) {
        return LINE;
    }else if (item->text(COLUMN_1) == POINT_STR){
        return POINT;
    }else if (item->text(COLUMN_1) == Fiber_STR){
        return FIBER;
    }
}

void PDTSourceTreeWidget::onItemClicked(QTreeWidgetItem* item, int col){
    (void* ) item;
    (void) col;

    // Clear all selection first
    m_sourceList->clearAllHighlights();

    // Hightlight the selected items
    QList<QTreeWidgetItem*> items = selectedItems();
    for (QTreeWidgetItem* item : items){
        int id = getId(item);
        QString info;
        if (item->type() == SOURCE_ITEM_TYPE) {
            m_sourceList->setSourceHighlightedById(id, true);
            info = QString::fromStdString(m_sourceList->getSourceInfoById(id));
        } else if (item->type() == PLANE_ITEM_TYPE) {
            m_sourceList->setPlanePlacementHighlightedById(id, true);
            info = QString::fromStdString(m_sourceList->getPlanePlacementInfoById(id));
        }
        emit (sendStatusBarMessage(info));
    }

    emit(renderWindow());
}

void PDTSourceTreeWidget::mousePressEvent(QMouseEvent *event) {
    QTreeWidget::mousePressEvent(event);
    QTreeWidgetItem *item = itemAt(event->pos());

    if (item == nullptr) {
        clearSelection();
    }
}
