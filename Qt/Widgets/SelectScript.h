#ifndef SELECTSCRIPT_H
#define SELECTSCRIPT_H

#include <QWidget>

namespace Ui {
class SelectScript;
}

class SelectScript : public QWidget
{
    Q_OBJECT

public:
    explicit SelectScript(QWidget *parent = nullptr);
    ~SelectScript();

private:
    Ui::SelectScript *ui;
};

#endif // SELECTSCRIPT_H
