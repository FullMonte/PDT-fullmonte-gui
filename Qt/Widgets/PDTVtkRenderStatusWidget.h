#ifndef PDTVTKRENDERSTATUSWIDGET_H
#define PDTVTKRENDERSTATUSWIDGET_H

#include <QWidget>

#include <vtkActor.h>

#include "VTK/Core/pVTKRenderStatus.h"

#include <QLineEdit>

#include "PDTWorkPageWidget.h"

#include <QVTKWidget.h>

#include <QLabel>

#include <Tcl/Tcl.h>

namespace Ui {
class PDTVtkRenderStatusWidget;
}

class PDTVtkRenderStatusWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PDTVtkRenderStatusWidget(QWidget *parent = 0);
    ~PDTVtkRenderStatusWidget();

    void setRenderStatus(pVTKRenderStatus*);

    void setVtkActor(vtkActor*);

    void update();

    void setMeshArrayNames(std::vector<const char*> arrayNames);

    void setPDTWorkPageWidget(PDTWorkPageWidget*);

    void setRenderWindow(vtkRenderWindow* Window);

    void setRenderer(vtkRenderer* renderer);

    void setMaxID(int);

    void passLine(double** array);
    void set_up_slider();
    void set_sliderLabel();
    void create_new_label();

private slots:
    void setVtkOpacity(double);
    void emitChangeArraySignal(int index);
    void getMax();
    void getMin();
    void get_fluence();



    void on_confirm_clicked();


    void getPoint();
    void getLine();



    void on_point_fluence_clicked();

    void on_line_fluence_clicked();

signals:
    void render();
    void changeMeshArraySignal(int);

private:
    Ui::PDTVtkRenderStatusWidget *ui;

    pVTKRenderStatus* renderStatus;

    PDTWorkPageWidget* workPageWidget;

    int max;
    int min;
    int maxID;

    double line[6];

    vector<QLabel*> Label;
    vector<QLabel*> Label2;

    int pointx;
    int pointy;
    int pointz;

    vtkActor* actor;
    vtkActor* actor1;
    vtkActor* actor2;
    vtkActor* actor3;
    vtkActor* actor4;

    void setupRenderMode();

    void updateGeometryType(GeometryType);
    void updateRenderMode(RenderMode);
    void updateOpacity();


};

#endif // PDTVTKRENDERSTATUSWIDGET_H
