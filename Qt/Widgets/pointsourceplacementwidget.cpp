#include "pointsourceplacementwidget.h"

#include "SourcePlacementWidgetEnum.h"
#include "ui_SourcePlacementWidget.h"

#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>

#include <vtkPointWidget.h>

#include "LightSource/pdtvtkpointsource.h"
#include "VTK/Widgets/pointwidgetcallback.h"

PointSourcePlacementWidget::PointSourcePlacementWidget(vtkPointWidget* pointWidget, SourceList* sourceList, PDTWorkPageWidget* workPageWidget, QWidget *parent) :
    SourcePlacementWidget(sourceList, workPageWidget, parent)

{
    workpage = workPageWidget;
    this->vtkSourceWidget=pointWidget;

    QHBoxLayout * pointLayout = new QHBoxLayout;
    QHBoxLayout * powerLayout = new QHBoxLayout;

    pointLayout->setSpacing(5);
    powerLayout->setSpacing(5);

    powerEdit = new QLineEdit(this);
    powerEdit->setText(tr("0.00"));
    powerLayout->addWidget(powerEdit);

    for (int i=0;i<3;i++){
        pointEdit[i]= new QLineEdit(this);
        pointEdit[i]->setText(tr("0.00"));
        pointLayout->addWidget(pointEdit[i]);
        connect(pointEdit[i],&QLineEdit::editingFinished,this,&PointSourcePlacementWidget::moveWidget);
    }

    QLabel* pointLabel = new QLabel(tr("Point Position\n(X,Y,Z)"),this);
    QLabel* powerLabel = new QLabel(tr("power"));

    ui->gridLayout->removeWidget(ui->placeButton);
    ui->gridLayout->removeWidget(ui->cancelButton);
    ui->gridLayout->addWidget(pointLabel,0,2,1,1);
    ui->gridLayout->addItem(pointLayout,1,0,1,5);
    ui->gridLayout->addWidget(powerLabel,3,2,1,1);
    ui->gridLayout->addItem(powerLayout,4,1,1,2);
    ui->gridLayout->addWidget(ui->placeButton,6,1);
    ui->gridLayout->addWidget(ui->cancelButton,6,3);

    ui->informationButton->setVisible(false); //make information invisible for any light source widget except fiber source

}



PointSourcePlacementWidget::~PointSourcePlacementWidget(){

}

void PointSourcePlacementWidget::createSource(){
    double point[3];
    double power;
    getPoint(point);
    power = getPower();
    PDTVtkPointSource* newPointSource = new PDTVtkPointSource(point,power);
    newPointSource->SetNumPointInSource(1);


    assert(sourceList != nullptr);

    sourceList->addSource(newPointSource,0);
    //std::cout<<"this part is ok"<<std::endl;

}


void PointSourcePlacementWidget::modifySource(){
    double point[3];
    double power;
    this->getPoint(point);
    power = getPower();

    PDTVtkPointSource* pointSource=(PDTVtkPointSource *)sourceList->getSourceById(curSid);
    pointSource->setEndPoints(point);
    pointSource->SetPower(power);
    pointSource->update();

    assert(sourceList != nullptr);
    sourceList->showSource(curSid);

}


void PointSourcePlacementWidget::moveWidget(){
    vtkPointWidget * pointWidget = (vtkPointWidget*) this->vtkSourceWidget;
    double point[3];
    getPoint(point);
    pointWidget->SetPosition(point);

    emit(renderWindow());
}


void PointSourcePlacementWidget::setupWidget(){

    vtkPointWidget* pointWidget =(vtkPointWidget*) this->vtkSourceWidget;
    pointWidget->AllOff();
    pointWidget->SetTranslationMode(1);
    vtkSmartPointer<FreePointWidgetCallBack> lcall= vtkSmartPointer<FreePointWidgetCallBack>::New();
    lcall->setPointEdit(pointEdit);
    pointWidget->RemoveAllObservers();
    pointWidget->AddObserver(vtkCommand::EndInteractionEvent,lcall);


    if(curAction==MODIFY){
        assert(sourceList!=nullptr);
        PDTVtkPointSource* pointSource = (PDTVtkPointSource *)sourceList->getSourceById(curSid);
        double pointPosition[3];
        pointSource->getEndpoints(pointPosition);
        pointWidget->SetPosition(pointPosition);

        double* points;
        points=pointWidget->GetPosition();
        setPointEdit(pointPosition);



    }

    pointWidget->On();
    double* points;
    points=pointWidget->GetPosition();

    //pointWidget->On();
    //pointWidget->PlaceWidget();
    //pointWidget->SetInteractor(workpage->getrenderWindow());

}


double PointSourcePlacementWidget::getPower(){
    double power = powerEdit->text().toDouble();
    return power;
}


void PointSourcePlacementWidget::getPoint(double point[3]){
    for(int i = 0 ; i < 3; i++){
        point[i] = pointEdit[i]->text().toDouble();
    }
}


void PointSourcePlacementWidget::showCurrentSource(){
    assert(sourceList != nullptr);
    sourceList->showSource(curSid);
}


void PointSourcePlacementWidget::setPointEdit(double point[3]){

    for(int i=0;i<3;i++){
        this->pointEdit[i]->setText(QString::number(point[i]));
    }
}

bool PointSourcePlacementWidget::closeWidget() {
    return 1;
}
