#ifndef PDTSOURCETREEWIDGET_H
#define PDTSOURCETREEWIDGET_H

#include <QTreeWidget>
#include <QTreeWidgetItem>

#include "LightSource/SourceList.h"


class PDTSourceTreeWidget : public QTreeWidget {
	Q_OBJECT

public:
	PDTSourceTreeWidget(QWidget* parent) ;
	~ PDTSourceTreeWidget();


	enum {
        PLANE_ITEM_TYPE = QTreeWidgetItem::UserType,
        SOURCE_ITEM_TYPE = QTreeWidgetItem::UserType + 1
	};

	void setSourceList (SourceList* sourceList) { this->m_sourceList = sourceList; }

public slots:
    // Behaviour
    void onItemClicked(QTreeWidgetItem*, int);

    // Update the tree table content to match the source list
	void update();

	// Clear all the selections
	void clearSelection();

    // Remove the plane by pid
    void removePlane(int);

    // Remove the source by source id
    void removeSource(int);

signals:
    void createSourceSignal(PDTVtkSouceType type, int pid);

	// Modify a plane and all its sources by pid
	void modifyPlaneSignal(int);

	// Modify a source by source id
    void modifySourceSignal(PDTVtkSouceType type, int);

    void renderWindow();

    // Send message to status bar
    void sendStatusBarMessage(const QString);

protected:
    virtual void mousePressEvent(QMouseEvent *event) override;

private slots:
	// Create sub-menu when user right click on item
	void prepareMenu(const QPoint & pos);
	void prepareSourceMenu(const QTreeWidgetItem * item, const QPoint & pos);
	void preparePlanePlacementMenu(const QTreeWidgetItem * item, const QPoint & pos);


private:
	SourceList* m_sourceList;
    QString sourceTypeToQString(PDTVtkSouceType type);

    int getId(const QTreeWidgetItem*);
    PDTVtkSouceType getSourceType(const QTreeWidgetItem*);
};



#endif 
