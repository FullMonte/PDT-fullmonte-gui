#ifndef SCRIPTSELECTOR_H
#define SCRIPTSELECTOR_H

#include <QWidget>

enum simtype{ single,multiple,none};

namespace Ui {
class scriptselector;
}

class scriptselector : public QWidget
{
    Q_OBJECT



public:
    explicit scriptselector(QWidget *parent = nullptr);
    void setupconnections();
    ~scriptselector();

private:
    Ui::scriptselector *ui;

private slots:
        void SelectSimulation();
        void CancelSelection();


signals:
        void fullmonte_signal(simtype);


};




#endif // SCRIPTSELECTOR_H
