#include "PlanePlacementChooseDialog.h"
#include "ui_PlanePlacementChooseDialog.h"
#include <QComboBox>

const QString FREE_SOURCE_STR = QString("Free Source");
extern int PlanePlacementChooseDialog::curPid;


PlanePlacementChooseDialog::PlanePlacementChooseDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlanePlacementChooseDialog)
{
    ui->setupUi(this);
    connect(this, &PlanePlacementChooseDialog::accepted, this, &PlanePlacementChooseDialog::acceptPlanePlacement);
    connect(this, &PlanePlacementChooseDialog::rejected, this, &PlanePlacementChooseDialog::rejectPlanePlacement);
}

PlanePlacementChooseDialog::~PlanePlacementChooseDialog()
{
    delete ui;
}

void PlanePlacementChooseDialog::acceptPlanePlacement() {
    QString currentText = this->ui->planeComboBox->currentText();
    int pid;
    if (currentText == FREE_SOURCE_STR) {
        pid = FREE_PID;
    } else {
        pid = currentText.toInt();
    }
    curPid = pid;
}

void PlanePlacementChooseDialog::rejectPlanePlacement() {
    curPid = CANCEL;
}

int PlanePlacementChooseDialog::choosePlanePlacement(SourceList& sourceList, QWidget *parent){
    PlanePlacementChooseDialog dialog(parent);
    dialog.ui->planeComboBox->setInsertPolicy(QComboBox::InsertAtBottom);
    dialog.ui->planeComboBox->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);
	
	// Add plane ID
    dialog.ui->planeComboBox->addItem(FREE_SOURCE_STR);
	for(auto iter = sourceList.getPlanePlacementMap().begin(); iter != sourceList.getPlanePlacementMap().end(); iter ++){
        if (iter->first != FREE_PID){
            dialog.ui->planeComboBox->addItem(QString::number(iter->first));
		}
	}

    dialog.exec();

    return curPid;
}
