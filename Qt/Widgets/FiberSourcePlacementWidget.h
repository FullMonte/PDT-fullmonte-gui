#ifndef FIBERSOURCEPLACEMENTWIDGET
#define FIBERSOURCEPLACEMENTWIDGET

#include <vector>

#include <QLineEdit>
#include <QWidget>
#include <QPushButton>
#include <QLabel>

//#include <vtkLineWidget.h>

#include "LightSource/SourceList.h"
#include "SourcePlacementWidget.h"

using namespace std;

class FiberSourcePlacementWidget : public SourcePlacementWidget
{
public:
    FiberSourcePlacementWidget(SourceList* sourceList, PDTWorkPageWidget* workPageWidget, QWidget *parent = 0);
    ~FiberSourcePlacementWidget();
    void setupWidget() override;

protected:
    void createSource() override;
    void modifySource() override;
    void showCurrentSource() override;
    bool closeWidget() override;

private slots:
    void moveWidget() override;

private:
    //change to cone parameter
    QLineEdit *centerLineEdits[3];
    QLineEdit *directionLineEdits[3];
    QLineEdit * radiusEdits;
    QLineEdit * NAEdits;
    QLineEdit * powerEdits;

    vtkSmartPointer<vtkRenderWindow> renderWindow;


    //to be modified
    void getCenter(double point1[3]);
    void getDirection(double point2[3]);
    double getRadius();
    double getNA();
    double getPower();

//    void setLineEdits(double [3], double[3]);//set direction and position
//    void setRadiusEdits(double);
//    void setNAEdits(double);


};

#endif // LINESOURCEPLACEMENTWIDGET_H

