#ifndef PDTVTKTOOLWIDGET_H
#define PDTVTKTOOLWIDGET_H

#include <memory>

#include <vtkActor.h>

#include <QWidget>

#include "VTK/Core/pVTKReader.h"
#include "VTK/Tools/pVTKTool.h"

#include "PDTWorkPageWidget.h"
#include "PDTVtkToolWidgetEnum.h"

using namespace std;

namespace Ui {
class PDTVtkToolWidget;
}

class PDTVtkToolWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PDTVtkToolWidget(pVTKReader* reader, vtkActor* actor, PDTWorkPageWidget* workPage, QWidget* parent=nullptr);
    ~PDTVtkToolWidget();

    void setEnabled(bool);

private slots:
    void apply();
    void cancel();

protected:
    Ui::PDTVtkToolWidget *ui;

    VtkToolType type;

    unique_ptr<pVTKTool> vtkTool;
};

#endif // PDTVTKTOOLWIDGET_H
