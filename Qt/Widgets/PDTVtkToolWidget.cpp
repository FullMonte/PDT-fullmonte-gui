#include "PDTVtkToolWidget.h"
#include "ui_PDTVtkToolWidget.h"

#include <vtkDataSetMapper.h>

PDTVtkToolWidget::PDTVtkToolWidget(pVTKReader* reader, vtkActor* actor, PDTWorkPageWidget* workPageWidget, QWidget* parent) :
    QWidget(parent),
    ui(new Ui::PDTVtkToolWidget)
{
    assert(workPageWidget != nullptr);
    (void *)reader;
    (void *)actor;

    ui->setupUi(this);

    connect(ui->applyButton, &QPushButton::released, this, &PDTVtkToolWidget::apply);
    connect(ui->cancelButton, &QPushButton::released, this, &PDTVtkToolWidget::cancel);

    connect(ui->applyButton, &QPushButton::released,
            this, [=](){workPageWidget->enableVtkToolMode(false);});
    connect(ui->cancelButton, &QPushButton::released,
            this, [=](){workPageWidget->enableVtkToolMode(false);});
}

PDTVtkToolWidget::~PDTVtkToolWidget()
{
    delete ui;
}

void PDTVtkToolWidget::apply() {
    vtkTool->setEnabled(false);
}

void PDTVtkToolWidget::cancel() {
    vtkTool->setEnabled(false);

    vtkActor* actor = vtkTool->getActor();
    pVTKReader* reader = vtkTool->getInput();
    vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
    mapper->SetInputConnection(reader->GetOutputPort());
    actor->SetMapper(mapper);

}

void PDTVtkToolWidget::setEnabled(bool enabled) {
    assert(vtkTool.get() != nullptr);
    if (enabled) {
        this->vtkTool->update();
    }
    vtkTool->setEnabled(enabled);
}

