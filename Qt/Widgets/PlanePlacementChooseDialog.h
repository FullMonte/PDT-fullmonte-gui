#ifndef PLANEPLACEMENTCHOOSEDIALOG_H
#define PLANEPLACEMENTCHOOSEDIALOG_H

#include <QDialog>

#include "LightSource/SourceList.h"

namespace Ui {
class PlanePlacementChooseDialog;
}

class PlanePlacementChooseDialog : public QDialog
{
    Q_OBJECT

public:
    enum {
        CANCEL = -1
    };

    static int choosePlanePlacement(SourceList& sourceList, QWidget *parent = nullptr);

    explicit PlanePlacementChooseDialog(QWidget *parent = 0);
    ~PlanePlacementChooseDialog();

	static int curPid;

public slots:
    void acceptPlanePlacement();
    void rejectPlanePlacement();

private:
    Ui::PlanePlacementChooseDialog *ui;
};

#endif // PLANEPLACEMENTCHOOSEDIALOG_H
