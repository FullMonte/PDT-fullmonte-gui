#ifndef SOURCEPLACEMENTWIDGETENUM_H
#define SOURCEPLACEMENTWIDGETENUM_H

enum SourcePlacementType {
    PLANE_PLACEMENT,
    FREE_LINE,
    LINE_PLANE_PLACEMENT,
    FREE_POINT,
    CUT_END_FIBER
    //add cut-end fiber later
};

enum SourcePlacementAction {
    CREATE,
    MODIFY
};

#endif
