#include "PDTVtkRenderStatusWidget.h"
#include "ui_PDTVtkRenderStatusWidget.h"
#include "PDTWorkPageWidget.h"
#include <assert.h>
#include <vtkProperty.h>
#include <QString>
#include <QVTKWidget.h>
#include <QSlider>
#include <QDir>


PDTVtkRenderStatusWidget::PDTVtkRenderStatusWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PDTVtkRenderStatusWidget) {
    ui->setupUi(this);

    setupRenderMode();

    // Add connections
    connect(ui->opacityHorizontalSlider, &QSlider::valueChanged,
                [=](int value){ this->setVtkOpacity(value/100.0); } );

    connect(ui->dataArrayComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(emitChangeArraySignal(int)));

    // initialize point and line inputs
    ui->pointx->setText(tr("0.00"));
    ui->pointy->setText(tr("0.00"));
    ui->pointz->setText(tr("0.00"));

    ui->startx->setText(tr("0.00"));
    ui->starty->setText(tr("0.00"));
    ui->startz->setText(tr("0.00"));

    ui->endx->setText(tr("0.00"));
    ui->endy->setText(tr("0.00"));
    ui->endz->setText(tr("0.00"));

}

PDTVtkRenderStatusWidget::~PDTVtkRenderStatusWidget() {
    delete ui;
}

void PDTVtkRenderStatusWidget::setRenderStatus(pVTKRenderStatus* renderStatus) {
    assert(renderStatus != nullptr);
    this->renderStatus = renderStatus;
}

//set maximum material ID
void PDTVtkRenderStatusWidget::setMaxID(int max){
    maxID = max;
}

void PDTVtkRenderStatusWidget::setVtkActor(vtkActor* actor) {
    assert(actor != nullptr);
    this->actor = actor;
}

void PDTVtkRenderStatusWidget::update() {
    assert(actor != nullptr && renderStatus != nullptr);

    updateGeometryType(renderStatus->getGeometryType());
    updateRenderMode(renderStatus->getRenderMode());
}

void PDTVtkRenderStatusWidget::updateGeometryType(GeometryType type) {
    QString text;
    switch (type) {
        case UNSTRUCTURED_GRID :
            text = "Unstructured Grid";
            break;
        case STRUCTURED_GRID :
            text = "Structured Grid";
            break;
        case POLY_DATA :
            text = "Poly Data";
            break;
        default:
            text = "Unkown";
    }

    //ui->geometryLineEdit->setText(text);
}

void PDTVtkRenderStatusWidget::updateRenderMode(RenderMode mode) {
    //ui->renderModeComboBox->setCurrentIndex(mode);
}

void PDTVtkRenderStatusWidget::setupRenderMode() {
     //ui->renderModeComboBox->insertItem(RenderMode::GEOMETRIC_MODE, "Geometric Mode");
}

void PDTVtkRenderStatusWidget::updateOpacity() {
    double opacity = actor->GetProperty()->GetOpacity() * 100;
    ui->opacityHorizontalSlider->setValue((int)opacity);
}

void PDTVtkRenderStatusWidget::setVtkOpacity(double opacity) {
    actor->GetProperty()->SetOpacity(opacity);
    emit render();
}



//This function is invoked when inside setMeshArrayName
// dataArrayComboBox is cleared
//Have to check index is not -1
void PDTVtkRenderStatusWidget::emitChangeArraySignal(int index){

    if(index>=0){
        emit changeMeshArraySignal(index);

    }

}



void PDTVtkRenderStatusWidget::setMeshArrayNames(std::vector<const char*> arrayNames){

    ui->dataArrayComboBox->clear();

    for(unsigned i=0;i<arrayNames.size();i++){
        ui->dataArrayComboBox->addItem(arrayNames[i]);
    }

}

//get max material ID from max slider bar
void PDTVtkRenderStatusWidget::getMax(){
    max = ui->sliderMax->value();
}

//get min material ID from min slider bar
void PDTVtkRenderStatusWidget::getMin(){
    min = ui->sliderMin->value();
}


//the following will only be used when goint back to the checkbox version of filter out material
void PDTVtkRenderStatusWidget::setPDTWorkPageWidget(PDTWorkPageWidget* ptr){
    workPageWidget = ptr;
}

//void PDTVtkRenderStatusWidget::setRenderWindow(vtkRenderWindow* Window){
//    this->RenderWindow  = Window;
//}

//void PDTVtkRenderStatusWidget::setRenderer(vtkRenderer* renderer){
//    this->Renderer  = renderer;
//}




//void PDTVtkRenderStatusWidget::on_Region1_clicked(bool checked)
//{
//    if(checked){
//        cout << "this is checked" << endl;
//        this->Renderer->RemoveAllViewProps();
//        actor1 = workPageWidget->loadRegion(1,1)->GetDataSetActor();
//        this->Renderer->AddActor(actor1);

//        this->Renderer->ResetCamera();
//        //this->RenderWindow->Render();
//        Renderer->GetRenderWindow()->Render();
//        cout << " there are "<< Renderer->VisibleActorCount() << endl;
//    }
//    else{
//        this->Renderer->RemoveActor(actor1);
//        this->Renderer->ResetCamera();
//        Renderer->GetRenderWindow()->Render();
//        cout << "unchecked" << endl;
//        cout << " there are "<< Renderer->VisibleActorCount() << endl;
//    }
//}



//void PDTVtkRenderStatusWidget::on_Region2_clicked(bool checked)
//{
//    if(checked){
//        cout << "this is checked" << endl;
//        this->Renderer->RemoveAllViewProps();
//        actor2 = workPageWidget->loadRegion(2,2)->GetDataSetActor();

//        this->Renderer->AddActor(actor2);

//        this->Renderer->ResetCamera();

//        this->RenderWindow->Render();

//        cout << " there are "<< Renderer->VisibleActorCount() << endl;
//    }
//    else{
//        this->Renderer->RemoveActor(actor2);
//        this->Renderer->ResetCamera();
//        this->RenderWindow->Render();
//        cout << "unchecked" << endl;
//        cout << " there are "<< Renderer->VisibleActorCount() << endl;
//    }
//}

//void PDTVtkRenderStatusWidget::on_Region3_clicked(bool checked)
//{
//    if(checked){
//        cout << "this is checked" << endl;
//        this->Renderer->RemoveAllViewProps();
//        actor3 = workPageWidget->loadRegion(3,3)->GetDataSetActor();
//        this->Renderer->AddActor(actor3);
//        this->Renderer->ResetCamera();
//        this->RenderWindow->Render();
//        cout << " there are "<< Renderer->VisibleActorCount() << endl;
//    }
//    else{
//        this->Renderer->RemoveActor(actor3);
//        this->Renderer->ResetCamera();
//        this->RenderWindow->Render();
//        cout << "unchecked" << endl;
//        cout << " there are "<< Renderer->VisibleActorCount() << endl;
//    }
//}

//void PDTVtkRenderStatusWidget::on_Region4_clicked(bool checked)
//{
//    if(checked){
//        cout << "this is checked" << endl;
//        this->Renderer->RemoveAllViewProps();
//        actor4 = workPageWidget->loadRegion(4,4)->GetDataSetActor();
//        this->Renderer->AddActor(actor4);
//        this->Renderer->ResetCamera();
//        this->RenderWindow->Render();
////        this->Renderer->AddActor(actor1->GetDataSetActor());
////        this->Renderer->ResetCamera();
//        cout << " there are "<< Renderer->VisibleActorCount() << endl;
//    }
//    else{
//        this->Renderer->RemoveActor(actor4);
//        this->Renderer->ResetCamera();
//        this->RenderWindow->Render();
//        cout << "unchecked" << endl;
//        cout << " there are "<< Renderer->VisibleActorCount() << endl;
//    }
//}


//set up slider bar
void PDTVtkRenderStatusWidget::set_up_slider()
{

    ui->sliderMin->setMaximum(maxID+1);
    ui->sliderMin->setMinimum(0);
    ui->sliderMin->setSingleStep(1);
    ui->sliderMin->setTickInterval(1);
    ui->sliderMin->setTickPosition(QSlider::TicksBelow);
    cout << "max id is " << maxID <<endl;

    ui->sliderMax->setMaximum(maxID+1);
    ui->sliderMax->setMinimum(0);
    ui->sliderMax->setSingleStep(1);
    ui->sliderMax->setTickInterval(1);
    ui->sliderMax->setTickPosition(QSlider::TicksBelow);


    for(int i = 0;i<= maxID;i++){
        QString s = QString::number(i);
        Label.push_back(new QLabel(this));
        Label[i]->setObjectName(s);
        Label[i]->setText(s);
        ui->Hlayout->addWidget(Label[i]);
    }

    for(int i = 0;i<= maxID;i++){
        QString s = QString::number(i);
        Label2.push_back(new QLabel(this));
        Label2[i]->setObjectName(s);
        Label2[i]->setText(s);
        ui->Hlayout2->addWidget(Label2[i]);
    }

}

//when confirm is clicked, render selected materials
void PDTVtkRenderStatusWidget::on_confirm_clicked()
{
    cout << "enter" << endl;
    getMax();
    getMin();
    cout << "max is " << endl;
    cout << max << endl;
    cout << "min is " << endl;
    cout << min << endl;

    workPageWidget->loadRegion(max,min);
}


void PDTVtkRenderStatusWidget::getPoint(){
    line[0] = ui->pointx->text().toDouble();
    line[1] = ui->pointy->text().toDouble();
    line[2] = ui->pointz->text().toDouble();
    line[3] = ui->pointx->text().toDouble();
    line[4] = ui->pointy->text().toDouble();
    line[5] = ui->pointz->text().toDouble();
}

void PDTVtkRenderStatusWidget::getLine(){
    line[0] = ui->startx->text().toDouble();
    line[1] = ui->starty->text().toDouble();
    line[2] = ui->startz->text().toDouble();
    line[3] = ui->endx->text().toDouble();
    line[4] = ui->endy->text().toDouble();
    line[5] = ui->endz->text().toDouble();
}

//connect to get fluence function//written to _phi.tcl
void PDTVtkRenderStatusWidget::on_point_fluence_clicked()
{
    getPoint();

    //call a workpage function
    get_fluence();


}

//connect to get fluence funtion//written to _phi.tcl
void PDTVtkRenderStatusWidget::on_line_fluence_clicked()
{
    getLine();
    //call a workpage function
    get_fluence();
}

//pass in data of line (using by line_fluence function)
void PDTVtkRenderStatusWidget::passLine(double** array){
    *array  = line;
}

//set up slider bar label from opticalList
void PDTVtkRenderStatusWidget::set_sliderLabel(){

    std::vector<OpticalProperty*> opticalList = workPageWidget->getOpticalPropertyList();
    cout << "size of list is " << opticalList.size() <<endl;
    for(int i = 0; i < opticalList.size();i++){
        Label[i]->setText(opticalList[i]->name);
        Label2[i]->setText(opticalList[i]->name);
    }

}

//create a new label
void PDTVtkRenderStatusWidget::create_new_label(){
    Label.push_back(new QLabel(this));
    Label2.push_back(new QLabel(this));
}

//function that write to _phi.tcl
void PDTVtkRenderStatusWidget::get_fluence(){
    //get point/line fluence
    Tcl my_tcl(workPageWidget);
    my_tcl.set_proj_dir();
    //determine if FM output avaliable
    QDir::setCurrent(my_tcl.getProjDir());//set current dir to proj dir
    bool exist = QDir("FM_output").exists();
    cout << exist << endl;
    my_tcl.setRun_FM(exist);

    //set up fluence.tcl file
    my_tcl.set_fluence_tcl_file();
    //save projet and store data
    my_tcl.setup_file(true);

}


