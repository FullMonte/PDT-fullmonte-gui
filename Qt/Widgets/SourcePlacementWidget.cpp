#include "SourcePlacementWidget.h"
#include "ui_SourcePlacementWidget.h"
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QMessageBox>
#include "LightSource/pdtvtkpointsource.h"
#include "VTK/Widgets/pointwidgetcallback.h"



SourcePlacementWidget::SourcePlacementWidget(SourceList* sourceList, PDTWorkPageWidget* workPageWidget, QWidget *parent) :
    sourceList(sourceList),
    QWidget(parent),
    ui(new Ui::SourcePlacementWidget) {
	assert(sourceList != nullptr);
    ui->setupUi(this);

    // Connections
    connect(ui->placeButton, SIGNAL(released()), this, SLOT(placeSource()));
    connect(ui->cancelButton, SIGNAL(released()), this, SLOT(cancelPlacement()));
    connect(ui->informationButton,SIGNAL(released()),this,SLOT(information()));

    connect(this, &SourcePlacementWidget::place,
            this, [=](){workPageWidget->enablePlacementMode(false);});
    connect(this, &SourcePlacementWidget::cancel,
            this, [=](){workPageWidget->enablePlacementMode(false);});
    connect(this, &SourcePlacementWidget::renderWindow,
            workPageWidget, &PDTWorkPageWidget::render);


}

SourcePlacementWidget::~SourcePlacementWidget(){
    delete ui;
}


void SourcePlacementWidget::information(){
    int ret = QMessageBox::information(this,
                                       tr("Numerical Aperture"),
                                       tr("It refers to sin(theta),\n"
                                          "which theta is the angle between central axis and outer shell."),
                                       QMessageBox::Ok);

}

void SourcePlacementWidget::placeSource() {
    if(curAction == CREATE) {
        createSource();
    } else if (curAction == MODIFY){
        modifySource();
    }
    if(closeWidget())
        vtkSourceWidget->Off();
    else
        cout << "it is fiber source, no widget" << endl;
    emit place();
}

void SourcePlacementWidget::cancelPlacement() {
    if (curAction == MODIFY) {
        showCurrentSource();
    }

    if(closeWidget())
        vtkSourceWidget->Off();
    else
        cout << "it is fiber source, no widget" << endl;

    emit cancel();
}




