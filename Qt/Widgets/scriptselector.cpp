#include "scriptselector.h"
#include "ui_scriptselector.h"

scriptselector::scriptselector(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::scriptselector)
{
    ui->setupUi(this);
    setupconnections();

}

void scriptselector::setupconnections(){
     connect(ui->OkButton,SIGNAL(clicked()),this,SLOT(SelectSimulation()));
     connect(ui->CancelButton,SIGNAL(clicked()),this,SLOT(CancelSelection()));

}



scriptselector::~scriptselector()
{
    delete ui;
}



void scriptselector::SelectSimulation(){

    simtype simultype = none;

    if(ui->SingleRadio->isChecked()){
        simultype=single;

    }

    else if(ui->MultipleRadio->isChecked())
    {
        simultype = multiple;
    }

    if(simultype!=none){


        scriptselector::close();
        emit fullmonte_signal(simultype);

    }





}

void scriptselector::CancelSelection(){

    scriptselector::close();


}

