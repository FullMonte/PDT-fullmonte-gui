#ifndef OPTICALPROPERTYWIDGET_H
#define OPTICALPROPERTYWIDGET_H

#include <QWidget>
#include "OpticalProperty/OpticalProperty.h"
#include "PDTWorkPageWidget.h"


namespace Ui {
class OpticalPropertyWidget;
}

class OpticalPropertyWidget : public QWidget
{
    Q_OBJECT
public:
    explicit OpticalPropertyWidget(QWidget *parent = nullptr);
    void clear_name();
    void setMaxID(int);
    int getMaxID();

signals:
    void new_optical_property(OpticalProperty* newOpticalProperty);
    void cancel_optical_property();

public slots:

private slots:
    void on_pushButton_clicked();



    void on_cancel_button_clicked();

private:
 Ui::OpticalPropertyWidget* ui;
 int maxID;
};

#endif // OPTICALPROPERTYWIDGET_H
