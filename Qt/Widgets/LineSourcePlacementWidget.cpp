#include "LineSourcePlacementWidget.h"
#include "ui_SourcePlacementWidget.h"

#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>

#include "LightSource/PDTVtkLineSource.h"
#include "VTK/Widgets/lineWidgetCallBack.h"

LineSourcePlacementWidget::LineSourcePlacementWidget(vtkLineWidget* lineWidget, SourceList* sourceList, PDTWorkPageWidget* workPageWidget, QWidget *parent) :
    SourcePlacementWidget(sourceList, workPageWidget, parent)
{
    this->vtkSourceWidget = lineWidget;

    QHBoxLayout * point1Layout = new QHBoxLayout;
    QHBoxLayout * point2Layout = new QHBoxLayout;
    QHBoxLayout * powerLayout = new QHBoxLayout;

    point1Layout->setSpacing(5);
    point2Layout->setSpacing(5);
    powerLayout->setSpacing(5);



    powerEdits = new QLineEdit(this);
    powerLayout->addWidget(powerEdits);

    powerEdits->setText(tr("0.00"));


    // Initialize line endpoints line edit
    for (int i=0; i < 3; i++) {
        point1LineEdits[i] = new QLineEdit(this);
        point1LineEdits[i]->setText(tr("0.00"));
        point1Layout->addWidget(point1LineEdits[i]);
        connect(point1LineEdits[i], &QLineEdit::editingFinished,
                this, &LineSourcePlacementWidget::moveWidget);

        point2LineEdits[i] = new QLineEdit(this);
        point2LineEdits[i]->setText(tr("0.00"));
        point2Layout->addWidget(point2LineEdits[i]);
        connect(point2LineEdits[i], &QLineEdit::editingFinished,
                this, &LineSourcePlacementWidget::moveWidget);
    }


    QLabel* point1Label = new QLabel(tr("Endpoint1\n(X,Y,Z)"), this);
    QLabel* point2Label = new QLabel(tr("Endpoint2\n(X,Y,Z)"), this);
    QLabel* powerLabel = new QLabel(tr("power"),this);

    ui->gridLayout->removeWidget(ui->placeButton);
    ui->gridLayout->removeWidget(ui->cancelButton);
    ui->gridLayout->addWidget(point1Label, 0, 2, 1, 1);
    ui->gridLayout->addItem(point1Layout, 1, 0, 1, 5);
    ui->gridLayout->addWidget(point2Label, 2, 2, 1, 1);
    ui->gridLayout->addItem(point2Layout, 3, 0, 1, 5);
    ui->gridLayout->addWidget(powerLabel,4,2,1,1);
    ui->gridLayout->addItem(powerLayout,5,2,1,2);

    ui->gridLayout->addWidget(ui->placeButton,6, 1);
    ui->gridLayout->addWidget(ui->cancelButton, 6, 3);

    ui->informationButton->setVisible(false);

}

LineSourcePlacementWidget::~LineSourcePlacementWidget(){

}

void LineSourcePlacementWidget::createSource() {
    double point1 [3], point2[3];
    double power;
    getPoint1(point1);
    getPoint2(point2);
    power = getPower();
    PDTVtkLineSource* newLineSource = new PDTVtkLineSource(point1, point2,power);

    assert(sourceList != nullptr);
	sourceList->addSource(newLineSource);

}

void LineSourcePlacementWidget::modifySource() {
    double point1 [3], point2[3];
    double power;
    getPoint1(point1);
    getPoint2(point2);
    power = getPower();

    PDTVtkLineSource* lineSource = (PDTVtkLineSource *)sourceList->getSourceById(curSid);
    lineSource->setEndpoints(point1, point2);
    lineSource->SetPower(power);
    lineSource->update();

    assert(sourceList != nullptr);
    sourceList->showSource(curSid);
}

void LineSourcePlacementWidget::moveWidget() {
    vtkLineWidget* lineWidget = (vtkLineWidget*) this->vtkSourceWidget;
    double point1 [3], point2[3];
    getPoint1(point1);
    getPoint2(point2);
    lineWidget->SetPoint1(point1);
    lineWidget->SetPoint2(point2);

    emit(renderWindow());
}

void LineSourcePlacementWidget::setupWidget() {
    vtkLineWidget* lineWidget = (vtkLineWidget*) this->vtkSourceWidget;
    vtkSmartPointer<FreeLineWidgetCallBack> lcall = vtkSmartPointer<FreeLineWidgetCallBack>::New();
    lcall->setLineEdits(point1LineEdits, point2LineEdits);
    lineWidget->RemoveAllObservers();
    lineWidget->AddObserver(vtkCommand::EndInteractionEvent, lcall);

    // Move widget to the current source position in modification mode
    if(curAction == MODIFY) {
        assert(sourceList != nullptr);
        PDTVtkLineSource* lineSource = (PDTVtkLineSource *)sourceList->getSourceById(curSid);
        double endpoint1[3], endpoint2[3];
        lineSource->getEndpoints(endpoint1, endpoint2);
        lineWidget->SetPoint1(endpoint1);
        lineWidget->SetPoint2(endpoint2);
        setLineEdits(endpoint1, endpoint2);
    }

    //lineWidget->On();
    lineWidget->SetEnabled(1);
    double* point1=lineWidget->GetPoint1();

}

double LineSourcePlacementWidget::getPower(){
    double power = powerEdits->text().toDouble();
    return power;
}

void LineSourcePlacementWidget::getPoint1(double point1[3]){
    for(int i = 0 ; i < 3; i++){
        point1[i] = point1LineEdits[i]->text().toDouble();
    }
}

void LineSourcePlacementWidget::getPoint2(double point2[3]){
    for(int i = 0 ; i < 3; i++){
        point2[i] = point2LineEdits[i]->text().toDouble();
    }
}

void LineSourcePlacementWidget::showCurrentSource() {
    assert(sourceList != nullptr);
    sourceList->showSource(curSid);
}

void LineSourcePlacementWidget::setLineEdits (double point1[3], double point2[3]) {
    for(int i = 0; i < 3; i++){
        this->point1LineEdits[i]->setText(QString::number(point1[i]));
        this->point2LineEdits[i]->setText(QString::number(point2[i]));
    }
}

bool LineSourcePlacementWidget::closeWidget() {
    return 1;
}

