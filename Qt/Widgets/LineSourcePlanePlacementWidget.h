#ifndef LINESOURCEPLANEPLACEMENTWIDGET_H
#define LINESOURCEPLANEPLACEMENTWIDGET_H

#include <vector>

#include <QLineEdit>
#include <QWidget>

#include <vtkLineWidget.h>

#include "LightSource/SourceList.h"
#include "SourcePlacementWidget.h"

using namespace std;

class LineSourcePlanePlacementWidget : public SourcePlacementWidget
{
public:
    LineSourcePlanePlacementWidget(vtkLineWidget *lineWidget, SourceList* sourceList, PDTWorkPageWidget* workPageWidget, QWidget *parent = 0);
    ~LineSourcePlanePlacementWidget();
    void setupWidget() override;

protected:
    void createSource() override;
    void modifySource() override;
    void showCurrentSource() override;
    bool closeWidget() override;

private slots:
    void moveWidget() override;

private:
    QLineEdit *planePosLineEdits[2];
    QLineEdit *depthsLineEdits[2];

    void getPlanePos(double planePos[3]);
    void getDepths(double depths[3]);

    void getWorldPosition(double endPoint1[3], double endPoint2[3]);

    void setLineEdits(double[2], double[2]);
};

#endif // LINESOURCEPLANEPLACEMENTWIDGET_H

