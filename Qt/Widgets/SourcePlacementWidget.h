#ifndef SOURCEPLACEMENTWIDGET_H
#define SOURCEPLACEMENTWIDGET_H

#include <QObject>
#include <QWidget>

#include <vtk3DWidget.h>
#include <vtkSmartPointer.h>

#include "LightSource/SourceList.h"
#include "PDTWorkPageWidget.h"

namespace Ui {
class SourcePlacementWidget;
}

class SourcePlacementWidget : public QWidget
{
    Q_OBJECT

public:
    SourcePlacementWidget(SourceList* sourceList, PDTWorkPageWidget* workPageWidget, QWidget *parent = nullptr);

    ~SourcePlacementWidget();

	// Set current action for placement widget, such as CREATE and MODIFY
    void setCurrentAction(SourcePlacementAction action) { this->curAction = action; }

	// Set current source List
	void setSourceList(SourceList *sourceList) { this->sourceList = sourceList; }

    // Set Plane placement which the source may add to
    void setCurrentPid(int pid) { this->curPid = pid; }

    // Set Plane placement which the source may add to
    void setCurrentSid(int sid) { this->curSid = sid; }

    // Can call after set current pid
    virtual void setupWidget() = 0;

signals:
    void place();
    void cancel();
    void renderWindow();

protected:
    vtk3DWidget *vtkSourceWidget;

    Ui::SourcePlacementWidget *ui;

    SourceList *sourceList;

	SourcePlacementAction curAction = CREATE;

    int curPid = FREE_PID;
    int curSid = -1;

    virtual void createSource() = 0;
    virtual void modifySource() = 0;
    virtual void showCurrentSource() = 0;
    virtual bool closeWidget() = 0;

private slots:
    void placeSource();
    void cancelPlacement();
    void information();
    virtual void moveWidget() = 0;

};

#endif // SOURCEPLACEMENTWIDGET_H
