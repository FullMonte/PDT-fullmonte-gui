#ifndef LINESOURCEPLACEMENTWIDGET_H
#define LINESOURCEPLACEMENTWIDGET_H

#include <vector>

#include <QLineEdit>
#include <QWidget>

#include <vtkLineWidget.h>

#include "LightSource/SourceList.h"
#include "SourcePlacementWidget.h"

using namespace std;

class LineSourcePlacementWidget : public SourcePlacementWidget
{
public:
    LineSourcePlacementWidget(vtkLineWidget *lineWidget, SourceList* sourceList, PDTWorkPageWidget* workPageWidget, QWidget *parent = 0);
    ~LineSourcePlacementWidget();
    void setupWidget() override;

protected:
    void createSource() override;
    void modifySource() override;
    void showCurrentSource() override;
    bool closeWidget() override;

private slots:
    void moveWidget() override;

private:
    QLineEdit *point1LineEdits[3];
    QLineEdit *point2LineEdits[3];
    QLineEdit *powerEdits;

    void getPoint1(double point1[3]);
    void getPoint2(double point2[3]);
    double getPower();

    void setLineEdits(double [3], double[3]);

};

#endif // LINESOURCEPLACEMENTWIDGET_H
