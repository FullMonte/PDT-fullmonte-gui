#include "PDTWorkPageWidget.h"
#include "ui_PDTWorkPageWidget.h"
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkRendererCollection.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkSmartPointer.h>
#include <vtkLineWidget.h>
#include <vtkPolyDataMapper.h>
#include <vtkPointPicker.h>
#include <vtkAxesActor.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkTransform.h>
#include <QFileDialog>
#include <QMessageBox>
#include <QMenu>
#include<QAction>
#include "LightSource/IO/SourceListWriter.h"
#include "LightSource/IO/SourceListReader.h"
#include "../Utils/MessageBoxUtils.h"
#include "LineSourcePlacementWidget.h"
#include "LineSourcePlanePlacementWidget.h"
#include "PlanePlacementWidget.h"
#include "PlanePlacementChooseDialog.h"
#include "pointsourceplacementwidget.h"
#include "FiberSourcePlacementWidget.h"
#include "PDTVtkToolWidget.h"
#include "PDTVtkDataSetClipperWidget.h"
#include <vtkScalarBarActor.h>
#include <set>

/*Once the mesh is loaded, all the user's interactions are facilitated
 * through the functions in this file. Of course this file uses many
 * other classes but a developer should always strart tracing the sequence
 * of events from this page*/

/* The way to trace these functions is to start from GUI. Identify the button
 * or action that you are dealing with. Through the use of connections (signal & slot)
 * realize which function would be invoked*/




/* A series of initialization functions are called
    It is unlikely that you will have to tweek the
    setup functions */
PDTWorkPageWidget::PDTWorkPageWidget(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PDTWorkPageWidget) {
    ui->setupUi(this);
    setupVtk();
    setupSourceList();
    setupPlacementWidget();
    setupSourceTreeWidget();//source widget
    setupRenderStatusWidget();//render status widget
    setupVtkToolWidget();
    setupOpticalPropertyWidget();//property widget

}

PDTWorkPageWidget::~PDTWorkPageWidget() {
    delete ui;
    delete sourceList;
}


//render window
void PDTWorkPageWidget::setupVtk() {
    // Add renderer to render window
    renderer = vtkSmartPointer<vtkRenderer>::New(); //initialize vtk render
    renderWindow = ui->qvtkWidget->GetRenderWindow();//initialize Render main window

    ui->qvtkWidget->GetRenderWindow()->AddRenderer(renderer);
    ui->qvtkWidget->GetRenderWindow()->Render();

    // Connect right click and get postion function
    vtkQtConnect->Connect(renderWindow->GetInteractor(),
                          vtkCommand::RightButtonPressEvent,
                          this,
                          SLOT(getPosition()));

    // Setup vtk widgets
    lineWidget->SetCurrentRenderer(renderer);
    lineWidget->SetInteractor(renderWindow->GetInteractor());
    planeWidget->SetCurrentRenderer(renderer);
    planeWidget->SetInteractor(renderWindow->GetInteractor());
    pointWidget->SetCurrentRenderer(renderer);
    pointWidget->SetInteractor(renderWindow->GetInteractor());
    coneWidget->SetCurrentRenderer(renderer);
    coneWidget->SetInteractor(renderWindow->GetInteractor());

}

void PDTWorkPageWidget::setupSourceList() {
    sourceList = new SourceList(this->renderer);
}


void PDTWorkPageWidget::setupPlacementWidget() {
    // Clear the default tabs in the stack widget
    while (ui->sourcePlacementWidgetStack->count() != 0){
        QWidget *w = ui->sourcePlacementWidgetStack->currentWidget();
        ui->sourcePlacementWidgetStack->removeWidget(w);
        delete w;
    }

    // Instantiate free line Source placement widget
    LineSourcePlacementWidget *lineSourcePlacementWidget =
            new LineSourcePlacementWidget(lineWidget.GetPointer(), this->sourceList,
                                          this, ui->sourcePlacementWidgetStack);

    // Instantiate plane placement widget
    PlanePlacementWidget *planePlacementWidget =
            new PlanePlacementWidget(planeWidget.GetPointer(), this->sourceList,
                                     this, ui->sourcePlacementWidgetStack);

    // Instantiate line plane placement widget
    LineSourcePlanePlacementWidget *lineSourcePlanePlacementWidget =
            new LineSourcePlanePlacementWidget(lineWidget.GetPointer(), this->sourceList,
                                          this, ui->sourcePlacementWidgetStack);
    // Instantiate point plane placement widget
    PointSourcePlacementWidget *pointSourcePlanePlacementWidget =
            new PointSourcePlacementWidget(pointWidget.GetPointer(), this->sourceList,
                                          this, ui->sourcePlacementWidgetStack);
    FiberSourcePlacementWidget *fiberSourcePlacementWidget =
            new FiberSourcePlacementWidget(this->sourceList,this,ui->sourcePlacementWidgetStack);

    //fiber placement widget---new
    //FiberSourcePlacementWidget *fiberSourcePlacementWidget =
           // new FiberSourcePlacementWidget(NEED CONE WIDGET HERE)

    // ATTENTION : The order of the insertion has to be the same as the enum value !!!!!!!
    ui->sourcePlacementWidgetStack->insertWidget(PLANE_PLACEMENT, planePlacementWidget);
    ui->sourcePlacementWidgetStack->insertWidget(FREE_LINE, lineSourcePlacementWidget);
    ui->sourcePlacementWidgetStack->insertWidget(LINE_PLANE_PLACEMENT, lineSourcePlanePlacementWidget);
    ui->sourcePlacementWidgetStack->insertWidget(FREE_POINT, pointSourcePlanePlacementWidget);
    ui->sourcePlacementWidgetStack->insertWidget(CUT_END_FIBER,fiberSourcePlacementWidget);
    //another insertion for VTKConewidget

    // Initially make the dock close
    ui->sourcePlacementDock->close();
}

void PDTWorkPageWidget::setupVtkToolWidget() {
    // Clear the default tabs in the stack widget
    while (ui->PVtkToolStackedWidget->count() != 0){
        QWidget *w = ui->PVtkToolStackedWidget->currentWidget();
        ui->PVtkToolStackedWidget->removeWidget(w);
        delete w;
    }

    PDTVtkDataSetClipperWidget* dataSetClipperWidget =
            new PDTVtkDataSetClipperWidget(&m_reader, m_actor.GetDataSetActor(), renderer.Get(),
                                                this, ui->PVtkToolStackedWidget);

    ui->PVtkToolStackedWidget->insertWidget(DATASET_CLIPPER, dataSetClipperWidget);

    ui->PVtkToolDockWidget->close();
}

void PDTWorkPageWidget::setupSourceTreeWidget() {
    ui->sourceTable->setSourceList(sourceList);

    connect(ui->sourceTable, &PDTSourceTreeWidget::createSourceSignal,
            this, &PDTWorkPageWidget::createSource);
    connect(ui->sourceTable, &PDTSourceTreeWidget::modifySourceSignal,
            this, &PDTWorkPageWidget::modifySource);
    connect(ui->sourceTable, &PDTSourceTreeWidget::modifyPlaneSignal,
            this, &PDTWorkPageWidget::modifyPlanePlacement);
    connect(ui->sourceTable, &PDTSourceTreeWidget::renderWindow,
            this, &PDTWorkPageWidget::render);
    connect(ui->sourceTable, &PDTSourceTreeWidget::sendStatusBarMessage,
            [=](QString s){ emit sendStatusBarMessage(s); });
    ui->sourceTable->update();
}

void PDTWorkPageWidget::setupRenderStatusWidget() {
    ui->vtkStatusWidget->setPDTWorkPageWidget(this);
    //ui->vtkStatusWidget->setRenderer(renderer);
    //ui->vtkStatusWidget->setRenderWindow(ui->qvtkWidget->GetRenderWindow());
    ui->vtkStatusWidget->setRenderStatus(&m_render_status);
    //get value of array from vtkstatus widget
    double** array;
    //pass in a double pointer to get value
    ui->vtkStatusWidget->passLine(array);
    line = *array;
    ui->vtkStatusWidget->setVtkActor(m_actor.GetDataSetActor());
    ui->vtkStatusWidget->update();
    connect(ui->vtkStatusWidget, &PDTVtkRenderStatusWidget::render, this, &PDTWorkPageWidget::render);
    connect(ui->vtkStatusWidget,SIGNAL(changeMeshArraySignal(int)),this,SLOT(changeMeshArray(int)));
}

void PDTWorkPageWidget::setupOpticalPropertyWidget(){
  connect(ui->add_opt_page, SIGNAL(new_optical_property(OpticalProperty* )),this,SLOT(new_property_add_to_widget(OpticalProperty* )));
  //connect(ui->add_opt_page,SIGNAL(new_optical_property(OpticalProperty*)),this,SLOT(set_slider_label));
  connect(ui->opticalTable,SIGNAL(modify_signal(QString)),this,SLOT(switchModifyPage(QString)));
  connect(ui->opticalModifyPage,SIGNAL(modify_property_signal(OpticalProperty*)),this,
          SLOT(modify_property(OpticalProperty*)));

  connect(ui->opticalTable,SIGNAL(delete_item_signal(QString,QPoint)),this,SLOT(deletOpticalTreeItem(QString,QPoint)));

  connect(ui->opticalModifyPage,SIGNAL(cancel_signal()),this,SLOT(cancelOpticalModifyWidget()));

  connect(ui->add_opt_page,SIGNAL(cancel_optical_property()),this,SLOT(cancelOpticalModifyWidget()));


}

/*This function is called from PDTGUIMainwindow
 * It loads the mesh file*/

bool PDTWorkPageWidget::loadFile(const QString & filePath) {
    QFileInfo info(filePath);// filepath is mesh file path
    QString file_extension = info.suffix();

    meshFileInfo = info;

    vtkSmartPointer<vtkDataSetMapper> dataSetMapper =
            vtkSmartPointer<vtkDataSetMapper>::New();

    // Read the file into reader. If it is not read successfully, report errors.
    if(! m_reader.read(file_extension, filePath,dataSetMapper)) {
        return false;
    }

    ui->vtkStatusWidget->setMaxID(m_reader.getMax());
    ui->add_opt_page->setMaxID(m_reader.getMax());

    // Default set render mode to geometric now
    m_render_status.setRenderMode(GEOMETRIC_MODE);

    vtkRenderer* renderer = ui->qvtkWidget->GetRenderWindow()->GetRenderers()->GetFirstRenderer();

    dataSetMapper->SetInputConnection(m_reader.GetOutputPort());

    // The array is index 0 (region)
    changeMeshArray(0);


    //draw out mesh
    m_actor.GetDataSetActor()->SetMapper(dataSetMapper);


    renderer->RemoveAllViewProps();
    renderer->AddViewProp(m_actor.GetDataSetActor());

    renderer->ResetCamera();

    ui->qvtkWidget->GetRenderWindow()->Render();


    setRenderWidgetArrayNames();

    return true;
}


//create another function that receives max and min as input to load mesh
//modify load mesh function
pVTKActor* PDTWorkPageWidget::loadRegion(int max,int min) {
    vtkSmartPointer<vtkUnstructuredGrid> rawData = vtkSmartPointer<vtkUnstructuredGrid>::New();

    //data before apply filter
    rawData = m_reader.getUGReader()->GetOutput();

    //set input to filter
    m_reader.getFilter()->SetInputData(rawData);
    //filter out material
    m_reader.getFilter()->ThresholdBetween(min,max);

    m_reader.getFilter()->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "Region");
    m_reader.getFilter()->Update();


    //set color array
    vtkSmartPointer<vtkDataSetMapper> dataSetMapper = m_reader.get_dataSetMapper();
    m_reader.SetMapperColorArray(dataSetMapper);

    // Default set render mode to geometric now
    m_render_status.setRenderMode(GEOMETRIC_MODE);

    //vtkRenderer* renderer = ui->qvtkWidget->GetRenderWindow()->GetRenderers()->GetFirstRenderer();

    //draw out color array and mesh after filter
    dataSetMapper->SetInputConnection(m_reader.getFilter()->GetOutputPort());

    pVTKActor* mActor = new pVTKActor;

    mActor->GetDataSetActor()->SetMapper(dataSetMapper);

    mActor->GetDataSetActor()->GetMapper()->Update();

    renderer->RemoveAllViewProps();

    renderer->AddActor(mActor->GetDataSetActor());

    renderer->ResetCamera();


    ui->qvtkWidget->GetRenderWindow()->Render();

    setRenderWidgetArrayNames();

    return mActor;
}

/* Used for displaying different colors for mesh.
 * Namely when the user switches between viewing
 * regions and fluence*/
void PDTWorkPageWidget::changeMeshArray(int array_num){

    vtkSmartPointer<vtkScalarBarActor> old_color_scale = m_reader.get_color_scale();
    renderer->RemoveActor(old_color_scale);

    vtkSmartPointer<vtkDataSetMapper> mapper =
            vtkSmartPointer<vtkDataSetMapper>::New();


    mapper->SetInputConnection(m_reader.GetOutputPort());


   // mapper =(vtkDataSetMapper*) m_actor.GetDataSetActor()->GetMapper();
    m_reader.SetMapperColorArray(mapper,array_num);

    QString current_name = mapper->GetArrayName();

    vtkSmartPointer<vtkLookupTable> lookupTable;


    if(current_name=="Region"){
       lookupTable= vtkSmartPointer<vtkLookupTable>::New();
    }
    else{
        lookupTable= vtkSmartPointer<vtkLogLookupTable>::New();
    }
    lookupTable->SetNumberOfTableValues(256);
    lookupTable->SetHueRange(0.677,1);
    lookupTable->Build();

    double * hue_range=new double[2];

    hue_range=lookupTable->GetHueRange();

    //Allows the color of the mesh to be changed
    mapper->SetLookupTable(lookupTable);
    mapper->SetScalarModeToUseCellFieldData();


     vtkSmartPointer<vtkLookupTable> lookupTable2=m_reader.setColorScale(mapper);
     vtkSmartPointer<vtkScalarBarActor> color_scale=vtkSmartPointer<vtkScalarBarActor>::New();

    color_scale=m_reader.setBarColorMap(lookupTable2,mapper,array_num);

    m_reader.set_color_scale(color_scale);




    m_actor.GetDataSetActor()->SetMapper(mapper);

    renderer->AddActor(color_scale);

    set_annotations();


}


/* Adds air to optical properties without
 * user having to specify in .opt file*/

void PDTWorkPageWidget::addAirProperty(){
    OpticalProperty* air_opt_prop;
    // check to see if optical list already has air
    bool exists=false;
    for(unsigned i=0;i<opticalPropertyList.size();i++){
        if(opticalPropertyList[i]->name=="air"){
            exists=true;
        }
    }

    //if no air, add air

    if(!exists){
        air_opt_prop= new OpticalProperty("air","0.0","0.0","1.1","0.0");
        this->newOpticalProperty(air_opt_prop);

    }

}

/* Annotations of the rightside bar that
 * quantifies the colors of the mesh*/

void PDTWorkPageWidget::set_annotations(){
     vtkSmartPointer<vtkDataSetMapper> dataSetMapper= m_reader.get_dataSetMapper();
     vtkSmartPointer<vtkLookupTable> lookupTable = m_reader.get_lin_lookupTable();

     m_reader.set_annotations(lookupTable,dataSetMapper,opticalPropertyList);
}


void PDTWorkPageWidget::setRenderWidgetArrayNames(){
    std::vector<const char*> array_names = m_reader.getArrayNames();
    ui->vtkStatusWidget->setMeshArrayNames(array_names);

}

/* X,Y,Z axis */
void PDTWorkPageWidget::showCoordinateAxis(){

    // Add the actors to the scene
    //renderer->AddActor(actor);
    //renderer->SetBackground(.2, .3, .4);

    vtkAxesActor* axes =vtkAxesActor::New();

    renderer->GetRenderWindow()->GetInteractor()->Initialize();

    vtkOrientationMarkerWidget* widget =vtkOrientationMarkerWidget::New();

    widget->SetOutlineColor( 0.9300, 0.5700, 0.1300 );
    widget->SetOrientationMarker( axes );
    widget->SetInteractor( renderer->GetRenderWindow()->GetInteractor() );
    widget->SetViewport( 0.0, 0.0, 0.4, 0.4 );
    widget->SetEnabled( 1 );


    renderer->ResetCamera();
    renderer->GetRenderWindow()->Render();



}

/* Number of regions in the mesh*/
int PDTWorkPageWidget::numberOfRegions(){
    vtkUnstructuredGrid * ug = vtkUnstructuredGrid::SafeDownCast(m_reader.GetOutput());
    vtkCellData * cellData = ug->GetCellData();
    vtkDataArray* scalarArray = cellData->GetScalars();
    vtkIdType numTetra = scalarArray->GetNumberOfTuples();
    std::set<double> unique_regions;
    for (vtkIdType tetraIdx = 0 ; tetraIdx < numTetra; tetraIdx ++) {
         unique_regions.insert(scalarArray->GetComponent(tetraIdx,0));

    }

    return unique_regions.size();
}


//create source according to source type
void PDTWorkPageWidget::createSource(PDTVtkSouceType type, int pid) {
    SourcePlacementType placementWidgetType;

    // Choose the plane to add the source
    if (pid == -1)
        pid = PlanePlacementChooseDialog::choosePlanePlacement(*this->sourceList, this);//choose plane to place source

    if (pid == PlanePlacementChooseDialog::CANCEL) {
        return ;
    } else {
        if (pid != FREE_PID ) {
            switch(type) {
            case PDTVtkSouceType::LINE:
                placementWidgetType = SourcePlacementType::LINE_PLANE_PLACEMENT;
                break;
            case PDTVtkSouceType::FIBER:
                placementWidgetType = SourcePlacementType::CUT_END_FIBER;
            default:
                MessageBoxUtils::showErrorMessageBox(tr("Unsupported source type,1"),this);
                return;
            }
        } else {
            switch(type) {
            case PDTVtkSouceType::LINE:
                placementWidgetType = SourcePlacementType::FREE_LINE;
                break;

            case PDTVtkSouceType::POINT:
                placementWidgetType=SourcePlacementType::FREE_POINT;
                break;
            //new added case
            case PDTVtkSouceType::FIBER:
                placementWidgetType = SourcePlacementType::CUT_END_FIBER;
                break;
            default:
                MessageBoxUtils::showErrorMessageBox(tr("Unsupported source type,2"),this);
                return;

            }
        }
    }

    showPlacementWidget(placementWidgetType, SourcePlacementAction::CREATE, pid);
}


//
void PDTWorkPageWidget::showPlacementWidget(SourcePlacementType type, SourcePlacementAction action, int pid, int sid){
    ui->sourcePlacementWidgetStack->setCurrentIndex(type);//need to define a new type here for fiber
    SourcePlacementWidget* currentPlacementWidget = (SourcePlacementWidget*)ui->sourcePlacementWidgetStack->currentWidget();
    // Set current pid and action first, and setup widget
    currentPlacementWidget->setCurrentPid(pid);
    currentPlacementWidget->setCurrentSid(sid);
    currentPlacementWidget->setCurrentAction(action);

    currentPlacementWidget->setupWidget();

    // Diable other widgets until placement finishes
    // and show source placement tab
    enablePlacementMode(true);
}



void PDTWorkPageWidget::createLineSource() {
    createSource(PDTVtkSouceType::LINE);
}

void PDTWorkPageWidget::createFiberSource() {
    createSource(PDTVtkSouceType::FIBER);
}

void PDTWorkPageWidget::createPointSource(){
    createSource(PDTVtkSouceType::POINT);
}

void PDTWorkPageWidget::createPlanePlacement() {
    showPlacementWidget(PLANE_PLACEMENT, SourcePlacementAction::CREATE);
}

void PDTWorkPageWidget::modifyPlanePlacement(int pid){
    sourceList->hidePlanePlacement(pid);
    showPlacementWidget(PLANE_PLACEMENT, SourcePlacementAction::MODIFY, pid);

}

void PDTWorkPageWidget::modifySource(PDTVtkSouceType type, int sid){
    SourcePlacementType placementWidgetType;

    // Hide the source from vtk view temporarily
    sourceList->hideSource(sid);

    if (sourceList->isFreeSource(sid)) {

        switch(type) {
        case PDTVtkSouceType::LINE:
            placementWidgetType = SourcePlacementType::FREE_LINE;
            break;
        case PDTVtkSouceType::POINT:
            placementWidgetType=SourcePlacementType::FREE_POINT;
            break;
        case PDTVtkSouceType::FIBER:
            placementWidgetType=SourcePlacementType::CUT_END_FIBER;
            break;
        default:
            MessageBoxUtils::showErrorMessageBox(tr("Unsupported source type,3"),this);
            return;

        }
    } else {
        switch(type) {
        case PDTVtkSouceType::LINE:
            placementWidgetType = SourcePlacementType::LINE_PLANE_PLACEMENT;
            break;
        default:
            MessageBoxUtils::showErrorMessageBox(tr("Unsupported source type,4"),this);
            return;
        }
    }

    showPlacementWidget(placementWidgetType, SourcePlacementAction::MODIFY, sourceList->getSourcePlanePlacementId(sid), sid);
}

void PDTWorkPageWidget::enablePlacementMode(bool enable) {
    ui->sourcePlacementWidgetStack->setEnabled(enable);
    ui->vtkStatusWidget->setEnabled(!enable);
    ui->sourceTable->setEnabled(!enable);
    ui->sourceStackedWidget->setEnabled(!enable);
    if (enable){
        ui->sourcePlacementDock->show();
        emit sendStatusBarMessage(tr("Enter placement mode"));
    } else {
        ui->sourcePlacementDock->close();
        emit sendStatusBarMessage(tr(""));
    }

    ui->sourceTable->update();
    renderWindow->Render();
}

//This slot function is used to catch the point position
void PDTWorkPageWidget::getPosition()
{
    int* globalPos = ui->qvtkWidget->GetInteractor()->GetEventPosition();
    for (int i = 0; i < 3; i++)
        globalPos[i] = ui->qvtkWidget->GetInteractor()->GetEventPosition()[i];

    ui->qvtkWidget->GetInteractor()->GetPicker()->Pick(globalPos[0], globalPos[1], 0, renderer);
    double pickedPos[3];
    ui->qvtkWidget->GetInteractor()->GetPicker()->GetPickPosition(pickedPos);
    QString str;
    str.sprintf("Picked position : x = %f , y = %f , z = %f  Global position: %d,%d,%d", pickedPos[0], pickedPos[1], pickedPos[2]
            , globalPos[0], globalPos[1], globalPos[2]);


    if(!(pickedPos[0]==pickedPos[1] && pickedPos[1]==pickedPos[2] && pickedPos[0]==0)){


        // create a function here to open a drop down window
        int* globalPos = ui->qvtkWidget->GetInteractor()->GetEventPosition();
        //mesh_right_click(globalPos);

        emit mesh_right_click();
    }
    emit sendStatusBarMessage(str);
}

/* This function was done as an execise
 * It allows to right click on mesh*/

void PDTWorkPageWidget::mesh_right_click(){

            QPoint _Position = this->mapFromParent(QCursor::pos());
            QMenu menu;
            QAction Add_property("Add Property",&menu);
            QAction dummy_action("Dummy Action",&menu);


            connect(&Add_property,SIGNAL(triggered()),this,SLOT(present_message()));

            menu.addAction(&Add_property);
            menu.addAction(&dummy_action);
            menu.exec(_Position);


}


void PDTWorkPageWidget::present_message(){


}

void PDTWorkPageWidget::render() {
    renderWindow->Render();
}

/* Changing the list of sources*/
void PDTWorkPageWidget::setSourceList(SourceList* list) {
    assert(list != nullptr);

    // Remove the old list and set the new one
    delete this->sourceList;
    this->sourceList = list;

    // Set source list to source table
    ui->sourceTable->setSourceList(list);
    ui->sourceTable->update();

    // Set source list to widgets
    ((SourcePlacementWidget*) ui->sourcePlacementWidgetStack->widget(LINE_PLANE_PLACEMENT))->setSourceList(list);
    ((SourcePlacementWidget*) ui->sourcePlacementWidgetStack->widget(FREE_LINE))->setSourceList(list);
    ((SourcePlacementWidget*) ui->sourcePlacementWidgetStack->widget(PLANE_PLACEMENT))->setSourceList(list);
    ((SourcePlacementWidget*) ui->sourcePlacementWidgetStack->widget(FREE_POINT))->setSourceList(list);
    //new added
    ((SourcePlacementWidget*) ui->sourcePlacementWidgetStack->widget(CUT_END_FIBER))->setSourceList(list);
}


/* Display the optical sources on the sources widget*/

void PDTWorkPageWidget::TclSetSourceTable(std::vector<PDTVtkSource*> sources){
    SourceList* tcl_source_list = new SourceList(renderer);

    for(unsigned i=0;i<sources.size();i++){
        tcl_source_list->addSource(sources[i]);
        //tcl_source_list->showSource(i);

        //renderer->AddActor(sources[i]->getActor());
        sources[i]->update();

        if(sources[i]->getSourceType()==POINT){
            PDTVtkPointSource* mysource = (PDTVtkPointSource* )sources[i];

            //number of vtk visible points for one point source
            mysource->SetNumPointInSource(1);

        }




    }

    setSourceList(tcl_source_list);

   // tcl_source_list->showAllSource();


}

void PDTWorkPageWidget::saveSourceFile(const QString filePath) {
    SourceListWriter w(filePath);
    if (! w.writeSourceList(*sourceList)) {
        MessageBoxUtils::showErrorMessageBox("Source writing failed", this, "Writing failure");
    }
}

void PDTWorkPageWidget::loadSourceFile(const QString filePath) {
    SourceListReader reader(filePath);
    SourceList* list = new SourceList(renderer);

    if (reader.readSourceList(*list)) {
        setSourceList(list);
        render();
        MessageBoxUtils::showInfoMessageBox("Load new source sucessfully!", this, "Loading success");
    } else {
        MessageBoxUtils::showErrorMessageBox("Source loading failed", this, "Loading failure");
        delete list;
    }
}


void PDTWorkPageWidget::saveOpticalFile(const QString filePath){

    OpticalPropertyListWriter w(filePath);
    if(! w.writeOpticalPropertyList(this->opticalPropertyList)){
        MessageBoxUtils::showErrorMessageBox("Optical property writing failed",this,"Writing failure");
    }

}

void PDTWorkPageWidget::createVtkTool(VtkToolType type) {
    ui->PVtkToolStackedWidget->setCurrentIndex(type);
    PDTVtkToolWidget* curWidget = (PDTVtkToolWidget*) ui->PVtkToolStackedWidget->currentWidget();
    assert(curWidget != nullptr);
    curWidget->setEnabled(true);
    enableVtkToolMode(true);
}

void PDTWorkPageWidget::createClipper() {
    createVtkTool(DATASET_CLIPPER);
}

void PDTWorkPageWidget::enableVtkToolMode(bool enable) {
    ui->PVtkToolStackedWidget->setEnabled(enable);
    ui->vtkStatusWidget->setEnabled(!enable);
    ui->sourceTable->setEnabled(!enable);
    if (enable){
        ui->PVtkToolDockWidget->show();
        emit sendStatusBarMessage(tr("Enter vtk tool mode"));
    } else {
        ui->PVtkToolDockWidget->close();
        emit sendStatusBarMessage(tr(""));
    }

    renderWindow->Render();
}


void PDTWorkPageWidget::new_property_add_to_widget(OpticalProperty* newOpticalProperty){
    this->newOpticalProperty(newOpticalProperty);


    //add the optical to optical property tree widget
    ui->opticalTable->header()->resizeSection(0,100);


    QTreeWidgetItem * newItem= new QTreeWidgetItem();

    newItem->setText(0,newOpticalProperty->name);
    ui->opticalTable->addTopLevelItem(newItem);

    //update the scale bar annotations
    vtkSmartPointer<vtkLookupTable> lookupTable = m_reader.get_lin_lookupTable();
    vtkSmartPointer<vtkDataSetMapper> dataSetMapper = m_reader.get_dataSetMapper();
    m_reader.set_annotations(lookupTable,dataSetMapper,opticalPropertyList);

    ui->vtkStatusWidget->create_new_label();

}



void PDTWorkPageWidget::newOpticalProperty(OpticalProperty* newOpticalProperty){
    int ans = checkDuplicateName(newOpticalProperty->name);
    if(ans!=-1){
        QMessageBox err_msg;
        err_msg.setIcon(QMessageBox::Critical);
        err_msg.setText("\""+newOpticalProperty->name+"\""+" already exists");
        err_msg.exec();
        return;
    }

    //air has to be added at index 0
    if(newOpticalProperty->name=="air"){
        std::vector<OpticalProperty*>::iterator it = opticalPropertyList.begin();
        opticalPropertyList.insert(it,newOpticalProperty);
    }
    else{
        this->opticalPropertyList.push_back(newOpticalProperty);
    }



    ui->OpticalStackedWidget->setCurrentIndex(1);
}





void PDTWorkPageWidget::addPropertyToTable(){
    for(unsigned i=0;i<opticalPropertyList.size();i++){
        ui->opticalTable->header()->resizeSection(0,100);
        //unsigned last_itm_index= opticalPropertyList.size()-1;

        QTreeWidgetItem * newItem= new QTreeWidgetItem();

        newItem->setText(0,opticalPropertyList[i]->name);
        ui->opticalTable->addTopLevelItem(newItem);
    }
}

int PDTWorkPageWidget::checkDuplicateName(QString newName){
    int ans=-1;
    for(unsigned i=0;i<opticalPropertyList.size();i++){
        if(newName==opticalPropertyList[i]->name){
            ans=i;
        }
    }

    return ans;

}

void PDTWorkPageWidget::modifyOPticalProperty(OpticalProperty* modified_optical_property,int index){
    delete ui->opticalTable->takeTopLevelItem(index);
    QTreeWidgetItem* newItem= new QTreeWidgetItem();

    newItem->setText(0,modified_optical_property->name);

    ui->opticalTable->insertTopLevelItem(index,newItem);



}


void PDTWorkPageWidget::deleteOpticalProperty(int index,QPoint pos){
    QTreeWidgetItem* item = ui->opticalTable->itemAt(pos);
    delete item;

}

void PDTWorkPageWidget::switchModifyPage(QString itemName){

    unsigned itemIndex = getItemIndexFromName(itemName);


    ui->opticalModifyPage->set_current_itm_index(itemIndex);

    OpticalProperty* modifyObjectName=opticalPropertyList[itemIndex];
    ui->opticalModifyPage->changewidget_values(modifyObjectName);



    ui->OpticalStackedWidget->setCurrentIndex(2);


}


void PDTWorkPageWidget::deletOpticalTreeItem(QString itemName,QPoint position){
    unsigned item_index = getItemIndexFromName(itemName);
    delete opticalPropertyList[item_index];


    opticalPropertyList.erase(opticalPropertyList.begin()+item_index);

     set_annotations();

    deleteOpticalProperty(item_index,position);




}

void PDTWorkPageWidget::cancelOpticalModifyWidget(){

    ui->OpticalStackedWidget->setCurrentIndex(1);
}





unsigned PDTWorkPageWidget::getItemIndexFromName(QString itemName){

    for(unsigned i=0;i<this->opticalPropertyList.size();i++){
        if(opticalPropertyList[i]->name==itemName){
            return i;
        }
    }

}



void PDTWorkPageWidget::modify_property(OpticalProperty* modified_optical_property){
    //The item currently being modified is already stored
    int index = ui->opticalModifyPage->get_current_itm_index();


    int ans=checkDuplicateName(modified_optical_property->name);
    if(ans!=index && ans!=-1){
        QMessageBox err_msg;
        err_msg.setIcon(QMessageBox::Critical);
        err_msg.setText("\""+modified_optical_property->name+"\""+" already exists");
        err_msg.exec();
        return;
    }


    if(opticalPropertyList[index]!=nullptr){
        delete opticalPropertyList[index];
    }

    opticalPropertyList[index]=modified_optical_property;
    modifyOPticalProperty(modified_optical_property,index);


    ui->OpticalStackedWidget->setCurrentIndex(1);

    set_annotations();


}



void PDTWorkPageWidget::on_add_property_clicked()
{   std::cout<<"button clicked"<<endl;
    ui->OpticalStackedWidget->setCurrentIndex(0);
    ui->add_opt_page->clear_name();
}


void PDTWorkPageWidget::on_modify_property_clicked()
{
    // get the index of the material item currently clicked
    int item_idx = ui->opticalTable->currentIndex().row();

    if (item_idx >= 0) // if less than 0, then nothing is clicked
    {
        QString item_name = opticalPropertyList[item_idx]->name;
        switchModifyPage(item_name);
    }
}


QFileInfo PDTWorkPageWidget::getMeshFileInfo(){
    return meshFileInfo;
}

std::vector<OpticalProperty*> PDTWorkPageWidget::getOpticalPropertyList(){
    return opticalPropertyList;
}

SourceList* PDTWorkPageWidget::getSourceList(){
    return sourceList;
}


void PDTWorkPageWidget::setTclProjInfo(TclProjInfo* projInfo){
    TclProjectInfo=projInfo;
}


TclProjInfo* PDTWorkPageWidget::getTclProjInfo(){


    TclProjInfo temp = (*(this->TclProjectInfo));

    return TclProjectInfo;
}

void PDTWorkPageWidget::process_finished(QProcess::ProcessState proc_state){

}


vtkSmartPointer<vtkRenderWindowInteractor> PDTWorkPageWidget::getrenderWindow(){
    return renderWindow->GetInteractor();
}

// slot for clicking on the delet optical value
void PDTWorkPageWidget::on_delete_property_clicked()
{

    // get the index of the material item clicked
    int item_index = ui->opticalTable->currentIndex().row();

    if (item_index >= 0) // if less than 0, nothing is clicked
    {

        QTreeWidgetItem* tree_item = ui->opticalTable->currentItem();

        delete opticalPropertyList[item_index]; // delete from the list
        opticalPropertyList.erase(opticalPropertyList.begin()+item_index); // decrease size of list

        delete tree_item; // delete from tree

        set_annotations();
    }
}

void PDTWorkPageWidget::on_delete_source_clicked()
{
    // get current selected item
    int item_id = ui->sourceTable->currentIndex().row();
    if (item_id > -1) // if not, then nothing is selected
    {
        // get the id of the source or plane to be modified
        int source_id = (ui->sourceTable->currentItem()->text(0)).toInt(); // 0 is for the first column in the table

        QTreeWidgetItem* source_item = ui->sourceTable->currentItem();

        QString item_type = source_item->text(1); // 1 is for the second column in the source table

        int i_type = -1;

        if (item_type == "Point")
            i_type = 0;
        else if (item_type == "Line")
            i_type = 1;
        else if (item_type == "PlanePlacement")
            i_type = 2;
        else if (item_type == "Fiber")
            i_type = 3;

        // call the function according to the correct type
        switch(i_type) {
            case 0:
            case 1:
            case 3:
                ui->sourceTable->removeSource(source_id);
                break;
            case 2:
                ui->sourceTable->removePlane(source_id);
                break;
            default:
                return;
        }

    }
}

void PDTWorkPageWidget::on_add_source_clicked()
{
    ui->sourceStackedWidget->setCurrentIndex(1);
}

void PDTWorkPageWidget::on_modify_source_clicked()
{
    // get current selected item
    int item_id = ui->sourceTable->currentIndex().row();
    if (item_id > -1) // if not, then nothing is selected
    {
        // get the id of the source or plane to be modified
        int source_id = (ui->sourceTable->currentItem()->text(0)).toInt(); // 0 is for the first column in the table

        QTreeWidgetItem* source_item = ui->sourceTable->currentItem();

        QString item_type = source_item->text(1); // 1 is for the second column in the source table

        int i_type = -1;

        if (item_type == "Point")
            i_type = 0;
        else if (item_type == "Line")
            i_type = 1;
        else if (item_type == "PlanePlacement")
            i_type = 2;
        else if(item_type == "Fiber")
            i_type = 3;

        // call the function according to the correct type
        switch(i_type) {
            case 0:
                modifySource(POINT, source_id);
                break;
            case 1:
                modifySource(LINE, source_id);
                break;
            case 2:
                modifyPlanePlacement(source_id);
                break;
            case 3:
                modifySource(FIBER,source_id);
                break;
            default:
                return;
        }
    }

}


void PDTWorkPageWidget::on_FiberSourceButton_clicked(){
    this->createFiberSource();
    ui->sourceStackedWidget->setCurrentIndex(0);
}

void PDTWorkPageWidget::on_LineSourceButton_clicked()
{
    this->createLineSource();
    ui->sourceStackedWidget->setCurrentIndex(0);
}

void PDTWorkPageWidget::on_PointSourceButton_clicked()
{
    this->createPointSource();
    ui->sourceStackedWidget->setCurrentIndex(0);
}

void PDTWorkPageWidget::on_PlaneButton_clicked()
{
    createPlanePlacement();
    ui->sourceStackedWidget->setCurrentIndex(0);
}

 double* PDTWorkPageWidget::getline(){
    return line;
}

void PDTWorkPageWidget::set_up_sliderbar(){
    ui->vtkStatusWidget->set_up_slider();
}

void PDTWorkPageWidget::set_slider_label(){
    ui->vtkStatusWidget->set_sliderLabel();
}


