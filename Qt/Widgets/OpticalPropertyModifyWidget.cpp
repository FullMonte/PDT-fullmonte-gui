#include "OpticalPropertyModifyWidget.h"
#include "ui_OpticalPropertyModifyWidget.h"
#include <QString>
#include <QMessageBox>

OpticalPropertyModifyWidget::OpticalPropertyModifyWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OpticalPropertyModifyWidget)
{
    ui->setupUi(this);
}

void OpticalPropertyModifyWidget::changewidget_values(OpticalProperty* modify_object){
    //QString item_index_txt=QString::number(item_index);
    ui->Name_value->setText(modify_object->name);
    ui->Absorption_coeff_value->setText(modify_object->absorption);
    ui->Anisotropy_factor_val->setText(modify_object->anistropy_factor);
    ui->Refractive_index_value->setText(modify_object->refractive_index);
    ui->Scattering_coeff_value->setText(modify_object->scattering);

}





void OpticalPropertyModifyWidget::on_Modify_clicked()
{



    QString Name = ui->Name_value->text();
    QString absorp_coeff= ui->Absorption_coeff_value->text();
    QString scatter_coeff=ui->Scattering_coeff_value->text();
    QString anisotropy_factor=ui->Anisotropy_factor_val->text();
    QString refractive_index = ui->Refractive_index_value->text();


    OpticalProperty* modified_optical_property= new OpticalProperty(Name,absorp_coeff,scatter_coeff,refractive_index,anisotropy_factor);

    bool valid = modified_optical_property->check_property_vals();
    if(valid){
        emit modify_property_signal(modified_optical_property);
    }
    else{
        delete modified_optical_property;
    }


}

//cancel button
void OpticalPropertyModifyWidget::on_pushButton_clicked()
{

        emit cancel_signal();
}

void OpticalPropertyModifyWidget::set_current_itm_index(unsigned current_index){
    this->current_modified_item_index=current_index;
}

unsigned OpticalPropertyModifyWidget::get_current_itm_index(){
    return this->current_modified_item_index;
}
