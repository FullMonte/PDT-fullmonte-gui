#include "FiberSourcePlacementWidget.h"
#include "ui_SourcePlacementWidget.h"
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include "LightSource/PDTVtkFiberSource.h"
#include <vtkConeSource.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <QMessageBox>
#include <QPushButton>
#include "../Utils/MessageBoxUtils.h"

/* this .cpp file creates a source placement widget of Fiber source, it pops up when user clickes on Fiber button,
 * The widget has 5 parameters:
 * 1.center(x,y,z)
 * 2.direction(x,y,z)
 * 3.radius
 * 4.NA(Numerical Aperture:sine of angle from central axis to outer shell)
 * 5.power
*/

FiberSourcePlacementWidget::FiberSourcePlacementWidget(SourceList* sourceList, PDTWorkPageWidget* workPageWidget, QWidget *parent) :
    SourcePlacementWidget(sourceList, workPageWidget, parent){

    //create user input widget
    QHBoxLayout * centerLayout = new QHBoxLayout;
    QHBoxLayout * directionLayout = new QHBoxLayout;
    QHBoxLayout * radiusLayout = new QHBoxLayout;
    QHBoxLayout * NALayout = new QHBoxLayout;
    QHBoxLayout *powerLayout = new QHBoxLayout;

    centerLayout->setSpacing(5);
    directionLayout->setSpacing(5);
    radiusLayout->setSpacing(2);
    NALayout->setSpacing(2);
    powerLayout->setSpacing(2);

    //add user input box
    powerEdits = new QLineEdit(this);
    radiusEdits = new QLineEdit(this);
    NAEdits = new QLineEdit(this);

    powerEdits->setText(tr("0.00"));
    radiusEdits->setText(tr("0.00"));
    NAEdits->setText(tr("0.00"));



    powerLayout->addWidget(powerEdits);
    radiusLayout->addWidget(radiusEdits);
    NALayout->addWidget(NAEdits);

    connect(radiusEdits, &QLineEdit::editingFinished,
            this, &FiberSourcePlacementWidget::moveWidget);
    connect(NAEdits, &QLineEdit::editingFinished,
            this, &FiberSourcePlacementWidget::moveWidget);
    connect(powerEdits, &QLineEdit::editingFinished,
            this,&FiberSourcePlacementWidget::moveWidget);

    //initialize each box with 0.00
    for (int i=0; i < 3; i++) {
        centerLineEdits[i] = new QLineEdit(this);
        centerLineEdits[i]->setText(tr("0.00"));
        centerLayout->addWidget(centerLineEdits[i]);
        connect(centerLineEdits[i], &QLineEdit::editingFinished,
                this, &FiberSourcePlacementWidget::moveWidget);

        directionLineEdits[i] = new QLineEdit(this);
        directionLineEdits[i]->setText(tr("0.00"));
        directionLayout->addWidget(directionLineEdits[i]);
        connect(directionLineEdits[i], &QLineEdit::editingFinished,
                this, &FiberSourcePlacementWidget::moveWidget);
    }


    //add label to each box
    QLabel* powerLabel = new QLabel(tr("power"),this);
    QLabel* centerLabel = new QLabel(tr("center\n(X,Y,Z)"), this);
    QLabel* directionLabel = new QLabel(tr("directon\n(X,Y,Z)"), this);
    QLabel* radiusLabel = new QLabel(tr("radius"),this);
    QLabel* NALabel = new QLabel(tr("NA"),this);

    ui->gridLayout->removeWidget(ui->placeButton);
    ui->gridLayout->removeWidget(ui->cancelButton);
    //center dialog
    ui->gridLayout->addWidget(centerLabel, 0, 2, 1, 1);
    ui->gridLayout->addItem(centerLayout, 1, 0, 1, 5);
    //direction dialog
    ui->gridLayout->addWidget(directionLabel, 2, 2, 1, 1);
    ui->gridLayout->addItem(directionLayout, 3, 0, 1, 5);


    ui->gridLayout->addWidget(radiusLabel, 6, 0, 1, 1);
    ui->gridLayout->addItem(radiusLayout, 7, 0, 1, 2);

    ui->gridLayout->addWidget(NALabel, 6, 2, 1, 1);
    ui->gridLayout->addItem(NALayout, 7, 2, 1, 2);

    ui->gridLayout->addWidget(powerLabel, 6, 4, 1, 1);
    ui->gridLayout->addItem(powerLayout, 7, 4, 1, 2);

    ui->gridLayout->addWidget(ui->placeButton, 8, 1);
    ui->gridLayout->addWidget(ui->cancelButton, 8, 3);

    //information button that explains definition of NA
    ui->gridLayout->addWidget(ui->informationButton,6,3);

    //renderWindow = workPageWidget->getrenderWindow();

}

FiberSourcePlacementWidget::~FiberSourcePlacementWidget(){

}


//being called when user click -->Fiber -->create
void FiberSourcePlacementWidget::createSource() {

    std::cout << "create source is called" << std::endl;
    double center [3], direction [3];
    double Radius;
    double NA;
    double Power;
    Radius = getRadius();
    NA = getNA();
    getCenter(center);
    getDirection(direction);
    Power = getPower();


    //error checking parameters
    if(Radius <= 0){
        MessageBoxUtils::showErrorMessageBox(tr("invalid user input,radius should be a positive number"),this);
        return;
    }
    if(NA <= 0){
        MessageBoxUtils::showErrorMessageBox(tr("invalid user input,NA should be a positive number"),this);
        return;
    }
    else if(NA >= 1){
        MessageBoxUtils::showErrorMessageBox(tr("invalid user input,NA should be smaller than 1"),this);
        return;
    }

    PDTVtkFiberSource* newFiberSource = new PDTVtkFiberSource(Radius,center,direction,NA,Power);

    assert(sourceList != nullptr);
    sourceList->addSource(newFiberSource);

}

//when user click modify
void FiberSourcePlacementWidget::modifySource() {
    double center [3], direction [3];
    double radius;
    double NA;
    double power;

    power = getPower();
    radius = getRadius();
    NA = getNA();
    getCenter(center);
    getDirection(direction);

    PDTVtkFiberSource* FiberSource = (PDTVtkFiberSource *)sourceList->getSourceById(curSid);
    FiberSource->SetRadius(radius);
    FiberSource->SetCenter(center[0],center[1],center[2]);
    FiberSource->SetDirection(direction[0],direction[1],direction[2]);
    FiberSource->SetNA(NA);
    FiberSource->SetPower(power);
    FiberSource->update();

    assert(sourceList != nullptr);
    sourceList->showSource(curSid);


}


//does not support this feature for Fibersource
void FiberSourcePlacementWidget::moveWidget() {

}

//does not support this feature for Fibersource
void FiberSourcePlacementWidget::setupWidget() {


}

void FiberSourcePlacementWidget::getCenter(double center[3]){
    for(int i = 0 ; i < 3; i++){
        center[i] = centerLineEdits[i]->text().toDouble();
    }

    std::cout << "center " << center[0] << center[1] << center[2] << std::endl;
}

void FiberSourcePlacementWidget::getDirection(double direction[3]){
    for(int i = 0 ; i < 3; i++){
        direction[i] = directionLineEdits[i]->text().toDouble();
    }

    std::cout << "direction " << direction[0] << direction[1] << direction[2] << std::endl;
}

double FiberSourcePlacementWidget::getRadius(){
    double radius = radiusEdits->text().toDouble();

    std::cout << "radius is " << radius;

    return radius;
}

double FiberSourcePlacementWidget::getNA(){
    double NA = NAEdits->text().toDouble();

    std::cout << "NA is " << NA;

    return NA;
}


double FiberSourcePlacementWidget::getPower(){
    double Power = powerEdits->text().toDouble();

    std::cout<< "power is" << Power;

    return Power;
}

void FiberSourcePlacementWidget::showCurrentSource() {
    assert(sourceList != nullptr);
    sourceList->showSource(curSid);
}

bool FiberSourcePlacementWidget::closeWidget() {
    return 0;
}

