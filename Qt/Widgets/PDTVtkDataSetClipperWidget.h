#ifndef PDTVTKDATASETCLIPPERWIDGET_H
#define PDTVTKDATASETCLIPPERWIDGET_H

#include "PDTVtkToolWidget.h"

class PDTVtkDataSetClipperWidget : public PDTVtkToolWidget
{
public:
    PDTVtkDataSetClipperWidget(pVTKReader* reader, vtkActor* actor, vtkRenderer* renderer,
                                PDTWorkPageWidget* workPage, QWidget *parent = nullptr);
};

#endif // PDTVTKDATASETCLIPPERWIDGET_H
