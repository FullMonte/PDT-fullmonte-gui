#include "PDTOpticalTreeWidget.h"
#include <QMenu>
#include <QMouseEvent>
#include <QMessageBox>
#include <iostream>
#include <QWidget>
#include <QAction>

PDTOpticalTreeWidget::PDTOpticalTreeWidget(QWidget* parent):QTreeWidget(parent)
{
    this->setColumnCount(1);
    QStringList headerLabels;

    headerLabels.push_back("Optical Properties");
    this->setHeaderLabels(headerLabels);

    //Allows for custom
    this->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this,
            SIGNAL(customContextMenuRequested(const QPoint)),
            SLOT(prepareMenu(const QPoint)));


    connect(this,
            SIGNAL(itemClicked(QTreeWidgetItem*,int)),
            SLOT(onItemClicked(QTreeWidgetItem*,int)));




}


void PDTOpticalTreeWidget::onItemClicked(QTreeWidgetItem*,int){



}


void PDTOpticalTreeWidget::prepareMenu(const QPoint &pos){
    QPoint _Position = this->mapFromParent(QCursor::pos());
    QTreeWidgetItem* item = itemAt(pos);

    if (item == NULL) // check if list is empty
    {
        return;
    }

    QMenu* drop_down = new QMenu();
    QAction* modify= new QAction("Modify",drop_down);
    QAction* delete_itm= new QAction("Delete",drop_down);
    drop_down->addAction(modify);
    drop_down->addAction(delete_itm);

    int item_id=item->columnCount();

    QString item_name = item->text(0);

    // connect modify and delete triggers to respective signals
    connect(modify, &QAction::triggered,this,[=](){ emit_prepare_menu_PDTWPW(item_name); });
    connect(delete_itm,&QAction::triggered,this,[=](){emit_delete_item(item_name,pos); });

    drop_down->exec(_Position);
}

// signal emitted to invoke function in PDTWorkPageWidget

void PDTOpticalTreeWidget::emit_prepare_menu_PDTWPW(QString item_name){


    emit modify_signal(item_name);

}


void PDTOpticalTreeWidget::emit_delete_item(QString item_name,QPoint _Position){
    emit delete_item_signal(item_name,_Position);
}
