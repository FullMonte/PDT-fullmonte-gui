#ifndef PDTWORKPAGEWIDGET_H
#define PDTWORKPAGEWIDGET_H

#include <QWidget>
#include <QMainWindow>

#include <vtkEventQtSlotConnect.h>
#include <vtkLineWidget.h>
#include <vtkPointWidget.h>
#include <vtkNew.h>
#include <vtkPlaneWidget.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>

#include "VTK/Core/pVTKActor.h"
#include "VTK/Core/pVTKMapper.h"
#include "VTK/Core/pVTKReader.h"
#include "VTK/Core/pVTKRenderStatus.h"

#include "LightSource/SourceList.h"

#include "Qt/Objects/ProjectInfo.h"

#include "PDTVtkToolWidgetEnum.h"
#include "SourcePlacementWidgetEnum.h"

#include "OpticalProperty/OpticalProperty.h"
#include "OpticalPropertyWidget.h"
#include "OpticalPropertyModifyWidget.h"

#include "OpticalProperty/IO/OpticalPropertyListWriter.h"

#include "Tcl/TclProjInfo.h"
#include <QProcess>


#include "vtkStructuredGridOutlineFilter.h"
#include "vtkLogLookupTable.h"

#include "vtkScalarBarActor.h"

#include "vtkActor2D.h"

#include "vtkProperty2D.h"


#include "LightSource/PDTVtkLineSource.h"
#include "LightSource/pdtvtkpointsource.h"







namespace Ui {
class PDTWorkPageWidget;
}

class PDTWorkPageWidget : public QMainWindow
{
    Q_OBJECT

public:
    PDTWorkPageWidget(QWidget *parent = 0);
    ~PDTWorkPageWidget();

    bool loadFile(const QString &);
    pVTKActor* loadRegion(int max,int min);

    // Create sources
    void createPlanePlacement();
    void createLineSource();
    void createPointSource();
    void createFiberSource();

    // Source loader/saver
    void saveSourceFile(const QString);
    void loadSourceFile(const QString);

    void saveOpticalFile(const QString);

    // Create VTK tools
    void createVtkTool(VtkToolType);
    void createClipper();

    // Set an new source list
    void setSourceList(SourceList* list);

    void TclSetSourceTable(std::vector<PDTVtkSource*> sources);

    void modifyOPticalProperty(OpticalProperty* modified_optical_property,int index);
    void deleteOpticalProperty(int index,QPoint position);

    QFileInfo getMeshFileInfo();


    ProjectInfo & getProjectInfo() {return projectInfo; }


    std::vector<OpticalProperty*> getOpticalPropertyList();

    SourceList* getSourceList();

    int numberOfRegions();

    void showCoordinateAxis();



    void setTclProjInfo(TclProjInfo*);
    TclProjInfo* getTclProjInfo();


    void addAirProperty();

    void set_annotations();

    void setRenderWidgetArrayNames();

    pVTKRenderStatus getRenderStatus(){
        return m_render_status;
    }

    //void setline(vector<double> line);

    double*  getline();
    void set_up_sliderbar();
    void set_slider_label();
public slots:
    // Set enabled/disabled the placement mode
    void enablePlacementMode(bool);

    // Set enabled/disabled the vtkTool mode
    void enableVtkToolMode(bool);

    // Pick the location in the coordinates
    void getPosition();

    // Render the window
    void render();

    //menu invoked on right click
    void mesh_right_click();

    void present_message();

    void new_property_add_to_widget(OpticalProperty* newOpticalProperty);

    void newOpticalProperty(OpticalProperty* newOpticalProperty);

    void addPropertyToTable();

    int checkDuplicateName(QString newName);

    void switchModifyPage(QString);

    void modify_property(OpticalProperty*);

    void deletOpticalTreeItem(QString,QPoint);

    void cancelOpticalModifyWidget();

    unsigned getItemIndexFromName(QString itemName);


    void process_finished(QProcess::ProcessState proc_state);

    void changeMeshArray(int);






    //void setSourceList(std::vector<OpticalProperty*>);

    vtkSmartPointer<vtkRenderWindowInteractor> getrenderWindow();




signals:
    void sendStatusBarMessage(const QString);
    void sendSourceMenuEnabled(bool);
    void sendToolMenuEnabled(bool);
    void sendSaveActionEnabled(bool);

private slots:
    // Source Creation functions
    // If pid equals -1, then jump out the pid choosing dialog
    void createSource(PDTVtkSouceType, int pid = -1);

    // Modify sources
    void modifyPlanePlacement(int pid);
    void modifySource(PDTVtkSouceType, int sid);





    // slots for the optical property widget
    void on_add_property_clicked();
    void on_modify_property_clicked();
    void on_delete_property_clicked();


    // slots for the source options widgets
    void on_add_source_clicked();
    void on_modify_source_clicked();
    void on_delete_source_clicked();

    // slots for the source type choices widgets
    void on_LineSourceButton_clicked();
    void on_PointSourceButton_clicked();
    void on_PlaneButton_clicked();
    void on_FiberSourceButton_clicked();

private:
    Ui::PDTWorkPageWidget * ui;

    ProjectInfo projectInfo;

    vtkSmartPointer<vtkRenderer> renderer;
    vtkSmartPointer<vtkRenderWindow> renderWindow;

    vtkNew<vtkEventQtSlotConnect> vtkQtConnect;

    // Widgets for placement
    vtkNew<vtkLineWidget> lineWidget;
    vtkNew<vtkPlaneWidget> planeWidget;
    vtkNew<vtkPointWidget> pointWidget;
    vtkNew<vtkLineWidget> coneWidget;
    //need a need widget for cone(cut-end fiber)

    // Source list
    SourceList* sourceList;

    // Optical properties list

    std::vector<OpticalProperty*> opticalPropertyList;

    // VTK PDT wrappers
    pVTKMapper m_mapper;
    pVTKReader m_reader;
    pVTKRenderStatus m_render_status;
    pVTKActor m_actor;

    QFileInfo meshFileInfo;

    TclProjInfo* TclProjectInfo;

    double* line;
    // Initial setup functions
    void setupVtk();
    void setupPlacementWidget();
    void setupSourceTreeWidget();
    void setupSourceList();
    void setupRenderStatusWidget();
    void setupVtkToolWidget();
    void setupOpticalPropertyWidget();


    // Placment widget helper 
    void showPlacementWidget(SourcePlacementType, SourcePlacementAction, int pid = FREE_PID, int sid = -1);



};

#endif
