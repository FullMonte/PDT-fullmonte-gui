#include "OpticalPropertyWidget.h"
#include "ui_OpticalPropertyWidget.h"
#include <QMessageBox>
#include "PDTWorkPageWidget.h"
#include "../Utils/MessageBoxUtils.h"




OpticalPropertyWidget::OpticalPropertyWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OpticalPropertyWidget)
{
    ui->setupUi(this);
}

//add new material when button is clicked
void OpticalPropertyWidget::on_pushButton_clicked()
{

  OpticalProperty *new_optical_properties= new OpticalProperty();

  //info message to limit #characters within material name according to #materialIDs of each mesh
  switch(maxID){
        case 1:
        if(ui->name_lineEdit->text().size() > 28){
            MessageBoxUtils::showInfoMessageBox(tr("Material name exceed 28 characters may result in text overlaping"),this);
            return;
        }
        break;
        case 2:
        if(ui->name_lineEdit->text().size() > 18){
          MessageBoxUtils::showInfoMessageBox(tr("Material name exceed 18 characters may result in text overlaping"),this);
          return;
        }
        break;
        case 3:
        if(ui->name_lineEdit->text().size() > 13){
            MessageBoxUtils::showInfoMessageBox(tr("Material name exceed 13 characters may result in text overlaping"),this);
            return;
        }
        break;
        case 4:
        if(ui->name_lineEdit->text().size() > 11){
            MessageBoxUtils::showInfoMessageBox(tr("Material name exceed 11 characters may result in text overlaping"),this);
            return;
        }
        break;
        case 5:
        if(ui->name_lineEdit->text().size() > 9){
            MessageBoxUtils::showInfoMessageBox(tr("Material name exceed 9 characters may result in text overlaping"),this);
            return;
        }
        break;
        case 6:
        if(ui->name_lineEdit->text().size() > 7){
            MessageBoxUtils::showInfoMessageBox(tr("Material name exceed 7 characters may result in text overlaping"),this);
            return;
        }
        break;
        case 7:
        if(ui->name_lineEdit->text().size() > 6){
            MessageBoxUtils::showInfoMessageBox(tr("Material name exceed 6a characters may result in text overlaping"),this);
            return;
        }
        break;
  }

  //check material name and make sure it does not exceed 6 characters


  new_optical_properties->name=ui->name_lineEdit->text();
  new_optical_properties->anistropy_factor=ui->aniostropy_LineEdit->text();
  new_optical_properties->absorption=ui->absorption_LineEdit->text();
  new_optical_properties->scattering=ui->scattering_LineEdit->text();
  new_optical_properties->refractive_index=ui->refractive_LineEdit->text();


  bool valid = new_optical_properties->check_property_vals();
  if(valid){
        emit(new_optical_property(new_optical_properties));

    }

  else{
      delete new_optical_properties;
  }



}

void OpticalPropertyWidget::clear_name(){

    ui->name_lineEdit->clear();
}



void OpticalPropertyWidget::on_cancel_button_clicked()
{
   emit cancel_optical_property();
}

void OpticalPropertyWidget::setMaxID(int max){
    maxID = max;
}

int OpticalPropertyWidget::getMaxID(){
    return maxID;
}


