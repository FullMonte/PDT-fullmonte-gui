#include "PlanePlacementWidget.h"
#include "ui_SourcePlacementWidget.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QGridLayout>

#include "VTK/Widgets/PlaneWidgetCallBack.h"

PlanePlacementWidget::PlanePlacementWidget(vtkPlaneWidget *planeWidget, SourceList* sourceList, PDTWorkPageWidget* workPageWidget, QWidget* parent):
    SourcePlacementWidget(sourceList, workPageWidget, parent)
{
    this->vtkSourceWidget = planeWidget;

    QHBoxLayout * normalLayout = new QHBoxLayout;
    QHBoxLayout * centerLayout = new QHBoxLayout;
    normalLayout->setSpacing(5);
    centerLayout->setSpacing(5);

    double normal[3], center[3];
    planeWidget->GetCenter(center);
    planeWidget->GetNormal(normal);

    // Initialize line endpoints line edit
    for (int i=0; i < 3; i++) {
        normalLineEdits[i] = new QLineEdit(this);
        normalLineEdits[i]->setText(QString::number(normal[i]));
        normalLayout->addWidget(normalLineEdits[i]);
        connect(normalLineEdits[i], &QLineEdit::editingFinished,
                this, &PlanePlacementWidget::moveWidget);

        centerLineEdits[i] = new QLineEdit(this);
        centerLineEdits[i]->setText(QString::number(center[i]));
        centerLayout->addWidget(centerLineEdits[i]);
        connect(centerLineEdits[i], &QLineEdit::editingFinished,
                this, &PlanePlacementWidget::moveWidget);
    }


    QLabel* normalLabel = new QLabel(tr("Normal Vector\n(X,Y,Z)"), this);
    QLabel* centerLabel = new QLabel(tr("Center\n(X,Y,Z)"), this);

    ui->gridLayout->removeWidget(ui->placeButton);
    ui->gridLayout->removeWidget(ui->cancelButton);
    ui->gridLayout->addWidget(normalLabel, 0, 2, 1, 1);
    ui->gridLayout->addItem(normalLayout, 1, 0, 1, 5);
    ui->gridLayout->addWidget(centerLabel, 2, 2, 1, 1);
    ui->gridLayout->addItem(centerLayout, 3, 0, 1, 5);
    ui->gridLayout->addWidget(ui->placeButton, 6, 1);
    ui->gridLayout->addWidget(ui->cancelButton, 6, 3);

    ui->informationButton->setVisible(false);

}

PlanePlacementWidget::~PlanePlacementWidget() {

}

void PlanePlacementWidget::createSource() {
    double center[3], normal[3];
    getCenter(center);
    getNormal(normal);

    vtkPlaneWidget* planeWidget = vtkPlaneWidget::SafeDownCast(this->vtkSourceWidget);
    PDTVtkPlanePlacement* newPlanePlacement = new PDTVtkPlanePlacement(vtkPlaneSource::SafeDownCast(planeWidget->GetPolyDataAlgorithm()));

    assert(sourceList != nullptr);
	sourceList->addPlanePlacement(newPlanePlacement);

}

void PlanePlacementWidget::modifySource() {
    double normal[3], center[3];
    getNormal(normal);
    getCenter(center);

    PDTVtkPlanePlacement* planePlacement = (PDTVtkPlanePlacement *)sourceList->getPlanePlacementById(curPid);
    planePlacement->setNormal(normal);
    planePlacement->setCenter(center);
    planePlacement->update();

    assert(sourceList != nullptr);
    sourceList->showPlanePlacement(curPid);
}

void PlanePlacementWidget::moveWidget() {
    vtkPlaneWidget* planeWidget = (vtkPlaneWidget*) this->vtkSourceWidget;
    double normal [3], center[3];
    getNormal(normal);
    getCenter(center);
    planeWidget->SetNormal(normal);
    planeWidget->SetCenter(center);

    emit(renderWindow());
}

void PlanePlacementWidget::setupWidget() {
    vtkPlaneWidget* planeWidget = vtkPlaneWidget::SafeDownCast(this->vtkSourceWidget);
    vtkSmartPointer<PlaneWidgetCallBack> pcall = vtkSmartPointer<PlaneWidgetCallBack>::New();
    pcall->setLineEdits(centerLineEdits, normalLineEdits);
    planeWidget->RemoveAllObservers();
    planeWidget->AddObserver(vtkCommand::EndInteractionEvent, pcall);

    // Move widget to the current position in modification mode
    if(curAction == MODIFY) {
        assert(sourceList != nullptr);
        PDTVtkPlanePlacement* planePlacement = (PDTVtkPlanePlacement *)sourceList->getPlanePlacementById(curPid);

        double normal[3], center[3];
        planePlacement->getCenter(center);
        planePlacement->getNormal(normal);
        planeWidget->SetCenter(center);
        planeWidget->SetNormal(normal);
    }

    planeWidget->On();
}

void PlanePlacementWidget::getNormal(double normal[3]){
    for(int i = 0 ; i < 3; i++){
        normal[i] = normalLineEdits[i]->text().toDouble();
    }
}

void PlanePlacementWidget::getCenter(double center[3]){
    for(int i = 0 ; i < 3; i++){
        center[i] = centerLineEdits[i]->text().toDouble();
    }
}

void PlanePlacementWidget::showCurrentSource() {
    assert(sourceList != nullptr);
    sourceList->showPlanePlacement(curPid);
}

bool PlanePlacementWidget::closeWidget() {
    return 1;
}
