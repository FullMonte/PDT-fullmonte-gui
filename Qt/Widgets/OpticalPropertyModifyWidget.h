#ifndef OPTICALPROPERTYMODIFYWIDGET_H
#define OPTICALPROPERTYMODIFYWIDGET_H

#include <QWidget>
#include "OpticalProperty/OpticalProperty.h"


namespace Ui {
class OpticalPropertyModifyWidget;
}

class OpticalPropertyModifyWidget: public QWidget
{

    Q_OBJECT

public:
    explicit OpticalPropertyModifyWidget(QWidget *parent = nullptr);

    void changewidget_values(OpticalProperty* modify_property);

    void set_current_itm_index(unsigned);
    unsigned get_current_itm_index();


private slots:
//    void on_pushButton_2_clicked();

    void on_Modify_clicked();


    void on_pushButton_clicked();

signals:
    void modify_property_signal(OpticalProperty* modified_optical_property);
    void cancel_signal();
private:
    Ui::OpticalPropertyModifyWidget* ui;

    unsigned current_modified_item_index;
};

#endif // OPTICALPROPERTYMODIFYWIDGET_H
