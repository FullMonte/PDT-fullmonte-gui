#ifndef PLANEPLACEMENTWIDGET_H
#define PLANEPLACEMENTWIDGET_H

#include <QLineEdit>

#include <vtkPlaneWidget.h>


#include "SourcePlacementWidget.h"

class PlanePlacementWidget : public SourcePlacementWidget {

public:
    PlanePlacementWidget(vtkPlaneWidget *planeWidget, SourceList* sourceList, PDTWorkPageWidget* workPageWidget, QWidget* parent = 0);
	~PlanePlacementWidget();

    void setupWidget() override;

protected:
    void createSource() override;
    void modifySource() override;
    void showCurrentSource() override;
    bool closeWidget() override;

private slots:
    void moveWidget() override;

private:
    QLineEdit *centerLineEdits[3];
    QLineEdit *normalLineEdits[3];

	void getNormal(double normal[3]);
    void getCenter(double center[3]);

};

#endif 
