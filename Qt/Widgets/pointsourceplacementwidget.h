#ifndef POINTSOURCEPLACEMENTWIDGET_H
#define POINTSOURCEPLACEMENTWIDGET_H


#include <vector>

#include <QLineEdit>
#include <QWidget>

#include <vtkLineWidget.h>

#include "LightSource/SourceList.h"
#include "SourcePlacementWidget.h"
#include "SourcePlacementWidgetEnum.h"


using namespace std;

class PointSourcePlacementWidget : public SourcePlacementWidget
{
public :
    PointSourcePlacementWidget(vtkPointWidget *pointWidget, SourceList* sourceList,PDTWorkPageWidget * workPageWidget,QWidget *parent=0);
    ~PointSourcePlacementWidget();
    void setupWidget() override;

protected:
    void createSource() override;
    void modifySource() override;
    void showCurrentSource() override;
    bool closeWidget() override;

private slots:
    void moveWidget() override;
private:
    QLineEdit *pointEdit[3];
    QLineEdit *powerEdit;

    void getPoint(double point[3]);
    double getPower();
    void setPointEdit(double [3]);

    PDTWorkPageWidget* workpage;
};

#endif // POINTSOURCEPLACEMENTWIDGET_H
