#include "LineSourcePlanePlacementWidget.h"
#include "ui_SourcePlacementWidget.h"

#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include "LightSource/PDTVtkLineSource.h"
#include "VTK/Widgets/lineWidgetCallBack.h"

LineSourcePlanePlacementWidget::LineSourcePlanePlacementWidget(vtkLineWidget* lineWidget, SourceList* sourceList, PDTWorkPageWidget* workPageWidget, QWidget *parent) :
    SourcePlacementWidget(sourceList, workPageWidget, parent)
{
    this->vtkSourceWidget = lineWidget;

    QHBoxLayout * planePosLayout = new QHBoxLayout;
    QHBoxLayout * depthsLayout = new QHBoxLayout;
    planePosLayout->setSpacing(5);
    depthsLayout->setSpacing(5);

    // Initialize line endpoints line edit
    for (int i=0; i < 2; i++) {
        planePosLineEdits[i] = new QLineEdit(this);
        planePosLineEdits[i]->setText(tr("0.00"));
        planePosLayout->addWidget(planePosLineEdits[i]);
        connect(planePosLineEdits[i], &QLineEdit::editingFinished,
                this, &LineSourcePlanePlacementWidget::moveWidget);

        depthsLineEdits[i] = new QLineEdit(this);
        depthsLineEdits[i]->setText(tr("0.00"));
        depthsLayout->addWidget(depthsLineEdits[i]);
        connect(depthsLineEdits[i], &QLineEdit::editingFinished,
                this, &LineSourcePlanePlacementWidget::moveWidget);
    }


    QLabel* planePosLabel = new QLabel(tr("Plane Position"), this);
    QLabel* depthsLabel = new QLabel(tr("End depths"), this);

    ui->gridLayout->removeWidget(ui->placeButton);
    ui->gridLayout->removeWidget(ui->cancelButton);
    ui->gridLayout->addWidget(planePosLabel, 0, 2, 1, 1);
    ui->gridLayout->addItem(planePosLayout, 1, 0, 1, 5);
    ui->gridLayout->addWidget(depthsLabel, 2, 2, 1, 1);
    ui->gridLayout->addItem(depthsLayout, 3, 0, 1, 5);
    ui->gridLayout->addWidget(ui->placeButton, 6, 1);
    ui->gridLayout->addWidget(ui->cancelButton, 6, 3);

    ui->informationButton->setVisible(false);

}

LineSourcePlanePlacementWidget::~LineSourcePlanePlacementWidget(){

}

void LineSourcePlanePlacementWidget::createSource() {
    double endpoint1 [3], endpoint2[3];
    getWorldPosition(endpoint1, endpoint2);
    PDTVtkLineSource* newLineSource = new PDTVtkLineSource(endpoint1, endpoint2);

    assert(sourceList != nullptr);
    sourceList->addSource(newLineSource, curPid);
}

void LineSourcePlanePlacementWidget::modifySource() {
    double planePos[2], depths[2];
    getPlanePos(planePos);
    getDepths(depths);

    PDTVtkLineSource* lineSource = (PDTVtkLineSource *)sourceList->getSourceById(curSid);
    lineSource->setPlanePosition(planePos, depths);
    lineSource->update();
    sourceList->showSource(curSid);
}

void LineSourcePlanePlacementWidget::moveWidget() {
    vtkLineWidget* lineWidget = (vtkLineWidget*) this->vtkSourceWidget;
    double endpoint1[3], endpoint2[3];
    getWorldPosition(endpoint1, endpoint2);

    lineWidget->SetPoint1(endpoint1);
    lineWidget->SetPoint2(endpoint2);

    emit(renderWindow());
}

void LineSourcePlanePlacementWidget::setupWidget() {
    vtkLineWidget* lineWidget = (vtkLineWidget*) this->vtkSourceWidget;
    vtkSmartPointer<PlanePlacementLineWidgetCallBack> lcall
            = vtkSmartPointer<PlanePlacementLineWidgetCallBack>::New();
    lcall->setLineEdits(planePosLineEdits, depthsLineEdits);

    lcall->setPlanePlacement(sourceList->getPlanePlacementById(curPid));
    lineWidget->RemoveAllObservers();
    lineWidget->AddObserver(vtkCommand::EndInteractionEvent, lcall);

    // Move widget to the current source position in modification mode
    if(curAction == MODIFY) {
        PDTVtkLineSource* lineSource = (PDTVtkLineSource *)sourceList->getSourceById(curSid);
        double endpoint1[3], endpoint2[3], planePos[2], depths[2];

        lineSource->getEndpoints(endpoint1, endpoint2);
        lineSource->getPlanePosition(planePos, depths);
        lineWidget->SetPoint1(endpoint1);
        lineWidget->SetPoint2(endpoint2);
        setLineEdits(planePos, depths);
    }

    lineWidget->On();
}

void LineSourcePlanePlacementWidget::getPlanePos(double planePos[2]){
    for(int i = 0 ; i < 2; i++){
        planePos[i] = planePosLineEdits[i]->text().toDouble();
    }
}

void LineSourcePlanePlacementWidget::getDepths(double depths[2]){
    for(int i = 0 ; i < 2; i++){
        depths[i] = depthsLineEdits[i]->text().toDouble();
    }
}


void LineSourcePlanePlacementWidget::getWorldPosition(double endPoint1[3], double endPoint2[3]){
    double planePos [2], depths[2];
    getPlanePos(planePos);
    getDepths(depths);

    double planePos1[3] = {planePos[0], planePos[1], depths[0]};
    double planePos2[3] = {planePos[0], planePos[1], depths[1]};
    PDTVtkPlanePlacement* planePlacement = sourceList->getPlanePlacementById(curPid);
    planePlacement->invertToWorld(planePos1, endPoint1);
    planePlacement->invertToWorld(planePos2, endPoint2);
}

void LineSourcePlanePlacementWidget::showCurrentSource() {
    sourceList->showSource(curSid);
}

void LineSourcePlanePlacementWidget::setLineEdits (double planePos[2], double depths[3]) {
    for(int i = 0; i < 2; i++){
        this->planePosLineEdits[i]->setText(QString::number(planePos[i]));
        this->depthsLineEdits[i]->setText(QString::number(depths[i]));
    }
}

bool LineSourcePlanePlacementWidget::closeWidget() {
    return 1;
}

