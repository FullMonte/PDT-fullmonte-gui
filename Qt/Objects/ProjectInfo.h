#ifndef PROJECTINFO_H
#define PROJECTINFO_H


#include <QString>

class ProjectInfo {
public:
    QString getSourceFilePath() {return sourceFilePath; }

    void setSourceFilePath(const QString filePath) { this->sourceFilePath = filePath; }
private:
    bool isSaved;

    QString sourceFilePath;

    QString projectFilePath;

};


#endif
