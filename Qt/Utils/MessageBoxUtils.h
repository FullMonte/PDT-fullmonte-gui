#include <QMessageBox>


namespace MessageBoxUtils{

    void showErrorMessageBox(QString msg, QWidget* parent = nullptr, QString title = QString("Error"));

    void showWarningMessageBox(QString msg, QWidget* parent = nullptr, QString title = QString("Error"));

    void showInfoMessageBox(QString msg, QWidget* parent = nullptr, QString title = QString("Error"));

    void showMessageBox(QString msg, QString title, QMessageBox::Icon icon, QWidget* parent = nullptr);

}
