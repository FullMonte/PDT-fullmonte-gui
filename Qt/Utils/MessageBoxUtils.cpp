#include "MessageBoxUtils.h"

void MessageBoxUtils::showMessageBox(QString msg, QString title, QMessageBox::Icon icon, QWidget *parent) {
    QMessageBox msgBox(parent);
    msgBox.setWindowTitle(title);
    msgBox.setIcon(icon);
    msgBox.setStandardButtons(QMessageBox::Close);
    msgBox.setText(msg);
    msgBox.exec();
}

void MessageBoxUtils::showErrorMessageBox(QString msg, QWidget *parent, QString title) {
    showMessageBox(msg, title, QMessageBox::Critical, parent);
}

void MessageBoxUtils::showWarningMessageBox(QString msg, QWidget *parent, QString title) {
    showMessageBox(msg, title, QMessageBox::Warning, parent);
}

void MessageBoxUtils::showInfoMessageBox(QString msg, QWidget *parent, QString title) {
    showMessageBox(msg, title, QMessageBox::Information, parent);
}

