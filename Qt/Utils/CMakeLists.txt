SET(CMAKE_INCLUDE_CURRENT_DIR ON)

SET(CMAKE_AUTOMOC ON)
SET(CMAKE_AUTOUIC ON)

SET(QT_UTILS_SRC
	MessageBoxUtils.h
	MessageBoxUtils.cpp
)

add_library(PQtUtils ${QT_UTILS_SRC})

target_include_directories(PQtUtils PUBLIC ${CMAKE_SOURCE_DIR})
qt5_use_modules(PQtUtils Core Gui Widgets)


